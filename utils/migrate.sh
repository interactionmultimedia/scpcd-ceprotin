#!/bin/bash

if [ $# -eq 0 ]; then
    echo "Usage: ./migrate.sh [command] [options]"
    exit 1;
fi

node ./../node_modules/db-migrate/bin/db-migrate --config ./../config/db-migrate.json --migrations-dir ./../migrations $*
