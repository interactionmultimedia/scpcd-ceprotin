var fs = require("fs");

var flattenObject = function (ob) {
    let toReturn = {};
    let flatObject;
    for (let i in ob) {
        if (!ob.hasOwnProperty(i)) {
            continue;
        }
        //Exclude arrays from the final result
        //Check this http://stackoverflow.com/questions/4775722/check-if-object-is-array
        if (ob[i] && Array === ob[i].constructor) {
            continue;
        }
        if ((typeof ob[i]) === 'object') {
            flatObject = flattenObject(ob[i]);
            for (let x in flatObject) {
                if (!flatObject.hasOwnProperty(x)) {
                    continue;
                }
                //Exclude arrays from the final result
                if (flatObject[x] && Array === flatObject.constructor) {
                    continue;
                }
                toReturn[i + (!!isNaN(x) ? '.' + x : '')] = flatObject[x];
            }
        } else {
            toReturn[i] = ob[i];
        }
    }
    return toReturn;
};

var arrayDiff = function (a1, a2) {
    var a = [], diff = [];

    for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
    }

    for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
            delete a[a2[i]];
        } else {
            a[a2[i]] = true;
        }
    }

    for (var k in a) {
        diff.push(k);
    }

    return diff;
};

var arrayIntersect = function (a1, a2) {
    var a = [], intersect = [];

    for (let i in a1) {
        if (!a1.hasOwnProperty(i)) {
            continue;
        }
        a[a1[i]] = true;
    }

    for (let i in a2) {
        if (!a2.hasOwnProperty(i)) {
            continue;
        }
        if (a[a2[i]]) {
            intersect.push(i);
        }
    }

    return intersect;
};

var execute = function () {
    var myArgs = process.argv.slice(2),
        contentSource,
        content;

    if (!myArgs[0]) {
        console.error("Missing parameter");
        return;
    }

    contentSource = flattenObject(JSON.parse(fs.readFileSync(__dirname + "/../webapp/js/locales/en/shire.json")));
    content = flattenObject(JSON.parse(fs.readFileSync(__dirname + "/../webapp/js/locales/" + myArgs[0] + "/shire.json")));

    console.log("Missing keys");
    console.log(arrayDiff(Object.keys(contentSource), Object.keys(content)));
    console.log("\n");
    console.log("Missing translation");
    console.log(arrayIntersect(contentSource, content));
};

execute();