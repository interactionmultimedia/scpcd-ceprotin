({
    appDir: './',
    baseUrl: './js/app',
    dir: './dist',
    modules: [
        {
            name: 'main'
        }
    ],
    fileExclusionRegExp: /^(r|build)\.js$/,
    optimizeCss: 'standard',
    removeCombined: true,
    paths: {
        backbone: "../../node_modules/backbone/backbone-min",
        "backbone.localStorage": "../../node_modules/backbone.localstorage/backbone.localStorage-min",
        "backbone.radio": "../../node_modules/backbone.radio/build/backbone.radio",
        cookies: "../../node_modules/js-cookie/src/js.cookie",
        css: "../../node_modules/require-css/css",
        deepmerge: "../../node_modules/deepmerge/dist/umd",
        EventEmitter: "../../node_modules/bower-event-emitter/dist/EventEmitter",
        fastclick: "../../node_modules/fastclick/lib/fastclick",
        handlebars: "../../node_modules/handlebars/dist/handlebars.min",
        hbs: "../../node_modules/require-handlebars-plugin/hbs",
        howler:"../../node_modules/howler/dist/howler.min",
        i18n: "../../node_modules/i18next/i18next.min",
        i18nXHR: "../../node_modules/i18next-xhr-backend/i18nextXHRBackend.min",
        i18nLngDetector: "../../node_modules/i18next-browser-languagedetector/i18nextBrowserLanguageDetector.min",
        jquery: "../../node_modules/jquery/dist/jquery.min",
        "jquery-mousewheel": "../../node_modules/jquery-mousewheel/jquery.mousewheel",
        "jquery.validate": "../../node_modules/jquery-validation/dist/jquery.validate",
        marionette: "../../node_modules/backbone.marionette/lib/backbone.marionette",
        mCustomScrollbar: "../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar",
        modernizr: "../../node_modules/npm-modernizr/modernizr",
        text: "../../node_modules/requirejs-text/text",
        underscore: "../../node_modules/underscore/underscore-min",
        GA: "../../node_modules/requirejs-google-analytics/dist/GoogleAnalytics",
    },
    shim: {
        backbone: {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        },
        cookies: {
            deps: ['jquery']
        },
        hbs: {
            deps: ["handlebars", "underscore"],
            templateExtension: "hbs",
            "hbs/handlebars": "handlebars",
            "hbs/underscore": "underscore"
        },
        "jquery.validate" : {
            deps : ["jquery"]
        },
        marionette: {
            deps: ["backbone"]
        },
        modernizr: {
            exports: "Modernizr"
        },
        mCustomScrollbar: {
            deps : ["jquery", "jquery-mousewheel"]
        }
    }
})
