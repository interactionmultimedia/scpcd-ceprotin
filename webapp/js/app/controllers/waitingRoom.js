define("controllers/waitingRoom", function(require) {
    "use strict";

    var Marionette                  = require("marionette"),
        Radio                       = require("backbone.radio"),
        $                           = require("jquery"),
        i18n                        = require("i18n"),
        WaitingRoomView             = require("views/waitingRoom"),
        ConsultationView            = require("views/consultation"),
        EmptyView                   = require("views/empty"),
        ConsultationsCollection     = require("collections/consultations"),
        ModalLayoutView             = require("views/modal/modal"),
        HeaderWithTitleView         = require("views/modal/headerWithTitle"),
        ClinicalCasesCollection     = require("collections/clinicalCases"),
        PatientProfileView          = require("views/waitingRoom/patientProfile"),
        WaitingRoomController;

    WaitingRoomController = Marionette.Controller.extend({
        routes: {
            "waiting-room": "index",
            "case": "case"
        },

        initialize: function() {
            this.initView();

            this.channel    = Radio.channel("App");
            this.channel.on('select:consultation', this.selectConsultation.bind(this), this);
            this.channel.on("screen:share:save", this.goToCase.bind(this), this);
            this.channel.on("screen:feedback:exit", this.goToCase.bind(this), this);
            this.channel.on("screen:globalConclusion:exit", this.goToCase.bind(this), this);
            this.channel.on("preload:end", this.afterLoad.bind(this), this);
        },

        initView: function() {
            if(!this.waitingRoomView || this.waitingRoomView.isDestroyed){
                this.waitingRoomView = new WaitingRoomView();
            }
            if(!this.emptyView || this.emptyView.isDestroyed){
                this.emptyView = new EmptyView();
            }
        },

        initLayout: function () {

            this.initView();

            this.channel.request("region:show", { view: this.waitingRoomView });
        },

        index: function(caseId, consultationId) {
            if(!App.Consultations){
                App.Consultations = new ConsultationsCollection();
            }
            this.initLayout();
        },

        case: function(caseId, consultationId) {
            this.initView();
            
            if(!App.Consultations){
                App.Consultations = new ConsultationsCollection();
            }

            var caseId = 1,
                clinicalCase = App.ClinicalCases.get(1);

            if(!App.Consultations.caseExists(caseId)){
                App.Consultations = new ConsultationsCollection();
            }

            this.channel.request("region:show", { view: this.emptyView});

            this.popupChoice = new ModalLayoutView({
                className: "overlay overlay-patient-profile overlay-with-padding overlay-takeda-logo overlay-transparent",
                headerView: null,
                contentView: new PatientProfileView({
                    model: clinicalCase
                }),
                footerView: null
            });

            this.popupChoice.setHeaderClassName("overlay-header clearfix");
            this.popupChoice.showModal();
        },

        selectConsultation: function(url) {
            if(this.popupChoice){
                this.popupChoice.hideModal();
                this.popupChoice.destroy();
            }

            this.channel.trigger("consultation:choice", url);

            App.router.navigate(url, {trigger: true});
        },

        afterLoad: function(context) {
            if(context !== "main" || App.Config.isRP()){
                return;
            }

            this.goToCase();
        },

        goToCase: function() {
            App.SimulationState.resetGuides();
            App.router.navigate('case', {trigger: true});
        },

        onDestroy: function() {
            this.channel.off(null, null, this);
        }
    });

    return WaitingRoomController;
});
