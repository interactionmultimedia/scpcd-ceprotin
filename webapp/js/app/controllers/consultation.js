define("controllers/consultation", function(require) {
    "use strict";

    var Marionette                  = require("marionette"),
        Radio                       = require("backbone.radio"),
        $                           = require("jquery"),
        ConsultationView            = require("views/consultation"),
        ConsultationModel           = require("models/consultation"),
        PreloadView                 = require("views/preload"),
        FormFeedbackView            = require("views/feedback-form"),
        ModalLayoutView             = require("views/modal/modal"),
        ConsultationsCollection     = require("collections/consultations"),
        ConsultationController;

    ConsultationController = Marionette.Controller.extend({
        routes: {
            "case/:caseId/consultation/:consultationId": "index"
        },

        initialize: function() {
            this.initView();

            this.channel    = Radio.channel("App");
            this.channel.on("clinicalExam:quit", this.quitClinicalExam.bind(this));
            this.channel.on("preload:end", this.showConsultation.bind(this));
            this.channel.on("participant:consultation:finish", this.redirectParticipantConsultationFinish.bind(this), this);
            this.channel.on("participant:meeting:finish", this.redirectParticipantMeetingFinish.bind(this), this);
            this.channel.on("screen:feedbackForm:save", this.hideFeedbackForm.bind(this), this);
        },

        initView: function() {
            if(!this.consultationView || this.consultationView.isDestroyed){
                this.consultationView = new ConsultationView();
            }
        },

        initLayout: function () {
            this.initView();
            this.channel.request("region:show", { view: this.consultationView });
        },

        showConsultation: function(context) {
            if(context !== "consultation"){
                return;
            }

            var that            = this,
                clinicalCase    = App.ClinicalCases.get(~~this.caseId);

            App.consultation = new ConsultationModel({
                caseId: this.caseId,
                consultationId: this.consultationId
            });
            App.consultation.set("last", ( clinicalCase ? clinicalCase.isLast(this.consultationId) : false ));
            
            if(App.Config.isRP()) {
                if(!App.launchedCases) {
                    App.launchedCases = [];
                }
                App.launchedCases.push(App.consultation.get('caseId')+'_'+App.consultation.get('consultationId'));
            }

            this.channel.request("track:event", {
                category: 'Consultation',
                action: 'Lancement',
                label: App.consultation.get("name")
            });

            $.when(
                App.consultation.fetch(),
                App.consultation.get('steps').fetch(),
                App.consultation.get('medicalFiles').fetch()
            ).then(function() {
                if(App.Config.isDev() || !App.Consultations){
                    App.Consultations = new ConsultationsCollection();
                }

                if(!App.Consultations.consultationExists(that.consultationId)){
                    App.Consultations.add(App.consultation);
                }

                App.consultation.initData();

                that.initLayout();

                App.SimulationState.set('startTimeConsultation', new Date().getTime());
                App.SimulationState.save();

                if( !App.Config.isRP() ) {
                    //that.showFeedbackForm();
                }
            });
        },

        index: function(caseId, consultationId) {
            this.caseId         = caseId;
            this.consultationId = consultationId;

            var folderName  = "/data/assets/c" + caseId + "_c" + consultationId + "/",
                preloadView;

            preloadView     = new PreloadView({
                path: folderName + "assets.json",
                context: "consultation"
            });

            this.channel.request("region:show", { view: preloadView});
        },

        showFeedbackForm: function() {
            if( App.SimulationState.get("email") !== null ) {
                return;
            }

            this.popupFeedbackForm = new ModalLayoutView({
                className: "overlay overlay-exam-report overlay-with-padding",
                contentView: new FormFeedbackView(),
                footerView: null
            });
            this.popupFeedbackForm.showModal();
        },

        hideFeedbackForm: function() {
            if( !!this.popupFeedbackForm ) {
                this.popupFeedbackForm.hideModal();
            }
        },

        quitClinicalExam: function(){
            // this.initLayout();
        },

        redirectParticipantConsultationFinish: function() {
            App.router.goto("participant-waiting");
        },

        redirectParticipantMeetingFinish: function() {
            App.router.goto("meeting-end");
        }

    });

    return ConsultationController;
});
