define("controllers/main", function(require) {
    "use strict";

    var Marionette                          = require("marionette"),
        i18n                                = require("i18n"),
        Radio                               = require("backbone.radio"),
        Cookies                             = require("cookies"),
        MainView                            = require("views/main"),
        LegalsView                          = require("views/legals"),
        HeaderView                          = require("views/header"),
        FooterView                          = require("views/footer"),
        HomeRPView                          = require("views/home-rp"),
        TrainerAuthenticationFormView       = require("views/meeting/trainer-authentication-form"),
        TrainerEventInfoFormView            = require("views/meeting/trainer-event-info-form"),
        TrainerEventParticipantsFormView    = require("views/meeting/trainer-event-participants-form"),
        ParticipantAuthenticationFormView   = require("views/meeting/participant-authentication-form"),
        MeetingLoadingView                  = require("views/meeting/loading"),
        MeetingFinishedView                 = require("views/meeting/finished"),
        PreloadView                         = require("views/preload"),
        ShareView                           = require("views/share"),
        ForbiddenView                       = require("views/forbidden"),
        MainController;

    MainController = Marionette.Controller.extend({
        routes: {
            "": "index",
            "trainer-authentication": "trainerAuthenticationForm",
            "event-information": "eventInformation",
            "event-participants": "eventParticipants",
            "participant-authentication": "participantAuthenticationForm",
            "participant-waiting": "showLoading",
            "waiting-feedback": "waitingFeedback",
            "share": "share",
            "forbidden": "forbidden",
            "meeting-end":"eventFinished"
        },
        initialize: function() {
            this.initMainView();

            this.channel    = Radio.channel("App");
            this.channel.on("app:change:language", this.reload.bind(this), this);
            this.channel.on("participant:logged", this.showLoading.bind(this), this);
            this.channel.on("preload:end", this.showHomeRP.bind(this), this);
            this.channel.on("screen:authenticationFrom:back", this.showHomeRP.bind(this), this);
        },
        onDestroy: function() {
            this.channel.off(null, null, this);
        },
        initLayout: function () {
            this.initMainView();
            this.initLegalsView();

            this.channel.request("region:show", { view: this.mainView });
            this.channel.request("legals:show", { view: this.legalsView });

            if( !(this.mainView.getRegion('header').currentView instanceof HeaderView) ) {
                this.mainView.showChildView('header', new HeaderView());
            }

            if( !(this.mainView.getRegion('footer').currentView instanceof FooterView) ) {
                this.mainView.showChildView('footer', new FooterView());
            }
        },
        initMainView: function() {
            if(!this.mainView || this.mainView.isDestroyed){
                this.mainView = new MainView();
            }
        },
        initLegalsView: function() {
            if(!this.legalsView || this.legalsView.isDestroyed){
                this.legalsView = new LegalsView();
            }
        },
        showHomeRP: function(context) {
            if(context !== "main" || !App.Config.isRP()){
                return;
            }

            this.initLayout();

            if( !(this.mainView.getRegion('content').currentView instanceof HomeRPView) ) {
                this.mainView.showChildView('content', new HomeRPView());
            }
        },
        index: function() {
            this.channel.request("region:show", { view: new PreloadView({
                path: "/img/assets.json",
                context: "main"
            })});
        },
        trainerAuthenticationForm: function() {
            if( !App.Config.isRP() ) {
                return App.router.goto("");
            }

            this.initLayout();

            if( !(this.mainView.getRegion('content').currentView instanceof TrainerAuthenticationFormView) ) {
                this.mainView.showChildView('content', new TrainerAuthenticationFormView());
            }
        },
        eventInformation: function() {
            if( !App.Config.isRP() || !App.Trainer ) {
                return App.router.goto("");
            }

            this.initLayout();

            if( !(this.mainView.getRegion('content').currentView instanceof TrainerEventInfoFormView) ) {
                this.mainView.showChildView('content', new TrainerEventInfoFormView());
            }

        },
        eventParticipants: function() {
            if( !App.Config.isRP() || !App.Trainer || !App.Meeting ) {
                return App.router.goto("");
            }

            this.initLayout();

            if( !(this.mainView.getRegion('content').currentView instanceof TrainerEventParticipantsFormView) ) {
                this.mainView.showChildView('content', new TrainerEventParticipantsFormView());
            }

        },
        eventFinished: function() {
            if( !App.Config.isRP() ) {
                return App.router.goto("");
            }

            this.initLayout();

            if( !(this.mainView.getRegion('content').currentView instanceof MeetingFinishedView) ) {
                this.mainView.showChildView('content', new MeetingFinishedView());
            }

        },
        participantAuthenticationForm: function() {
            if( !App.Config.isRP() ) {
                return App.router.goto("");
            }

            this.initLayout();

            if( !(this.mainView.getRegion('content').currentView instanceof ParticipantAuthenticationFormView) ) {
                this.mainView.showChildView('content', new ParticipantAuthenticationFormView());
            }
        },
        showLoading: function(e) {
            if( !App.Config.isRP() || !App.Participant ) {
                return App.router.goto("");
            }

            this.initLayout();

            if( !(this.mainView.getRegion('content').currentView instanceof MeetingLoadingView) ) {
                this.mainView.showChildView('content', new MeetingLoadingView());
            }
        },
        share: function() {
            if( App.Config.isRP() ) {
                return App.router.goto("");
            }

            this.initLayout();

            if( !(this.mainView.getRegion('content').currentView instanceof ShareView) ) {
                this.mainView.showChildView('content', new ShareView());
            }
        },
        waitingFeedback: function() {
            if( !App.Config.isRP() ) {
                return App.router.goto("");
            }

            this.initLayout();

            if( !(this.mainView.getRegion('content').currentView instanceof MeetingLoadingView) ) {
                this.mainView.showChildView('content', new MeetingLoadingView());
            }
        },
        forbidden: function() {
            this.initMainView();

            this.channel.request("region:show", { view: this.mainView });

            if( !(this.mainView.getRegion('content').currentView instanceof ForbiddenView) ) {
                this.mainView.showChildView('content', new ForbiddenView());
            }
        },
        reload: function() {
            window.location.reload();
        }
    });

    return MainController;
});
