define("views/waitingRoom", function(require) {
    "use strict";

    var Radio                           = require("backbone.radio"),
        $                               = require("jquery"),
        i18n                            = require("i18n"),
        BaseLayoutView                  = require("views/base"),
        ModalLayoutView                 = require("views/modal/modal"),
        WaitingRoomTemplate             = require("hbs!templates/waitingRoom"),
        SelectView                      = require("views/waitingRoom/select"),
        HeaderWithTitleView             = require("views/modal/headerWithTitle"),
        PersonsView                     = require("views/waitingRoom/persons"),
        CopyrightFooterView             = require("views/waitingRoom/copyright"),
        WaitingRoomView;

    WaitingRoomView = BaseLayoutView.extend({
        template: WaitingRoomTemplate,
        data: {},

        regions: {
            "persons": "#persons"
        },

        initialize: function() {
            this.channel    = Radio.channel("App");
            this.channel.on('change:room', this.changeRoom.bind(this), this);
        },

        changeRoom: function(roomId) {
            var nextRoom;

            if( this.popupChoice ) {
                this.popupChoice.hideModal();
                this.popupChoice.destroy();
                this.popupChoice    = null;
            }

            if(~~roomId === 1){
                nextRoom = 2;
            } else {
                nextRoom = 1;
            }

            this.showChildView('persons', new PersonsView({
                data: {
                    baseUrl: "/data/",
                    nextRoom: nextRoom,
                    nbPersonsInNextRoom: App.ClinicalCases.getPublishedClinicalCases(nextRoom, i18n.language).length,
                    persons: App.ClinicalCases.getPublishedClinicalCases(roomId, i18n.language).toJSON()
                }
            }));

            $("body")
                .removeClass()
                .addClass("no-overlay waiting-room waiting-room-0" + roomId + ( App.Config.isWebconf() ? " waiting-room-webconf" : "" ));
        },

        onBeforeShow: function() {
            if( App.Config.isWebconf() ) {
                this.channel.trigger('change:room', 1);
            } else {
                this.popupChoice = new ModalLayoutView({
                    className: "overlay overlay-waiting-room-choice overlay-with-padding",
                    headerView : new HeaderWithTitleView({
                        data: {
                            title: i18n.t("waitingRoom.choose")
                        },
                        hideBtn: true
                    }),
                    contentView: new SelectView(),
                    footerView: new CopyrightFooterView()
                });

                this.popupChoice.setHeaderClassName("overlay-header clearfix");
                this.popupChoice.showModal();
            }
        },

        onRender: function() {
            $("body")
                .removeAttr("style")
                .addClass("waiting-room-choice no-overlay");
        },

        /**
         * A la destruction :
         * - suppression des classes CSS
         * - suppression des listeners
         */
        onDestroy: function() {
            $("body").removeClass("waiting-room-choice no-overlay");

            this.channel.off(null, null, this);

            if(this.popupChoice){
                this.popupChoice.destroy();
            }

            this.popupChoice = null;
        }
    });

    return WaitingRoomView;
});

