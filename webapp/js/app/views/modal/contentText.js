define("views/modal/contentText", function(require) {
    "use strict";

    var _                       = require("underscore"),
        BaseLayoutView          = require("views/base"),
        CustomScrollbarBehavior = require("views/behaviors/customScrollbarBehavior"),
        ContentTextTemplate     = require("hbs!templates/modal/contentText"),
        ContentTextView;

    ContentTextView = BaseLayoutView.extend({
        template: ContentTextTemplate,
        data: {},
        behaviors: {
            CustomScrollbarBehavior: {
                behaviorClass: CustomScrollbarBehavior
            }
        },

        initialize: function(options) {
            var data = options.data.list ? options.data.list : null,
                html = options.data.content ? options.data.content : "",
                height = options.data.height ? options.data.height : null;

            if(data){
                if(_.isArray(data)){
                    _.each(data, function(element){
                        if(element.text){
                            html += "<div>" + element.text + "</div>";
                        } else if(element.AssetVO && element.AssetVO.url){
                            html += "<figure class=\"text-center\">";
                            html += "  <img src='" + element.AssetVO.url + "' />";
                            html += "  <figcaption>"+( element.AssetVO.source ? element.AssetVO.source : "" )+"</figcaption>";
                            html += "</figure>";
                        }
                    });
                } else {
                    html += "<div>" + data.text + "</div>";
                }
            }

            if( typeof options.data.subtitle !== "undefined" ) {
                this.data.subtitle = options.data.subtitle;
            } else {
                this.data.subtitle = null;
            }

            this.data.content = html;
            this.data.height = height;
        },

        onShow: function() {
            this.trigger("CustomScrollbarBehavior:init", {
                $el: this.$el,
                height: this.data.height
            });
        }
    });

    return ContentTextView;
});
