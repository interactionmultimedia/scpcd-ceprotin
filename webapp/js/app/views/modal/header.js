define("views/modal/header", function(require) {
    "use strict";

    var Radio           = require("backbone.radio"),
        BaseLayoutView  = require("views/base"),
        HeaderTemplate  = require("hbs!templates/modal/header"),
        HeaderView;

    HeaderView = BaseLayoutView.extend({
        template: HeaderTemplate,
        ui: {
            'btnClose': '.btn-close',
            'btnBack': '.btn-back-circle',
        },
        events: {
            'click @ui.btnClose': 'close',
            'click @ui.btnBack': 'close',
        },

        initialize: function () {
            this.channel    = Radio.channel("App");
            this.channel.on("click:quitConsultation", this.hideCloseButton.bind(this), this);

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },

        /**
         * Ferme la "modal"
         */
        close: function (e) {
            e.preventDefault();

            this.channel.trigger('modal:close');
            $('#back-button').addClass('d-none');
        },

        /**
         * Cache le bouton de fermeture
         */
        hideCloseButton: function(){
            this.$el.find(".btn-close").hide();
        },

        /**
         * Affiche le bouton de retour
         */
        showBackButton: function(){
            this.$el.find(".btn-back-circle").removeClass('hidden');
        },

        /**
         * A la destruction de la vue
         */
        onDestroy: function() {
            this.channel.off(null, null, this);
        }
    });

    return HeaderView;
});
