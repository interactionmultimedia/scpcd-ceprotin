define("views/modal/contentQuiz", function(require) {
    "use strict";
    var Radio                   = require("backbone.radio"),
        BaseLayoutView          = require("views/base"),
        CustomScrollbarBehavior = require("views/behaviors/customScrollbarBehavior"),
        ContentQuizTemplate     = require("hbs!templates/modal/contentQuiz"),
        ContentQuizView;

    ContentQuizView = BaseLayoutView.extend({
        template: ContentQuizTemplate,
        data: {},
        behaviors: {
            CustomScrollbarBehavior: {
                behaviorClass: CustomScrollbarBehavior
            }
        },

        ui: {
            'btnValidate': '.btn',
            'fieldAnswer': 'input',
            'btnBack': '.btn-back-quizz'
        },

        events: {
            'click @ui.btnValidate': 'validate',
            'click @ui.fieldAnswer': 'selectAnswer',
            'click @ui.btnBack': 'back'
        },

        initialize: function(options) {

            this.channel = Radio.channel("App");

            this.index = 0;

            this.moduleName = options.quiz.get("moduleName");
            this.quiz = options.quiz;

            this.questions = options.quiz.get("questions");
            this.setModel();

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },

        /**
         * Mémorisation dans le model au clic sur la réponse
         * @param  {Object} e Event
         */
        selectAnswer: function(e) {
            var answerId = $(e.currentTarget).data("id"),
                answer = this.model.get("answers").get(answerId);

            if(!this.model.isMultiple()){
                this.model.resetSelection();
            }

            if ($(e.currentTarget).closest(".row").find("input").is(":checked")) {
                answer.set("checked", true);
            } else {
                answer.set("checked", false);
            }
        },

        /**
         * Validation de la question
         * - Prochaine question
         * - Fermeture de la modal
         * @param  {Object} e Event
         */
        validate: function(e) {
            e.preventDefault();

            var that            = this,
                hasAnswers      = this.model.hasAnswer(),
                questionLabel   = App.consultation.getIdWithPrefix("c") +' - '+ this.model.get("questionLabel"),
                answersData     = [];

            this.channel.request("track:event", {
                category: 'Consultation',
                action: questionLabel,
                label: ( hasAnswers && this.model.isCorrect() ? "Correct" : "Incorrect" )
            });

            if( hasAnswers ) {

                this.model.getUserAnswers().each(function (answer) {

                    if (App.Config.isRP() && !!App.Participant) {
                        answersData.push({
                            'participant_id': App.Participant.get('id'),
                            'answer_id': answer.get('id')

                        });
                    } else if( !App.Config.isRP() ) {
                        that.channel.request("track:event", {
                            category: 'Consultation',
                            action: questionLabel,
                            label: answer.get("answerLabel")
                        });
                    }

                });

                if (App.Config.isRP() && !!App.Participant) {
                    $.post(
                        App.Config.get("gateway") + "/participant/answers", 
                        { 'answers':answersData }
                    );
                }

            }

            if(this.quizFinish()){
                App.SimulationState.saveQuiz(this.moduleName, this.quiz);
                this.channel.trigger("modal:close");
            } else {
                this.trigger("CustomScrollbarBehavior:destroy", {
                    $el: this.$el
                });
                this.index++;
                this.setModel();
                this.render();
            }

            if (this.questions.length > 1){
                $('#back-button').removeClass('d-none');
            }

        },
    /* Back to the previous question */
        
        back: function (e) {
            e.preventDefault();

            var that = this,
                hasAnswers = this.model.hasAnswer(),
                questionLabel = App.consultation.getIdWithPrefix("c") + ' - ' + this.model.get("questionLabel"),
                answersData = [];

            this.channel.request("track:event", {
                category: 'Consultation',
                action: questionLabel,
                label: (hasAnswers && this.model.isCorrect() ? "Correct" : "Incorrect")
            });

            if (hasAnswers) {

                this.model.getUserAnswers().each(function (answer) {

                    if (App.Config.isRP() && !!App.Participant) {
                        answersData.push({
                            'participant_id': App.Participant.get('id'),
                            'answer_id': answer.get('id')
                        });
                    } else if (!App.Config.isRP()) {
                        that.channel.request("track:event", {
                            category: 'Consultation',
                            action: questionLabel,
                            label: answer.get("answerLabel")
                        });
                    }

                });

                if (App.Config.isRP() && !!App.Participant) {
                    $.post(
                        App.Config.get("gateway") + "/participant/answers", {
                            'answers': answersData
                        }
                    );
                }
            }

            if (this.index > 0) {
                this.trigger("CustomScrollbarBehavior:destroy", {
                    $el: this.$el
                });
                App.SimulationState.saveQuiz(this.moduleName, this.quiz);
                this.index--;
                this.setModel();
                this.render();
                $('#back-button').removeClass('d-none');
                
            }
            
            if (this.index == 0) {
                 $('#back-button').addClass('d-none');
            }
        }, 

        /**
         * Le quiz est-il terminé ?
         * @return {Boolean} Quiz terminé
         */
        quizFinish: function() {
            return this.index >= this.questions.length - 1;
        },

        onBeforeRender: function(){
            this.data.isMultiple = this.model.isMultiple();
            this.data.moduleName = this.moduleName;
            this.data.index = this.index + 1;
        },
        
        onRender: function() {
            this.trigger("CustomScrollbarBehavior:init", {
                $el: this.$el,
                height: (window.innerHeight - 150)
            });
        },

        /**
         * Le quiz est-il terminé ?
         * @return {Boolean} Quiz terminé
         */
        setModel: function(){
            this.model = this.questions.at(this.index);
        },

        /**
         * Suppression des listeners à la destruction de la vue
         */
        onDestroy: function() {
            this.channel.off(null, null, this);
        }
    });
    
    return ContentQuizView;
});
