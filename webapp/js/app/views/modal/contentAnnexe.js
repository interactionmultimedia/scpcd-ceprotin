define("views/modal/contentAnnexe", function(require) {
    "use strict";

    var Radio                   = require("backbone.radio"),
        BaseLayoutView          = require("views/base"),
        CustomScrollbarBehavior = require("views/behaviors/customScrollbarBehavior"),
        ContentAnnexeTemplate   = require("hbs!templates/modal/contentAnnexe"),
        ContentAnnexeView;

    ContentAnnexeView = BaseLayoutView.extend({
        template: ContentAnnexeTemplate,

        behaviors: {
            CustomScrollbarBehavior: {
                behaviorClass: CustomScrollbarBehavior
            }
        },

        initialize: function() {
            this.channel = Radio.channel("App");
            this.channel.on("modal:shown", this.activateScroll.bind(this), this);
        },

        activateScroll: function(){
            this.trigger("CustomScrollbarBehavior:init", {
                $el: this.$el,
                height: window.innerHeight - 125
            });
        },

        onDestroy: function() {
            this.channel.off(null, null, this);
        },
    });

    return ContentAnnexeView;
});
