define("views/modal/modal", function(require) {
    "use strict";

    var Marionette                  = require("marionette"),
        Radio                       = require("backbone.radio"),
        _                           = require("underscore"),
        BaseLayoutView              = require("views/base"),
        ModalTemplate               = require("hbs!templates/modal/modal"),
        HeaderView                  = require("views/modal/header"),
        FooterView                  = require("views/modal/footer"),
        ModalLayoutView;

    ModalLayoutView = BaseLayoutView.extend({
        template: ModalTemplate,
        className: "overlay",
        attributes: {},
        data: {
            id: "",
            title: null,
            content: "",
            close: true
        },

        regions: {
            'header': '#modal-header',
            'content': '#modal-content',
            'footer': '#modal-footer'
        },

        initialize: function (options) {
            this.channel    = Radio.channel("App");
            this.channel.on("modal:close", this.hideModal.bind(this), this);

            this.headerView = null;
            if(typeof options.headerView === "undefined"){
                this.headerView = new HeaderView();
            } else if(options.headerView !== null){
                this.headerView = options.headerView;
            }

            this.footerView = null;
            if(typeof options.footerView === "undefined"){
                this.footerView = new FooterView();
            } else if(options.footerView !== null){
                this.footerView = options.footerView;
            }

            this.contentView = null;
            if(typeof options.contentView !== "undefined" && options.contentView !== null){
                this.contentView = options.contentView;
            }

            $('body').append(this.$el);

            this.render();
        },

        setClassName: function(className) {
            this.$el.removeClass(this.className).addClass(className);

            this.className  = className;
        },

        setHeaderClassName: function(className) {
            this.getRegion("header").$el
                .removeClass()
                .addClass(className);
        },

        setFooterClassName: function(className) {
            this.getRegion("footer").$el
                .removeClass()
                .addClass(className);
        },

        /**
         * Ouverture de la modal
         */
        showModal: function () {
            var that = this;

            $('body')
                .addClass('is-overlay');

            this.$el
                .attr('id', _.uniqueId('modal_'))
                .fadeIn(300, function(){
                    that.channel.trigger("modal:shown");
                });
        },

        /**
         * Fermeture de la modal
         */
        hideModal: function () {
            var that = this;

            $('body').removeClass('is-overlay');

            this.$el.fadeOut(300, function(){
                that.$el.remove();
                that.channel.trigger("modal:hidden");
                that.destroy();
            });
        },

        enable: function () {
            this.$el.fadeIn(300);
        },

        disable: function () {
            this.$el.fadeOut(300);
        },

        /**
         * Instanciation des régions
         */
        onRender: function () {
            if(this.headerView !== null){
                if( this.getRegion('header').currentView !== this.headerView ) {
                    this.showChildView('header', this.headerView);
                }
            }

            if(this.footerView !== null){
                if( this.getRegion('footer').currentView !== this.footerView ) {
                    this.showChildView('footer', this.footerView);
                }
            }

            if(this.contentView !== null){
                if( this.getRegion('content').currentView !== this.contentView ) {
                    this.showChildView('content', this.contentView);
                }
            }
        },

        /**
         * Destruction de la modal
         */
        onDestroy: function () {
            this.channel.off(null, null, this);
        }
    });

    return ModalLayoutView;
});
