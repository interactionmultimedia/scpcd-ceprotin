define("views/modal/headerWithTitle", function(require) {
    "use strict";

    var Radio               = require("backbone.radio"),
        HeaderView          = require("views/modal/header"),
        LanguageSelectView  = require("views/language-select"),
        HeaderTemplate      = require("hbs!templates/modal/headerWithTitle"),
        HeaderTitleView;

    HeaderTitleView = HeaderView.extend({
        template: HeaderTemplate,
        regions: {
            'languageSelect': '#language-select'
        },

        initialize: function (options) {
            this.hideBtn = options.hideBtn || false;
            this.backBtn = options.backBtn || false;

            HeaderView.prototype.initialize.apply(this, arguments);
        },

        /**
         * Cache le bouton de fermeture
         */
        onBeforeShow: function(){
            if( this.hideBtn ) {
                this.hideCloseButton();
            }
            if( this.backBtn ) {
                this.showBackButton();
            }
        },
        
        onShow:function () {

            //this.showChildView('languageSelect', new LanguageSelectView());
            
        }

    });

    return HeaderTitleView;
});
