define("views/modal/footer", function(require) {
    "use strict";

    var BaseLayoutView  = require("views/base"),
        FooterTemplate  = require("hbs!templates/modal/footer"),
        FooterView;

    FooterView = BaseLayoutView.extend({
        template: FooterTemplate
    });

    return FooterView;
});
