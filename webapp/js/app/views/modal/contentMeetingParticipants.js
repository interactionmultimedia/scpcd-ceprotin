define("views/modal/contentMeetingParticipants", function(require) {
    "use strict";

    var Radio                               = require("backbone.radio"),
        Participant                         = require("models/participant"),
        ParticipantsCollection              = require("collections/participants"),
        BaseLayoutView                      = require("views/base"),
        CustomScrollbarBehavior             = require("views/behaviors/customScrollbarBehavior"),
        ContentMeetingParticipantsTemplate  = require("hbs!templates/modal/contentMeetingParticipants"),
        ContentMeetingParticipantsView;

    ContentMeetingParticipantsView = BaseLayoutView.extend({
        template: ContentMeetingParticipantsTemplate,

        behaviors: {
            CustomScrollbarBehavior: {
                behaviorClass: CustomScrollbarBehavior
            }
        },

        initialize: function() {
            this.channel = Radio.channel("App");
            this.channel.on("modal:shown", this.activateScroll.bind(this), this);

            this.collection = new ParticipantsCollection();
        },

        getParticipants: function() {

            var that = this;

            $.get(
                App.Config.get("gateway") + "/participants/" + App.Meeting.get('id')
            ).success(function (participants) {

                $.each(participants, function(i,item) {
                    that.collection.add(new Participant(item));
                });
                that.render();

            });
        },

        activateScroll: function(){
            this.trigger("CustomScrollbarBehavior:init", {
                $el: this.$el
            });
        },

        onShow: function () {
            this.getParticipants();
        },

        onDestroy: function() {
            this.channel.off(null, null, this);
        }
    });

    return ContentMeetingParticipantsView;
});
