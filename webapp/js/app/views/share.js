define("views/share", function(require) {
    "use strict";

    var Radio                   = require("backbone.radio"),
        i18n                    = require("i18n"),
        $                       = require("jquery"),
        BaseLayoutView          = require("views/base"),
        ValidationBehavior      = require("views/behaviors/validationBehavior"),
        ShareTemplate           = require("hbs!templates/share"),
        ShareView;

    ShareView = BaseLayoutView.extend({
        template: ShareTemplate,
        ui:{
            'form': '#share-form',
            'message': '.message',
            'back': '.js-back-waitingRoom',
            'submit': '.btn'
        },
        events:{
            'submit @ui.form': 'shareCase',
            'click @ui.back': 'back'
        },
        behaviors: {
            ValidationBehavior: {
                behaviorClass: ValidationBehavior
            }
        },

        initialize: function() {
            this.channel    = Radio.channel("App");
        },

        onShow: function() {
            this.trigger("ValidationBehavior:initFormValidation", {
                formId: "#share-form",
                ignoreTitle: true,
                focusInvalid: true,
                ignore: '.ignore',
                rules: {
                    'name': {
                        required: true
                    },
                    'message': {
                        required: true
                    },
                    'email': {
                        required: true
                    }
                }
            });
        },

        onBeforeShow: function() {
            $("body").addClass("share");
        },

        onDestroy: function() {
            $("body").removeClass("share");
        },

        shareCase: function(e) {
            e.preventDefault();

            var that = this;

            $.when(this.send(e)).then(function(){
                that.channel.request("track:event", {
                    category: 'Global',
                    action: 'E-mail share',
                    label: ( !!App.consultation && !!App.consultation.get("name") ? App.consultation.get("name") : 'E-mail share' )
                });

                setTimeout(function(){
                    that.channel.trigger("screen:share:save");
                }, 2000);
            });
        },

        send: function(e){
            var promise = new $.Deferred(),
                that = this,
                $form,
                formData,
                data;

            $form = $(e.target);
            formData = $form.serializeArray();
            data = _.object(_.pluck(formData, 'name'), _.pluck(formData, 'value'));

            data.subject = i18n.t("share.subject");

            this.ui.submit.attr("disabled", "disabled");

            $.ajax({
                url: App.Config.get("gateway") + "/share",
                data: data,
                method: "POST"
            }).done(function (e) {
                that.ui.message.html(i18n.t(e.message));
                promise.resolve();
            }).fail(function (e) {
                that.ui.submit.removeAttr("disabled");
                that.ui.message.html(i18n.t(e.message));
                promise.reject();
            });

            return promise;
        },

        back: function(e) {
            e.preventDefault();

            this.channel.trigger("screen:share:save");
        }
    });

    return ShareView;
});
