define("views/main", function(require) {
    "use strict";

    var $               = require("jquery"),
        i18n            = require("i18n"),
        BaseLayoutView  = require("views/base"),
        MainTemplate    = require("hbs!templates/main"),
        MainView;

    MainView = BaseLayoutView.extend({
        className: "centred",
        template: MainTemplate,
        regions: {
            'header': '#header',
            'content': '#content',
            'footer': '#footer'
        },
        onShow: function() {
            $("body").addClass("lang-"+i18n.language);
        }
    });

    return MainView;
});
