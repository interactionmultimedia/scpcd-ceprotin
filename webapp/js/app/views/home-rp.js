define("views/home-rp", function(require) {
    "use strict";

    var $               = require("jquery"),
        BaseLayoutView  = require("views/base"),
        HomeRPTemplate  = require("hbs!templates/home-rp"),
        HomeRPView;

    HomeRPView = BaseLayoutView.extend({
        template: HomeRPTemplate,
        ui: {
            'btn': 'li a'
        },
        events: {
            'click @ui.btn': 'choice'
        },
        choice: function(e) {
            e.preventDefault();

            var url = $(e.currentTarget).attr("href");
            App.router.navigate(url, {trigger: true});
        },
        onBeforeShow: function() {
            $("body")
                .removeClass("loading")
                .addClass("rp-01");
        },
        onDestroy: function() {
            $("body").removeClass("rp-01");
        }
    });

    return HomeRPView;
});
