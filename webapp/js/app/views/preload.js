define("views/preload", function (require) {
    "use strict";

    var Radio               = require("backbone.radio"),
        Marionette          = require("marionette"),
        Howler              = require("howler"),
        PreloadTemplate     = require("hbs!templates/preload"),
        Loader              = require("libs/loader"),
        PreloadView;

    PreloadView = Marionette.LayoutView.extend({
        template: PreloadTemplate,
        id: "preload",
        data: {},
        initialize: function (options) {
            App.loader      = new Loader();

            this.channel    = Radio.channel("App");
            this.path       = options.path;
            this.context    = options.context;
        },

        onRender: function () {
            $("body").addClass("loading");
            var that = this,
                progress = 0,
                promises = [],
                promises_count;

            require(["text!" + this.path], function(data) {
                var assets = JSON.parse(data);
                _.each(assets, function(a){
                    if(a.filetype === "mp3"){
                        // promises.push(App.loader.audio(a.location));
                    } else {
                        promises.push(App.loader.asset(a.location));
                    }
                });

                promises_count = promises.length;

                $.when.apply($, promises).progress(function () {
                    progress++;
                    that.setProgress(Math.floor(100 * progress / promises_count));
                }).then(function (e) {
                    progress++;
                    window.setTimeout(function () {
                        that.channel.trigger("preload:end", that.context);
                    }, 500);
                });
            }, function(error){
                that.channel.trigger("preload:end", that.context);
            });
        },
        serializeData: function () {
            var viewData = {
                data: this.data
            };
            return _.extend(viewData, Marionette.ItemView.prototype.serializeData.apply(this, arguments));
        },

        setProgress: function (persentage) {
            $('#loading').text(persentage + "%");
        }
    });



    return PreloadView;
});
