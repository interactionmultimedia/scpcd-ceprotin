define("views/language-select", function(require) {
    "use strict";

    var Marionette                  = require("marionette"),
        Radio                       = require("backbone.radio"),
        _                           = require("underscore"),
        i18n                        = require("i18n"),
        LanguageSelectOptionView    = require("views/language-select-option"),
        LanguageSelectTemplate      = require("hbs!templates/language-select"),
        LanguageSelectView;

    LanguageSelectView = Marionette.CompositeView.extend({
        className: "language-select",
        template: LanguageSelectTemplate,
        childView: LanguageSelectOptionView,
        childViewContainer: '#language-select-options',
        ui: {
            'languageCurrent': '.active-lang'
        },
        events: {
            'click @ui.languageCurrent': 'toggleLanguageSelect'
        },
        childEvents: {
            'language:select': 'selectLanguage'
        },
        initialize: function() {
            this.collection = App.Languages.getActiveLanguages();

            this.setCurrentLanguage();

            this.channel    = Radio.channel("App");
        },
        setCurrentLanguage: function() {
            this.model      = this.collection.getLanguageByCode(i18n.language);

            if( this.model ) {
                this.data   = {
                    currentLanguageLabel: this.model.get("label")
                };
            }
        },
        onBeforeRender: function() {
            if( this.model ) {
                this.$el.attr('class', this.className).addClass(this.model.get("classNameCurrent"));
            }
        },
        toggleLanguageSelect: function(e) {
            e.preventDefault();

            this.$el.toggleClass('is-open');
        },
        selectLanguage: function(args) {
            var that   = this;

            if( i18n.language === args.model.get("code") ) {
                return;
            }

            i18n.changeLanguage(args.model.get("code"), function() {
                that.channel.trigger("app:change:language");
            });
        }
    });

    return LanguageSelectView;
});
