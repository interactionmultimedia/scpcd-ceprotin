define("views/consultation", function (require) {
    "use strict";

    var Radio = require("backbone.radio"),
        $ = require("jquery"),
        _ = require("underscore"),
        i18n = require("i18n"),
        BaseLayoutView = require("views/base"),
        AudioPlayerView = require("views/consultation/audio-player"),
        ModalLayoutView = require("views/modal/modal"),
        StepsView = require("views/consultation/steps/content"),
        MedicalFilesView = require("views/consultation/medicalFiles/content"),
        HeaderWithNavigationView = require("views/consultation/medicalFiles/headerWithNavigation"),
        ContentWithNavigationView = require("views/consultation/medicalFiles/contentWithNavigation"),
        PatientProfileView = require("views/consultation/medicalFiles/patientProfile"),
        MedicalHistoriesView = require("views/consultation/medicalFiles/medicalHistories"),
        OngoingTreatmentView = require("views/consultation/medicalFiles/ongoingTreatment"),
        WayOfLivingView = require("views/consultation/medicalFiles/wayOfLiving"),
        NeonatSymptomsView = require("views/consultation/medicalFiles/neonatSymptoms"),
        PatientHistoryView = require("views/consultation/medicalFiles/patientHistory"),
        SpecialistsReportView = require("views/consultation/medicalFiles/specialistsReport"),
        ExamsResultsView = require("views/consultation/medicalFiles/examsResults"),
        LaboratoryResultsView = require("views/consultation/medicalFiles/laboratoryResults"),
        FamilyBackgroundView = require("views/consultation/medicalFiles/familyBackground"),
        ExamsResultsComplementaryView = require("views/consultation/medicalFiles/examsResultsComplementary"),
        QuitView = require("views/consultation/quit"),
        FinishConsultationView = require("views/meeting/finishConsultation"),
        FinishMeetingView = require("views/meeting/finishMeeting"),
        ClinicalExamView = require("views/consultation/clinicalExam"),
        SpeechesView = require("views/consultation/speeches"),
        FeatureImage = require("libs/featureImage"),
        HeaderWithTitleView = require("views/modal/headerWithTitle"),
        ContentAnnexeView = require("views/modal/contentAnnexe"),
        ContentQuizView = require("views/modal/contentQuiz"),
        ContentTextView = require("views/modal/contentText"),
        ContentMeetingParticipantsView = require("views/modal/contentMeetingParticipants"),
        BreadcrumbsView = require("views/consultation/breadcrumbs"),
        HeaderFeedbackView = require("views/consultation/feedback/headerWithNavigation"),
        ContentFeedbackView = require("views/consultation/feedback/content"),
        FooterFeedbackView = require("views/consultation/feedback/footer"),
        HeaderGlobalConclusionView = require("views/consultation/globalConclusion/header"),
        ContentGlobalConclusionView = require("views/consultation/globalConclusion/content"),
        FooterGlobalConclusionView = require("views/consultation/globalConclusion/footer"),
        ConsultationTemplate = require("hbs!templates/consultation"),
        AvatarsCollection = require("collections/avatars"),
        ConsultationView;

    ConsultationView = BaseLayoutView.extend({
        template: ConsultationTemplate,
        className: "",
        isChangingMedicalFile: false,

        ui: {
            'btnConsultationSteps': '.btn-consultation-steps:first',
            'btnParticipants': '.meeting-participants',
            'patients': '.gifs-animated',
            'btnFinish': '.btn-finish',
            'btnFinishMeeting': '.btn-finish-meeting',
            'menu': '.consultation-menu-content'
        },

        events: {
            'click @ui.btnConsultationSteps': 'showConsultationSteps',
            'click @ui.btnParticipants': 'showParticipants',
            'click @ui.btnFinish': 'finishConsultationModal',
            'click @ui.btnFinishMeeting': 'finishMeetingModal'
        },

        regions: {
            'btnQuitStep': '#btn-quit-step',
            'audioPlayer': '#audio-player',
            'speeches': '#speeches',
            'breadcrumbs': '#breadcrumbs',
            'menu': '#consultation-menu-content',
            'scene': '.scene'
        },

        data: {
            menuShowed: false
        },

        initialize: function () {
            this.channel = Radio.channel("App");

            this.channel.on("step:select", this.consultationStepChange.bind(this), this);
            this.channel.on("medicalFiles:select", this.medicalFileChange.bind(this), this);
            this.channel.on("medicalFiles:guide:ended", this.showGuideElement.bind(this, "btnClinicalExam"), this);
            this.channel.on("medicalFiles:guide:ended", this.hasSeenAllMedicalFiles.bind(this), this);
            this.channel.on("show:modal:quit", this.showModalQuit.bind(this), this);
            this.channel.on("consultation:quit", this.quitConsultation.bind(this), this);
            this.channel.on("consultation:back", this.backConsultation.bind(this), this);
            this.channel.on("consultation:speech:stepEnded", this.quitStep.bind(this), this);
            this.channel.on("consultation:speech:stepEnded", this.resetAnimations.bind(this), this);
            this.channel.on("consultation:speech:stepEnded", this.removeGuideElement.bind(this, "speechStep"), this);
            this.channel.on("consultation:ended", this.showFeedback.bind(this), this);
            this.channel.on("consultation:finish:back", this.finishConsultationBack.bind(this), this);
            this.channel.on("consultation:finish", this.finishConsultation.bind(this), this);
            this.channel.on("meeting:finish", this.finishMeeting.bind(this), this);
            this.channel.on("step:quit", this.quitStep.bind(this), this);
            this.channel.on("modal:medicalFiles:back", this.showMedicalFiles.bind(this), this);
            this.channel.on("modal:medicalFiles:previous", this.showPreviousMedicalFiles.bind(this), this);
            this.channel.on("modal:medicalFiles:next", this.showNextMedicalFiles.bind(this), this);
            this.channel.on("modal:feedback:close", this.hideFeedback.bind(this), this);
            this.channel.on("modal:feedback:hideDetails", this.enableFeedback.bind(this), this);
            this.channel.on("modal:feedback:showDetails", this.disableFeedback.bind(this), this);
            this.channel.on("modal:feedback:end", this.manageEndClinicalCase.bind(this), this);
            this.channel.on("modal:globalConclusion:end", this.hideGlobalConclusion.bind(this), this);
            this.channel.on("modal:globalConclusion:share", this.showShare.bind(this), this);
            this.channel.reply("scene:show", this.showScene.bind(this));
            this.channel.reply("scene:reset", this.resetScene.bind(this));
            this.channel.on("speech:idle", this.showIdleAvatars.bind(this));
            this.channel.on("speech:talk", this.showSpeakingAvatar.bind(this));
            this.channel.on("consultation:speech:activate", this.activateAvatars.bind(this));
            this.channel.on("consultation:speech:deactivate", this.deactivateAvatars.bind(this));
        },

        /*
         *
         */
        showAvatarAnimation: function (avatar, mode, show) {
            var caseId = App.consultation.get("caseId"),
                consultationId = App.consultation.get("consultationId"),
                folderName = "c" + caseId + "_c" + consultationId,
                src = "/data/assets/" + folderName + "/" + avatar.get(mode).get('url'),
                $img = $(this.ui.patients).find('[data-id="' + avatar.get('id') + '"][data-mode="' + mode + '"]');

            if ($img.length == 0) {
                $img = $('<img>').attr('data-id', avatar.get('id'))
                    .attr('data-mode', mode).attr('src', src)
                    .addClass('gif-animated');
                if (show === false) {
                    $img.addClass('hidden');
                }
                $(this.ui.patients).append($img);
            } else {
                return;
            }

            // let's calc real position
            var computedStyle = getComputedStyle(document.body),
                image = new Image(),
                src = computedStyle.backgroundImage.replace(/url\((['"])?(.*?)\1\)/gi, '$2');
            image.onload = function () {
                new FeatureImage(document.body, avatar.get(mode), this.width, this.height);
            };
            image.src = src;
            avatar.get(mode).on('change', function () {
                $img.css({
                    width: this.get('width') + 'px',
                    height: this.get('height') + 'px',
                    top: this.get('top') + 'px',
                    left: this.get('left') + 'px',
                });
            });
            avatar.on('change:active', function () {
                if (this.get('active') && mode == 'idle') {
                    $img.removeClass('hidden');
                } else {
                    $img.addClass('hidden');
                }
            });
        },

        changeAvatarState: function (avatar, mode) {
            var $img = $(this.ui.patients).find('[data-id="' + avatar.get('id') + '"][data-mode="' + mode + '"]');
            $img.removeClass('hidden');
            var otherMode = (mode == 'idle') ? 'speaking' : 'idle';
            var $otherImg = $(this.ui.patients).find('[data-id="' + avatar.get('id') + '"][data-mode="' + otherMode + '"]');
            $otherImg.addClass('hidden');
        },

        showIdleAvatars: function () {
            var that = this;
            this.avatarsCollection.each(function (avatar) {
                if(avatar.get('active')) {
                    that.changeAvatarState(avatar, 'idle');
                }
            });
        },

        activateAvatars: function (ids) {
            var that = this;
            _.each(ids, function (id) {
                var model = that.avatarsCollection.get(id);
                if(model instanceof Backbone.Model) {
                    model.set('active', true);
                }
                //that.showAvatarAnimation(model, 'idle', true);
            });
        },

        deactivateAvatars: function (ids) {
            var that = this;
            _.each(ids, function (id) {
                var model = that.avatarsCollection.get(id);
                model.set('active', false);
            });
        },

        showSpeakingAvatar: function (avatar) {
            var that = this;
            that.changeAvatarState(this.avatarsCollection.get(avatar.get('id')), 'speaking');
        },

        resetAnimations: function () {
            var that = this;
            var avatars = App.consultation.getStep("ChatIntro").getSpeechesAvatars();
            _.each(avatars, function (avatar) {
                var model = that.avatarsCollection.get(avatar.id);
                if (model instanceof Backbone.Model) {
                    model.set('active', avatar.active); // reset original state
                    if(avatar.active) {
                        that.changeAvatarState(model, 'idle');
                    }
                }
            });
        },

        /**
         * Affichage de la fenêtre des étapes de consultation
         */
        showConsultationSteps: function (e) {
            if (e) {
                e.preventDefault();
            }

            this.ui.menu.toggleClass('show');
            this.data.menuShowed = this.ui.menu.hasClass('show');
            if (this.data.menuShowed) {
                this.removeGuideElement('btnConsultationSteps');
            }
        },

        /**
         * A la sélection d'une étape de consultation
         */
        consultationStepChange: function (args) {
            var moduleName = args.moduleName;

            if (this.popupConsultation) {
                this.channel.once("modal:hidden", this.applyStep.bind(this, moduleName));

                this.popupConsultation.hideModal();
            } else {
                this.applyStep(moduleName);
            }
        },

        /**
         *
         */
        applyStep: function (moduleName) {

            if (this.currentStep == moduleName) {
                this.channel.trigger('step:quit', moduleName);
                this.currentStep = null;
                //this.ui.menu.find('button').removeClass('active');
                return;
            } else {
                this.quitStep(this.currentStep);
            }
            this.currentStep = moduleName;

            switch (moduleName) {
                case "ChatIntro":
                    /*this.showChildView('btnQuitStep', new BtnQuitStepView({
                        moduleName: moduleName,
                        data: {
                            title: i18n.t("consultation.consultationSteps.chatIntro.quit")
                        }
                    }));*/
                    this.removeGuideElement("btnChatIntro");
                    this.showChildView('audioPlayer', new AudioPlayerView());
                    this.showChildView('speeches', new SpeechesView({
                        moduleName: moduleName
                    }));
                    break;

                case "ClinicalExaminationModule":
                    this.channel.request("scene:show", {
                        view: new ClinicalExamView()
                    });
                    this.removeGuideElement('btnClinicalExam');
                    break;

                case "Diagnosis":
                    this.showModalQuiz(moduleName, "consultation.consultationSteps.diagnosis.title");
                    break;

                case "PrescriptionExams":
                    this.showModalQuiz(moduleName, "consultation.consultationSteps.prescriptionExams.title");
                    break;

                case "Other":
                    this.showModalQuiz(moduleName, "consultation.consultationSteps.patientManagement.title");
                    break;

                case "MedicalTreatmentModule":
                    this.showModalQuiz(moduleName, "consultation.consultationSteps.treatmentPrescriptions.title");
                    break;

                case "ResponseCustomModule":
                    this.showModalText(moduleName, "consultation.consultationSteps.response.title");
                    break;

                case "MonitoringProtocol":
                    this.showModalQuiz(moduleName, "consultation.consultationSteps.followUp.title");
                    break;

                case "AppendicesModule":
                    this.showAnnexes();
                    break;

                case "MedicalFiles":
                    this.showMedicalFiles();
                    break;
            }
        },

        /**
         * Méthode générique permettant d'afficher un quiz
         * @param  {String} moduleName Nom du module à charger
         * @param  {String} title      Clé de la chaîne de traduction
         */
        showModalQuiz: function (moduleName, slugTitle) {
            var that = this,
                headerView,
                contentView;

            headerView = new HeaderWithTitleView({
                data: {
                    title: i18n.t(slugTitle)
                }
            });

            var quiz = App.consultation.get("quizzes").getQuizByModuleName(moduleName);
            var step = App.consultation.getStep(moduleName);
            //quiz.resetSelection();

            contentView = new ContentQuizView({
                quiz: quiz
            });

            this.popupQuiz = new ModalLayoutView({
                className: "overlay overlay-quiz overlay-with-padding " + step.get('className'),
                headerView: headerView,
                contentView: contentView,
                footerView: null
            });

            this.channel.once("modal:hidden", function (moduleName) {
                //that.showConsultationSteps();
                this.quitStep(moduleName);
            }.bind(this, moduleName));

            this.popupQuiz.setHeaderClassName("overlay-header clearfix");
            this.popupQuiz.showModal();
        },

        showModalText: function (moduleName, slugTitle) {
            var that = this,
                data = App.consultation.getTextByModuleName(moduleName);

            this.popupText = new ModalLayoutView({
                className: "overlay overlay-consultation-steps overlay-with-padding",
                headerView: new HeaderWithTitleView({
                    data: {
                        title: i18n.t(slugTitle)
                    }
                }),
                contentView: new ContentTextView({
                    data: {
                        list: data.ModuleVO.content.ContentVO.contentList.ContentItemVO
                    }
                }),
                footerView: null
            });

            this.channel.once("modal:hidden", function () {
                that.showConsultationSteps();
            });

            this.popupText.setHeaderClassName("overlay-header clearfix");
            this.popupText.showModal();
        },

        /**
         *
         */
        showMedicalFiles: function (e) {
            var className = ModalLayoutView.prototype.className,
                that = this;

            if (e) {
                e.preventDefault();
            }

            this.removeGuideElement("btnMedicalFiles");
            this.isChangingMedicalFile = false;

            if (this.popupItemMedicalFile) {
                this.popupItemMedicalFile.hideModal();
                delete this.popupItemMedicalFile;
            }

            this.popupMedicalFile = new ModalLayoutView({
                className: className + " overlay-medical-record",
                headerView: new HeaderWithTitleView({
                    data: {
                        title: i18n.t("consultation.medicalFiles.title")
                    }
                }),
                contentView: new MedicalFilesView(),
                footerView: null
            });

            this.popupMedicalFile.channel.once("modal:hidden", function () {
                that.channel.trigger('step:quit', 'MedicalFiles');
            });

            this.popupMedicalFile.setHeaderClassName("overlay-header clearfix");
            this.popupMedicalFile.showModal();
        },

        /**
         *
         */
        showPreviousMedicalFiles: function (index) {
            this.showNeighborMedicalFiles(index - 1);
        },

        /**
         *
         */
        showNextMedicalFiles: function (index) {
            this.showNeighborMedicalFiles(index + 1);
        },

        /**
         *
         */
        showNeighborMedicalFiles: function (index) {
            var availableMedicalFiles = App.consultation.getAvailableMedicalFiles(),
                medicalFilesModule = availableMedicalFiles.at(index);

            this.medicalFileChange({
                moduleName: medicalFilesModule.get("moduleName"),
                moduleIndex: index
            });
        },

        /**
         *
         */
        medicalFileChange: function (args) {
            var availableMedicalFiles = App.consultation.getAvailableMedicalFiles(),
                moduleName = args.moduleName,
                moduleIndex = args.moduleIndex || availableMedicalFiles.getModuleIndex(moduleName);
            this.isChangingMedicalFile = true;
            if (this.popupMedicalFile) {
                this.channel.once("modal:hidden", this.applyMedicalFile.bind(this, availableMedicalFiles, moduleName, moduleIndex));

                this.popupMedicalFile.hideModal();
            } else {
                this.applyMedicalFile(availableMedicalFiles, moduleName, moduleIndex);
            }
        },

        /**
         *
         */
        applyMedicalFile: function (availableMedicalFiles, moduleName, moduleIndex) {
            var contentView,
                contentItemView,
                headerView,
                className,
                title;

            switch (moduleName) {
                case "HistoryOfDiseaseModule":
                    contentItemView = new PatientHistoryView();
                    className = "overlay overlay-patient-history overlay-with-padding";
                    title = i18n.t("consultation.medicalFiles.patientHistory.title");
                    break;

                case "PatientProfileModule":
                    contentItemView = new PatientProfileView();
                    className = "overlay overlay-patient-profile overlay-with-padding";
                    title = i18n.t("consultation.medicalFiles.patientProfile.title");
                    break;

                case "MedicalHistoriesModule":
                    contentItemView = new MedicalHistoriesView();
                    className = "overlay overlay-medical-history overlay-with-padding overlay-with-scroll";
                    title = i18n.t("consultation.medicalFiles.medicalHistory.title");
                    break;

                case "CurrentTreatmentModule":
                    contentItemView = new OngoingTreatmentView();
                    className = "overlay overlay-ongoing-treatment overlay-with-padding";
                    title = i18n.t("consultation.medicalFiles.ongoingTreatment.title");
                    break;

                case "LifestyleModule":
                    contentItemView = new WayOfLivingView();
                    className = "overlay overlay-way-of-life overlay-with-padding";
                    title = i18n.t("consultation.medicalFiles.wayOfLiving.title");
                    break;

                case "ResultsComplementaryExamsCustomModule":
                    contentItemView = new ExamsResultsComplementaryView();
                    className = "overlay overlay-results-complementary-exams overlay-with-padding";
                    title = i18n.t("consultation.medicalFiles.resultsComplementaryExams.title");
                    break;

                case "SpecialistsConsultationReportCustomModule":
                    contentItemView = new SpecialistsReportView();
                    className = "overlay overlay-specialist-consultation-reports overlay-with-padding";
                    title = i18n.t("consultation.medicalFiles.specialistsConsultationReport.title");
                    break;

                case "ExamsResultsModule":
                    contentItemView = new ExamsResultsView();
                    className = "overlay overlay-results-complementary-exams overlay-with-padding";
                    title = i18n.t("consultation.medicalFiles.examsResults.title");
                    break;

                case "LaboratoryResultsModule":
                    contentItemView = new LaboratoryResultsView();
                    className = "overlay overlay-results-complementary-exams overlay-with-padding";
                    title = i18n.t("consultation.medicalFiles.laboratoryResults.title");
                    break;

                case "FamilyBackgroundModule":
                    contentItemView = new FamilyBackgroundView();
                    className = "overlay overlay-family-background overlay-with-padding";
                    title = i18n.t("consultation.medicalFiles.familyBackground.title");
                    break;

                case "NeonatSymptomsModule":
                    contentItemView = new NeonatSymptomsView();
                    className = "overlay overlay-neonat-symptoms overlay-with-padding";
                    title = i18n.t("consultation.medicalFiles.neonatSymptoms.title");
                    break;
            }

            if (!!contentItemView) {
                headerView = new HeaderWithNavigationView({
                    data: {
                        title: title
                    }
                });

                contentView = new ContentWithNavigationView({
                    moduleIndex: moduleIndex,
                    moduleIndexMax: availableMedicalFiles.length - 1,
                    contentView: contentItemView
                });

                if (!!this.popupItemMedicalFile) {
                    this.popupItemMedicalFile.setClassName(className);
                    this.popupItemMedicalFile.showChildView("header", headerView);
                    this.popupItemMedicalFile.showChildView("content", contentView);
                } else {
                    this.popupItemMedicalFile = new ModalLayoutView({
                        headerView: headerView,
                        contentView: contentView,
                        footerView: null
                    });

                    this.popupItemMedicalFile.setClassName(className);
                    this.popupItemMedicalFile.setHeaderClassName("overlay-header clearfix");
                    this.popupItemMedicalFile.showModal();
                }
                this.popupItemMedicalFile.channel.once('modal:hidden', function () {
                    this.quitStep('MedicalFiles');
                }, this);
                this.isChangingMedicalFile = false;
            }
        },

        /**
         * Affichage de la popup "Quit"
         */
        showModalQuit: function () {
            this.popupConsultation.showChildView("content", new QuitView());
        },

        /**
         * Retour à la salle d'attente
         */
        quitConsultation: function () {
            if (this.popupConsultation) {
                this.popupConsultation.hideModal();
                this.popupConsultation.destroy();
            }

            // tracking du temps passé dans la consultation
            var startTimeConsultation = App.SimulationState.get("startTimeConsultation"),
                currentTimeConsultation = new Date().getTime(),
                timeElapsed = null;

            if (startTimeConsultation) {
                timeElapsed = currentTimeConsultation - startTimeConsultation;

                this.channel.request("track:event", {
                    category: 'Consultation',
                    action: 'Temps passé',
                    label: App.consultation.get("name"),
                    value: timeElapsed
                });

                this.channel.request("track:event", {
                    category: 'Consultation',
                    action: 'Fin',
                    label: App.consultation.get("name")
                });
            }

            this.channel.trigger("consultation:ended");
        },

        /**
         * Fermeture de la fenêtre "Quit"
         */
        backConsultation: function () {
            if (this.popupConsultation) {
                this.popupConsultation.hideModal();

                this.showConsultationSteps();
            }
        },

        /*
         * En mode RP, l'animateur peut mettre fin à la consultation
         */
        finishConsultationModal: function (e) {
            e.preventDefault();

            if (!App.Config.isRP() || !App.Trainer) {
                return;
            }

            var className = ModalLayoutView.prototype.className;

            this.popupConsultation = new ModalLayoutView({
                className: className + " overlay-consultation",
                contentView: new FinishConsultationView()
            });
            this.popupConsultation.showModal();
        },

        /*
         * En mode RP, l'animateur peut mettre fin au meeting
         */
        finishMeetingModal: function (e) {
            e.preventDefault();

            if (!App.Config.isRP() || !App.Trainer) {
                return;
            }

            var className = ModalLayoutView.prototype.className;

            this.popupConsultation = new ModalLayoutView({
                className: className + " overlay-consultation",
                contentView: new FinishMeetingView()
            });
            this.popupConsultation.showModal();
        },

        /*
         *
         */
        finishConsultation: function () {
            App.router.navigate("waiting-room", {
                trigger: true
            });
        },

        finishMeeting: function () {
            $.when(App.Meeting.finishMeeting()).then(function () {
                App.router.navigate("meeting-end", {
                    trigger: true
                });
            });
        },

        /*
         *
         */
        finishConsultationBack: function () {
            if (this.popupConsultation) {
                this.popupConsultation.hideModal();
            }
        },

        /**
         * Quitte l'étape sélectionnée et retourne à l'écran de consultation
         * les comportements spécifiques aux étapes sont gérés dans les vues
         * des étapes
         */
        quitStep: function (moduleName) {
            switch (moduleName) {
                case "ChatIntro":
                    this.getRegion("btnQuitStep").empty();
                    this.getRegion("speeches").empty();
                    this.getRegion("audioPlayer").empty();
                    this.removeGuideElement('btnChatIntro');
                    this.removeGuideElement('speechStep');
                    this.resetAnimations();
                    this.currentStep = null;
                    break;
                case 'ClinicalExaminationModule':
                    this.currentStep = null;
                    this.channel.request("scene:reset");
                    this.channel.trigger("clinicalExam:quit");
                    break;
                case 'MedicalFiles':
                    this.currentStep = null;
                    if (!App.SimulationState.hasSeenAllMedicalFiles() && !this.isChangingMedicalFile && !App.SimulationState.hasSeenGuideElement('btnClinicalExam')) {
                        App.SimulationState.removeGuideElement('btnMedicalFiles');
                        this.showGuideElement('btnMedicalFiles');
                    }
                    break;
                default:
                    this.currentStep = null;
                    break;
            }
            this.channel.trigger('quitted:step', moduleName);
        },

        hasSeenAllMedicalFiles: function () {
            App.SimulationState.seenAllMedicalFiles();
        },

        showAnnexes: function (e) {
            var that = this;
            if (e && e.preventDefault) {
                e.preventDefault();
            }

            this.popupAnnexes = new ModalLayoutView({
                className: "overlay overlay-annexe overlay-with-padding overlay-with-scroll",
                headerView: new HeaderWithTitleView({
                    data: {
                        title: i18n.t("consultation.annexes.title")
                    }
                }),
                contentView: new ContentAnnexeView({
                    collection: App.consultation.get("annexes")
                }),
                footerView: null
            });

            this.popupAnnexes.channel.once("modal:close", function () {
                that.channel.trigger('step:quit', 'AppendicesModule');
            });

            this.popupAnnexes.setHeaderClassName("overlay-header clearfix");
            this.popupAnnexes.showModal();
        },

        showParticipants: function (e) {
            e.preventDefault();

            if (!App.Config.isRP()) {
                return;
            }

            this.popupParticipants = new ModalLayoutView({
                className: "overlay overlay-participants overlay-with-padding overlay-with-scroll",
                headerView: new HeaderWithTitleView({
                    data: {
                        title: i18n.t("meeting.participants")
                    }
                }),
                contentView: new ContentMeetingParticipantsView(),
                footerView: null
            });

            this.popupParticipants.showModal();
        },

        showFeedback: function () {
            if (App.Config.isRP() && !!App.Participant) {
                return;
            }

            this.popupFeedback = new ModalLayoutView({
                className: "overlay overlay-consultation-synthesis overlay-with-padding",
                headerView: new HeaderFeedbackView(),
                contentView: new ContentFeedbackView(),
                footerView: new FooterFeedbackView()
            });
            this.popupFeedback.setHeaderClassName("overlay-header clearfix");
            this.popupFeedback.setFooterClassName("overlay-site-footer clearfix");
            this.popupFeedback.showModal();
        },

        /**
         * Ferme la popup feedback et retourne à la waiting room
         */
        hideFeedback: function () {
            this.hidePopupFeedback();
        },

        hideGlobalConclusion: function () {
            if (this.popupGlobalConclusion) {
                this.popupGlobalConclusion.hideModal();
                this.popupGlobalConclusion.destroy();
                this.channel.trigger('screen:globalConclusion:exit');
            }
        },

        showShare: function () {
            if (App.Config.isRP()) {
                return;
            }

            if (this.popupGlobalConclusion) {
                this.popupGlobalConclusion.hideModal();
                this.popupGlobalConclusion.destroy();
            }
            App.router.navigate("share", {
                trigger: true
            });
        },

        /**
         * Ferme la popup feedback
         */
        hidePopupFeedback: function () {
            if (this.popupFeedback) {
                this.popupFeedback.hideModal();
                this.popupFeedback.destroy();
            }
        },

        manageEndClinicalCase: function () {
            this.hidePopupFeedback();

            if (!!App.consultation.get("synthesis")) {
                this.showGlobalConclusion();
            } else {
                App.router.navigate("share", {
                    trigger: true
                });
            }
        },

        sendFeedback: function () {
            var promise = $.Deferred();

            if (!!App.SimulationState.get("email")) {
                var quizzes = [];

                App.Consultations.each(function (consultation) {
                    consultation.cleanConsultationForFeedback();
                    quizzes.push(consultation);
                });

                $.ajax({
                    url: App.Config.get("gateway") + "/feedback",
                    data: {
                        email: App.SimulationState.get("email"),
                        subject: i18n.t("formFeedback.subject"),
                        message: JSON.stringify(quizzes)
                    },
                    method: "POST"
                }).done(function (e) {
                    promise.resolve();
                }).fail(function (e) {
                    promise.reject();
                });
            } else {
                promise.resolve();
            }

            return promise.promise();
        },

        showGlobalConclusion: function () {
            var that = this;

            this.hidePopupFeedback();

            $.when([this.sendFeedback()]).always(function () {

                that.channel.request("track:event", {
                    category: 'Consultation',
                    action: 'E-mail statistique envoyé à l\'utilisateur',
                    label: App.consultation.get("name")
                });

                that.popupGlobalConclusion = new ModalLayoutView({
                    className: "overlay overlay-global-conclusion overlay-with-padding overlay-with-scroll",
                    headerView: new HeaderGlobalConclusionView(),
                    contentView: new ContentGlobalConclusionView(),
                    footerView: new FooterGlobalConclusionView()
                });
                that.popupGlobalConclusion.setHeaderClassName("overlay-header clearfix");
                that.popupGlobalConclusion.setFooterClassName("overlay-site-footer clearfix");
                that.popupGlobalConclusion.showModal();
            });
        },

        enableFeedback: function () {
            if (this.popupFeedback) {
                this.popupFeedback.enable();
            }
        },

        disableFeedback: function () {
            if (this.popupFeedback) {
                this.popupFeedback.disable();
            }
        },

        showGuideElement: function (which) {
            switch (which) {
                case "btnConsultationSteps":
                    if (!App.SimulationState.hasSeenGuideElement('btnConsultationSteps')) {
                        this.ui.btnConsultationSteps.addClass("lookatme");
                    }
                    break;

                case "btnMedicalFiles":
                    this.getChildView('menu').showGuideElement('btnMedicalFiles');
                    break;

                case "btnChatIntro":
                    this.getChildView('menu').showGuideElement('btnChatIntro');
                    break;

                case "btnClinicalExam":
                    this.getChildView('menu').showGuideElement('btnClinicalExam');
                    break;
            }
        },

        removeGuideElement: function (which) {
            switch (which) {
                case "btnConsultationSteps":
                    this.ui.btnConsultationSteps.removeClass("lookatme");
                    App.SimulationState.addGuideElement('btnConsultationSteps');
                    if (!App.SimulationState.hasSeenGuideElement('btnChatIntro')) {
                        this.showGuideElement('btnChatIntro');
                    }
                    break;

                case "btnChatIntro":
                    this.getChildView('menu').removeGuideElement('btnChatIntro');
                    App.SimulationState.addGuideElement('btnChatIntro');
                    break;

                case "speechStep":
                    if (!App.SimulationState.hasSeenGuideElement('btnMedicalFiles')) {
                        this.showGuideElement('btnMedicalFiles');
                    }
                    break;
                case "btnMedicalFiles":
                    this.getChildView('menu').removeGuideElement('btnMedicalFiles');
                    App.SimulationState.addGuideElement('btnMedicalFiles');
                    break;

                case "btnClinicalExam":
                    this.getChildView('menu').removeGuideElement('btnClinicalExam');
                    App.SimulationState.addGuideElement('btnClinicalExam');
                    break;
            }
        },

        showScene: function (params) {
            this.getRegion('scene').show(params.view);
        },

        resetScene: function () {
            this.getRegion('scene').empty();
            this.getRegion('scene').$el.html('<div class="gifs-animated"></div>');
            this.ui.patients = this.$el.find('.gifs-animated');
            this.setBodyClassName();
        },

        /**
         * Avant le rendu
         *  - détermine si des étapes sont disponibles
         */
        onBeforeRender: function () {
            this.data.hasSteps = App.consultation.hasSteps();
            this.data.isTrainer = (App.Config.isRP() && !!App.Trainer);
        },

        /**
         * Avant l'affichage :
         * - ajout des classes CSS
         */
        onBeforeShow: function () {
            this.setBodyClassName();
            this.showChildView('breadcrumbs', new BreadcrumbsView());
            this.showChildView('menu', new StepsView({
                step: this.currentStep
            }));

            if (this.data.menuShowed) {
                this.ui.menu.addClass('show');
                this.removeGuideElement("btnConsultationSteps");
            } else {
                this.showGuideElement("btnConsultationSteps");
            }
        },


        /**
         * Après le render :
         * - affichage des gifs idle
         */
        setBodyClassName: function () {
            var that = this;
            var environnement = App.consultation.getEnvironnement("Camera-Office"),
                caseId = App.consultation.get("caseId"),
                consultationId = App.consultation.get("consultationId"),
                folderName = "c" + caseId + "_c" + consultationId;
            $("body")
                .removeClass()
                .addClass("consultation no-overlay")
                .css("background-image", "url('/data/assets/" + folderName + "/images/" + environnement.environmentBackground + "')");
            this.avatarsCollection = new AvatarsCollection(App.consultation.getStep("ChatIntro").getSpeechesAvatars());
            this.avatarsCollection.each(function (avatar) {
                that.showAvatarAnimation(avatar, "idle", avatar.get('active'));
                that.showAvatarAnimation(avatar, "speaking", false);
            });
        },

        /**
         * A la destruction :
         * - suppression des classes CSS
         * - suppression des listeners
         */
        onDestroy: function () {
            $("body")
                .removeAttr("style")
                .removeClass("consultation no-overlay");

            if (this.popupConsultation) {
                this.popupConsultation.hideModal();
                this.popupConsultation.destroy();
                this.popupConsultation = null;
            }

            if (this.popupFeedback) {
                this.popupFeedback.hideModal();
                this.popupFeedback.destroy();
                this.popupFeedback = null;
            }

            if (this.popupGlobalConclusion) {
                this.popupGlobalConclusion.hideModal();
                this.popupGlobalConclusion.destroy();
                this.popupGlobalConclusion = null;
            }

            if (this.popupMedicalFile) {
                this.popupMedicalFile.hideModal();
                this.popupMedicalFile.destroy();
                this.popupMedicalFile = null;
            }

            if (this.popupItemMedicalFile) {
                this.popupItemMedicalFile.hideModal();
                this.popupItemMedicalFile.destroy();
                this.popupItemMedicalFile = null;
            }

            if (this.popupAnnexes) {
                this.popupAnnexes.hideModal();
                this.popupAnnexes.destroy();
                this.popupAnnexes = null;
            }

            if (this.popupQuiz) {
                this.popupQuiz.hideModal();
                this.popupQuiz.destroy();
                this.popupQuiz = null;
            }

            if (this.popupText) {
                this.popupText.hideModal();
                this.popupText.destroy();
                this.popupText = null;
            }

            this.channel.off(null, null, this);
        }
    });

    return ConsultationView;
});
