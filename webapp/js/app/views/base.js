define("views/base", function(require) {
    "use strict";

    var Marionette      = require("marionette"),
        _               = require("underscore"),
        BaseLayoutView;

    BaseLayoutView = Marionette.LayoutView.extend({
        initialize: function(options) {
            this.data = options.data ? options.data : {};
        },
        serializeData: function() {
            var viewData = {
                data: this.data
            };

            return _.extend(viewData, Marionette.LayoutView.prototype.serializeData.apply(this, arguments));
        },
        getRandomCode: function(length) {
    
            var str = '';
            var chars ='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
            var charsLen = chars.length;
            if (!length) {
                length = ~~(Math.random() * charsLen);
            }
            for (var i = 0; i < length; i++) {
                str += chars[~~(Math.random() * charsLen)];
            }
            return str;
    
        }
    });

    return BaseLayoutView;
});