define("views/language-select-option", function(require) {
    "use strict";
    
    var BaseLayoutView                  = require("views/base"),
        LanguageSelectOptionTemplate    = require("hbs!templates/language-select-option"),
        LanguageSelectOptionView;

    LanguageSelectOptionView = BaseLayoutView.extend({
        tagName: "li",
        template: LanguageSelectOptionTemplate,
        triggers: {
            "click @ui.btnLanguage": {
                event: "language:select",
                preventDefault: true
            }
        },
        ui: {
            'btnLanguage': 'a'
        },
        onBeforeShow: function() {
            this.$el.addClass(this.model.get("className"));
        }
    });

    return LanguageSelectOptionView;
});
