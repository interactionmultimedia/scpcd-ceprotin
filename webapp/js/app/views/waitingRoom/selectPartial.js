define("views/waitingRoom/selectPartial", function(require) {
    "use strict";

    var Radio                           = require("backbone.radio"),
        BaseLayoutView                  = require("views/base"),
        WaitingRoomPartialTemplate      = require("hbs!templates/waitingRoom/selectPartial"),
        WaitingRoomPartialView;

    WaitingRoomPartialView = BaseLayoutView.extend({
        template: WaitingRoomPartialTemplate,
        data: {},


        initialize: function() {
            this.channel = Radio.channel("App");

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },
    });

    return WaitingRoomPartialView;
});
