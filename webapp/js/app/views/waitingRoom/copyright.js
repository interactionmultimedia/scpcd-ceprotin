define("views/waitingRoom/copyright", function(require) {
    "use strict";

    var Radio                       = require("backbone.radio"),
        $                           = require("jquery"),
        i18n                        = require("i18n"),
        BaseLayoutView              = require("views/base"),
        CopyrightTemplate             = require("hbs!templates/waitingRoom/copyright"),
        CopyrightFooterView;

    CopyrightFooterView = BaseLayoutView.extend({
        template: CopyrightTemplate
    });

    return CopyrightFooterView;
});
