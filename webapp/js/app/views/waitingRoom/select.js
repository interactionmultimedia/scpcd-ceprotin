define("views/waitingRoom/select", function(require) {
    "use strict";

    var Radio                           = require("backbone.radio"),
        $                               = require("jquery"),
        i18n                            = require("i18n"),
        BaseLayoutView                  = require("views/base"),
        WaitingRoomContentTemplate      = require("hbs!templates/waitingRoom/select"),
        SelectPartialView               = require("views/waitingRoom/selectPartial"),
        WaitingRoomContentView;

    WaitingRoomContentView = BaseLayoutView.extend({
        className: "table",
        template: WaitingRoomContentTemplate,
        data: {},
        ui:{
            'btnRoom': '.js-room',
            'imgRoom': '.img-room'
        },

        events:{
            'click @ui.btnRoom': 'showRoom',
            'click @ui.imgRoom': 'showRoom'
        },

        regions: {
            "room1": "#room1",
            "room2": "#room2"
        },

        initialize: function() {
            this.channel = Radio.channel("App");

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },

        showRoom: function(e) {
            e.preventDefault();

            var roomId = $(e.currentTarget).attr("data-room");

            this.channel.trigger('change:room', roomId);
        },

        onBeforeShow: function() {
            var dataRoom1   = App.ClinicalCases.getPublishedClinicalCases(1, i18n.language),
                dataRoom2   = App.ClinicalCases.getPublishedClinicalCases(2, i18n.language);

            this.showChildView("room1", new SelectPartialView({
                data: {
                    index: 1,
                    rooms: dataRoom1.toJSON()
                }
            }));

            this.showChildView("room2", new SelectPartialView({
                data: {
                    index: 2,
                    rooms: dataRoom2.toJSON()
                }
            }));
        }
    });

    return WaitingRoomContentView;
});
