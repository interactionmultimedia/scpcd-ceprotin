define("views/waitingRoom/persons", function(require) {
    "use strict";

    var Radio                       = require("backbone.radio"),
        $                           = require("jquery"),
        i18n                        = require("i18n"),
        BaseLayoutView              = require("views/base"),
        PatientProfileView          = require("views/waitingRoom/patientProfile"),
        ModalLayoutView             = require("views/modal/modal"),
        HeaderWithTitleView         = require("views/modal/headerWithTitle"),
        ClinicalCasesCollection     = require("collections/clinicalCases"),
        ConsultationsCollection     = require("collections/consultations"),
        PersonsTemplate             = require("hbs!templates/waitingRoom/persons"),
        PersonsView;

    PersonsView = BaseLayoutView.extend({
        template: PersonsTemplate,
        data: {},
        ui:{
            'btnPerson': '.gif-link',
            'btnRoom': '.js-go-room'
        },

        events:{
            'click @ui.btnPerson': 'showDetails',
            'click @ui.btnRoom': 'goRoom'
        },

        initialize: function() {
            this.channel        = Radio.channel("App");
            this.channel.on('select:consultation', this.selectConsultation.bind(this), this);

            BaseLayoutView.prototype.initialize.apply(this, arguments);

            this.data.labelBtnGoRoom = i18n.t("action.goRoom").replace("%d", this.data.nextRoom);
        },

        selectConsultation: function(url) {
            if(this.popupChoice){
                this.popupChoice.hideModal();
                this.popupChoice.destroy();
            }

            this.channel.trigger("consultation:choice", url);

            App.router.navigate(url, {trigger: true});
        },

        goRoom: function(e){
            e.preventDefault();

            var roomId = $(e.currentTarget).attr("data-next-room");

            this.channel.trigger('change:room', roomId);
        },

        onRender: function() {
            $("body")
                .removeClass()
                .addClass("waiting-room");
        },

        showDetails: function(e) {
            e.preventDefault();

            var caseId = $(e.currentTarget).attr("data-caseId"),
                clinicalCase = new ClinicalCasesCollection(this.data.persons),
                oneClinicalCase = clinicalCase.get(caseId);

            if(!App.Consultations.caseExists(caseId)){
                App.Consultations = new ConsultationsCollection();
            }

            this.popupChoice = new ModalLayoutView({
                className: "overlay overlay-patient-profile overlay-with-padding",
                headerView: new HeaderWithTitleView({
                    data: {
                        title: i18n.t("consultation.medicalFiles.patientProfile.title")
                    }
                }),
                contentView: new PatientProfileView({
                    model: oneClinicalCase
                }),
                footerView: null
            });

            this.popupChoice.setHeaderClassName("overlay-header clearfix");
            this.popupChoice.showModal();

            $("body").addClass("waiting-room");
        },

        onDestroy: function() {
            this.channel.off(null, null, this);
        }
    });

    return PersonsView;
});
