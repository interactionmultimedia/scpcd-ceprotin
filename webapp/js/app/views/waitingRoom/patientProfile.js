define("views/waitingRoom/patientProfile", function(require) {
    "use strict";

    var Radio                   = require("backbone.radio"),
        $                       = require("jquery"),
        i18n                    = require("i18n"),
        BaseLayoutView          = require("views/base"),
        CustomScrollbarBehavior = require("views/behaviors/customScrollbarBehavior"),
        PatientProfileTemplate  = require("hbs!templates/waitingRoom/patientProfile"),
        PatientProfileView;

    PatientProfileView = BaseLayoutView.extend({
        template: PatientProfileTemplate,
        ui:{
            'btnConsultation': '.flat-list a',
            'btnFirstConsultation': '.flat-list li:first-child a',
            'btnsConsultation': '.flat-list li a',
            'wrapperProfile': '.wrapper-profile'
        },

        events:{
            'click @ui.btnConsultation': 'showConsultation'
        },
        behaviors: {
            CustomScrollbarBehavior: {
                behaviorClass: CustomScrollbarBehavior
            }
        },

        initialize: function() {
            this.channel    = Radio.channel("App");
            this.baseUrl    = "/data/";
        },

        showConsultation: function(e) {
            e.preventDefault();

            var consultationUrl = $(e.currentTarget).attr("data-url");
            var index = $(e.currentTarget).parents('ul.flat-list').find('a').index(e.currentTarget);
            App.SimulationState.seenConsultation(index);

            this.removeGuideElement();

            this.channel.trigger("select:consultation", consultationUrl);
        },

        showGuideElement: function() {
            var seenConsultations = App.SimulationState.get('seenConsultations');
            var shown = false;
            var nbConsultations = $(this.ui.btnsConsultation).length;
            for(var i = 0; i < nbConsultations; i++) {
                if(!shown && seenConsultations.indexOf(i) == -1) {
                    shown = true;
                    $(this.ui.btnsConsultation).eq(i).addClass('lookatme');
                }
            }
        },

        removeGuideElement: function() {
            this.$el.find(this.ui.btnsConsultation).removeClass("lookatme");
            //App.SimulationState.addGuideElement('btnFirstConsultation');
        },

        onBeforeRender: function() {
            var patientProfilePicture   = this.model.get("patientPictureProfile"),
                clinicalCase            = App.ClinicalCases.get(this.model.get("caseId")),
                lastConsultationIndex   = clinicalCase.getLastConsultationIndex(),
                lastConsultation        = clinicalCase.getConsultation(lastConsultationIndex),
                examinations            = [],
                consultation,
                disclaimer;

            for(var i = 0; i < ~~this.model.get("nbExaminations"); i++){
                consultation    = clinicalCase.getConsultation(i);
                
                if( ~~consultation.published === 1 || !App.Config.isProduction() ) {
                    examinations.push({
                        url: "#case/" + ~~this.model.get("id") + "/consultation/" + i,
                        name: consultation.title
                    });
                }
            }

            if( App.Config.isWebconf() ) {
                disclaimer  = i18n.t("waitingRoom.disclaimerWebconf");
            } else {
                disclaimer  = i18n.t("waitingRoom.disclaimer");
            }

            this.data   = {
                patientFullName: this.model.getPatientFullName(),
                patientProfilePicture: ( !!patientProfilePicture ? this.baseUrl + patientProfilePicture + ".png" : "" ),
                examinations: examinations,
                disclaimer: disclaimer,
                jobBag: ( !!lastConsultation.jobBag ? lastConsultation.jobBag : "" )
            };
        },

        onBeforeShow: function() {
            this.showGuideElement();

            this.trigger("CustomScrollbarBehavior:init", {
                $el: this.$el.find(this.ui.wrapperProfile),
                height: (window.innerHeight - 15)
            });
        }
    });

    return PatientProfileView;
});
