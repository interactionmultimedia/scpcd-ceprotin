define("views/legals", function(require) {
    "use strict";

    var i18n            = require("i18n"),
        BaseLayoutView  = require("views/base"),
        LegalsTemplate  = require("hbs!templates/legals"),
        LegalsView;

    LegalsView = BaseLayoutView.extend({
        template: LegalsTemplate,
        className: "job-bags",
        initialize: function(options) {
            this.data           = options.data ? options.data : {};
            this.data.jobBag    = App.ClinicalCases.getMainJobBag(i18n.language);
        }
    });

    return LegalsView;
});
