define("views/meeting/trainer-authentication-form", function(require) {
    "use strict";

    var Backbone                        = require("backbone"),
        Radio                           = require("backbone.radio"),
        i18n                            = require("i18n"),
        $                               = require("jquery"),
        _                               = require("underscore"),
        Trainer                         = require("models/trainer"),
        BaseLayoutView                  = require("views/base"),
        ValidationBehavior              = require("views/behaviors/validationBehavior"),
        TrainerAuthenticationTemplate   = require("hbs!templates/meeting/trainer-authentication-form"),
        TrainerAuthenticationView;

    TrainerAuthenticationView = BaseLayoutView.extend({
        template: TrainerAuthenticationTemplate,
        ui: {
            'btnBack': '.btn-back',
            'form': 'form',
            'message': '.message',
            'submit': 'button[type="submit"]'
        },
        events: {
            'click @ui.btnBack': 'back',
            'submit @ui.form': 'authenticate'
        },
        behaviors: {
            ValidationBehavior: {
                behaviorClass: ValidationBehavior
            }
        },
        initialize: function() {
            this.channel    = Radio.channel("App");

            this.channel.on("screen:authenticationFrom:back", function() {
                Backbone.history.navigate("");
            }, this);
        },
        authenticate: function(e) {
            var that = this,
                $form,
                formData,
                data;

            e.preventDefault();

            $form = $(e.target);
            formData = $form.serializeArray();
            data = _.object(_.pluck(formData, 'name'), _.pluck(formData, 'value'));

            this.ui.message.html("");
            this.ui.submit.attr("disabled", "disabled");

            App.Trainer = new Trainer();
            $.when(App.Trainer.login(data))
                .then(function(result) {
                    App.router.goto("event-information");
                })
                .fail(function() {
                    that.ui.submit.removeAttr("disabled");
                    that.ui.message.html(i18n.t("trainerAuthenticationForm.messages.badAuthentication"));
                });
        },
        back: function(e) {
            e.preventDefault();

            this.channel.trigger("screen:authenticationFrom:back", "main");
        },
        onShow: function() {
            this.trigger("ValidationBehavior:initFormValidation", {
                formId: "#connect-form",
                ignoreTitle: true,
                focusInvalid: true,
                rules: {
                    'login': {
                        required: true
                    },
                    'password': {
                        required: true
                    }
                }
            });
        },
        onBeforeShow: function() {
            $("body").addClass("rp-02");
        },
        onDestroy: function() {
            $("body").removeClass("rp-02");

            this.channel.off(null, null, this);
        }
    });

    return TrainerAuthenticationView;
});
