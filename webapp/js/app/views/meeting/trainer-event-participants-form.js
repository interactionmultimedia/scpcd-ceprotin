define("views/meeting/trainer-event-participants-form", function (require) {
    "use strict";

    var Radio = require("backbone.radio"),
        $ = require("jquery"),
        _ = require("underscore"),
        ParticipantsCollection = require("collections/participants"),
        Participant = require("models/participant"),
        Veeva = require("models/veeva"),
        BaseLayoutView = require("views/base"),
        ValidationBehavior = require("views/behaviors/validationBehavior"),
        CustomScrollbarBehavior = require("views/behaviors/customScrollbarBehavior"),
        TrainerEventParticipantsFormTemplate = require("hbs!templates/meeting/trainer-event-participants-form"),
        TrainerEventParticipantsFormView

    TrainerEventParticipantsFormView = BaseLayoutView.extend({
        template: TrainerEventParticipantsFormTemplate,
        ui: {
            'formFind': '#search-participant',
            'addButton': '#add-participant',
            'closeModal': '.modal-close',
            'pendingLi': 'ul.pending li.filled',
            'moveButton': '#select-participant',
            'removeButton': '.chosen .remove',
            'addCustomForm': '#add-custom-participant',
            'startButton': '.row-submit .btn'
        },
        events: {
            'submit @ui.formFind': 'findParticipants',
            'submit @ui.addCustomForm': 'addCustomParticipant',
            'click @ui.addButton': 'showModal',
            'click @ui.closeModal': 'closeModal',
            'click @ui.pendingLi': 'highlightParticipant',
            'click @ui.moveButton': 'moveParticipant',
            'click @ui.removeButton': 'removeParticipant',
            'click @ui.startButton': 'startMeeting'
        },
        behaviors: {
            ValidationBehavior: {
                behaviorClass: ValidationBehavior
            },
            CustomScrollbar: {
                behaviorClass: CustomScrollbarBehavior
            }
        },

        data: {},

        initialize: function () {

            this.channel    = Radio.channel("App");

            this.data.countries = App.i18n.getResource(App.i18n.language, 'shire', 'countries');

            this.data.pendingParticipants = new ParticipantsCollection();
            this.data.chosenParticipants = new ParticipantsCollection();
            
        },

        initBehaviors: function () {
            this.trigger("CustomScrollbarBehavior:init", {
                $el: $('ul.chosen')
            });
            this.trigger("ValidationBehavior:initFormValidation", {
                formId: "#search-participant",
                ignoreTitle: true,
                focusInvalid: true,
                rules: {
                    'firstName': {
                        required: true
                    },
                    'lastName': {
                        required: true
                    },
                    'country': {
                        required: true
                    }
                }
            });
            this.trigger("ValidationBehavior:initFormValidation", {
                formId: "#add-custom-participant",
                ignoreTitle: true,
                focusInvalid: true,
                rules: {
                    'custom_first_name': {
                        required: true
                    },
                    'custom_last_name': {
                        required: true
                    },
                    'custom_country': {
                        required: true
                    }
                }
            });
        },

        onRender: function () {
            this.initBehaviors();
        },

        onShow: function () {
            this.initBehaviors();
            this.getParticipants();
        },

        startMeeting: function (e) {
            e.preventDefault();

            if (this.data.chosenParticipants.length === 0) {
                alert(App.i18n.getResource(App.i18n.language, 'shire', 'eventParticipantsForm.need_participant'));
            } else {
                this.channel.trigger("trainer:start", App.Meeting);

                App.router.navigate("waiting-room", {
                    trigger: true
                });
            }
        },

        moveParticipant: function (e) {
            e.preventDefault();

            var that = this,
                index = $('li.selected').attr('data-index'),
                pending;

            pending = this.data.pendingParticipants.at(index);
            if (!!pending) {
                $.when(App.Meeting.getLastCodeParticipant(App.Meeting.get("id"))).then(function(code) {
                    var index = !!code ? code.substr(5) : 0;

                    pending.set("code", App.Meeting.get("code")+(~~index+1));
                    pending.save(null, {
                        success: function (model, result) {
                            that.data.pendingParticipants.remove(pending);
                            that.data.chosenParticipants.add(pending);
                            that.render();
                        }
                    });
                });
            }
        },

        removeParticipant: function (e) {
            var that = this,
                index = $(e.currentTarget).attr('data-index'),
                participant = this.data.chosenParticipants.at(index);

            participant.delete(null, {
                parse: false,
                success: function (model, result) {
                    participant.unset("id");
                    that.data.chosenParticipants.remove(participant);
                    that.data.pendingParticipants.add(participant);
                    that.render();
                }
            });
        },

        addCustomParticipant: function (e) {

            e.preventDefault();

            var that = this,
                participant;

            $.when(App.Meeting.getLastCodeParticipant(App.Meeting.get("id"))).then(function(code) {
                var index = !!code ? code.substr(5) : 0;
                participant = new Participant({
                    'code': App.Meeting.get("code")+(~~index+1),
                    'id_meeting': App.Meeting.get("id"),
                    'firstname': $('#custom_first_name').val(),
                    'lastname': $('#custom_last_name').val(),
                    'country': $('#custom_country').val()
                });

                participant.save(null, {
                    success: function (model, result) {
                        that.data.chosenParticipants.add(participant);
                        that.closeModal();
                        that.render();
                    }
                });
            });
        },

        highlightParticipant: function (e) {
            var $li = $(e.currentTarget);

            $('li.filled').removeClass('selected');
            $li.addClass('selected');
        },

        findParticipants: function (e) {

            e.preventDefault();

            var that = this,
                data = $('#search-participant').serialize();

            App.Veeva = new Veeva();
            $.when(App.Veeva.findParticipant(data)).then(function (result) {
                
                if (result.length) {

                    that.data.pendingParticipants.reset();

                    _.each(result, function (item) {
                        var participant = new Participant({
                            veeva_id: item.Salesforce_HCP_Id,
                            id_meeting: App.Meeting.get("id"),
                            firstname: item.firstName,
                            lastname: item.lastName,
                            country: item.country,
                            workplace: {
                                id: item.workPlaceId,
                                name: item.workPlaceName,
                                address1: item.workPlaceAddressLine1,
                                address2: item.workPlaceAddressLine2,
                                zipcode: item.workPlaceZipcode,
                                city: item.workPlaceCity,
                                country: item.workPlaceCountry,
                                state: item.workPlace_State_Province
                            }
                        });

                        that.data.pendingParticipants.add(participant);
                        that.render();
                    });

                } else {
                    alert(App.i18n.getResource(App.i18n.language, 'shire', 'eventParticipantsForm.noParticipants'));
                }
            });

        },

        getParticipants: function() {

            var that = this;
    
            $.get(
                App.Config.get("gateway") + "/participants/" + App.Meeting.get('id')
            ).success(function (participants) {

                $.each(participants, function(i,item) {
                    that.data.chosenParticipants.add(new Participant(item));
                });
                that.render();
                    
            });
        },

        showModal: function (e) {
            e.preventDefault();
            $('body').addClass('show-modal');
        },

        closeModal: function (e) {

            if (e) {
                e.preventDefault();
            }
            $('body').removeClass('show-modal');

        },
        serializeData: function () {

            if (this.data.pendingParticipants.length > 0) {
                this.data.TemplatePendingParticipants = this.data.pendingParticipants.toJSON();
            } else {
                this.data.TemplatePendingParticipants = [];
            }

            if (this.data.chosenParticipants.length > 0) {
                this.data.TemplateChosenParticipants = this.data.chosenParticipants.toJSON();
            } else {
                this.data.TemplateChosenParticipants = [];
            }

            var viewData = {
                data: this.data
            };

            return _.extend(viewData, Marionette.LayoutView.prototype.serializeData.apply(this, arguments));
        }
    });

    return TrainerEventParticipantsFormView;
});
