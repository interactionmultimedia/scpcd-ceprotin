define("views/meeting/finishMeeting", function(require) {
    "use strict";

    var BaseLayoutView              = require("views/base"),
        Radio                       = require("backbone.radio"),
        QuitTemplate                = require("hbs!templates/meeting/finishMeeting"),
        QuitView;

    QuitView = BaseLayoutView.extend({
        template: QuitTemplate,
        ui: {
            'btnBack': '.js-back',
            'btnFinish': '.js-finish'
        },
        events:{
            'click @ui.btnBack': 'backConsultation',
            'click @ui.btnFinish': 'finishMeeting'
        },
        initialize: function() {
            this.channel    = Radio.channel("App");
        },

        /**
         * Action sur le bouton "back"
         */
        backConsultation: function(e) {
            e.preventDefault();
            this.channel.trigger("consultation:finish:back");
        },

        /**
         * Action sur le bouton "finish"
         */
        finishMeeting: function(e) {
            e.preventDefault();
            this.channel.trigger("meeting:finish");
        },

        onShow: function() {
            $("body").addClass("quit-consultation");
        },

        onDestroy: function() {
            $("body").removeClass("quit-consultation");
        }
    });

    return QuitView;
});
