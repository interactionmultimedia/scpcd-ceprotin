define("views/meeting/finished", function(require) {
    "use strict";

    var $                       = require("jquery"),
        BaseLayoutView          = require("views/base"),
        CustomScrollbarBehavior = require("views/behaviors/customScrollbarBehavior"),
        FinishedView;

    FinishedView = BaseLayoutView.extend({
        behaviors: {
            CustomScrollbarBehavior: {
                behaviorClass: CustomScrollbarBehavior
            }
        },
        onBeforeRender:function() {
            if( !!App.Trainer ) {
                this.template   = require("hbs!templates/meeting/finished");
            } else {
                this.template   = require("hbs!templates/meeting/finishedParticipant");
            }
        },
        onShow:function() {
            if( !!App.Trainer ) {
                this.showFeedbackTrainer();
            }
        },
        showFeedbackTrainer:function() {
            var that = this;

            if( !!App.Meeting ) {
                $.when(App.Meeting.getParticipantsStats()).then(function(data) {
                    that.data       = data;
                    that.render();

                    that.trigger("CustomScrollbarBehavior:init", {
                        $el: that.$el
                    });

                    App.Meeting = null;
                });
            }
        }
    });

    return FinishedView;
});
