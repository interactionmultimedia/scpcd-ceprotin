define("views/meeting/participant-authentication-form", function(require) {
    "use strict";

    var Backbone                            = require("backbone"),
        Radio                               = require("backbone.radio"),
        i18n                                = require("i18n"),
        $                                   = require("jquery"),
        Participant                         = require("models/participant"),
        BaseLayoutView                      = require("views/base"),
        ValidationBehavior                  = require("views/behaviors/validationBehavior"),
        ParticipantAuthenticationTemplate   = require("hbs!templates/meeting/participant-authentication-form"),
        ParticipantAuthenticationView;

    ParticipantAuthenticationView = BaseLayoutView.extend({
        template: ParticipantAuthenticationTemplate,
        ui: {
            'btnBack': '.btn-back',
            'form': 'form',
            'message': '.message',
            'submit': 'button[type="submit"]'
        },
        events: {
            'click @ui.btnBack': 'back',
            'submit @ui.form': 'authenticate'
        },
        behaviors: {
            ValidationBehavior: {
                behaviorClass: ValidationBehavior
            }
        },
        initialize: function() {
            this.channel    = Radio.channel("App");

            this.channel.on("screen:authenticationFrom:back", function() {
                Backbone.history.navigate("");
            }, this);
        },
        authenticate: function(e) {
            var that = this,
                $form,
                formData,
                data;

            e.preventDefault();

            $form = $(e.target);
            formData = $form.serializeArray();
            data = _.object(_.pluck(formData, 'name'), _.pluck(formData, 'value'));

            this.ui.message.html("");
            this.ui.submit.attr("disabled", "disabled");

            App.Participant = new Participant();
            $.when(App.Participant.login(data))
                .then(function() {
                    $.when(App.Participant.fetch())
                        .then(function() {
                            that.channel.trigger("participant:logged", App.Participant.getRP());
                        });
                })
                .fail(function() {
                    that.ui.submit.removeAttr("disabled");
                    that.ui.message.html(i18n.t("participantAuthenticationForm.messages.badAuthentication"));
                });
        },
        back: function(e) {
            e.preventDefault();

            this.channel.trigger("screen:authenticationFrom:back", "main");
        },
        onShow: function() {
            this.trigger("ValidationBehavior:initFormValidation", {
                formId: "#connect-form",
                ignoreTitle: true,
                focusInvalid: true,
                rules: {
                    'code': {
                        required: true
                    }
                }
            });
        },
        onBeforeShow: function() {
            $("body").addClass("rp-02");
        },
        onDestroy: function() {
            $("body").removeClass("rp-02");

            this.channel.off(null, null, this);
        }
    });

    return ParticipantAuthenticationView;
});
