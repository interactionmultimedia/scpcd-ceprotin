define("views/meeting/loading", function(require) {
    "use strict";

    var Radio               = require("backbone.radio"),
        i18n                = require("i18n"),
        $                   = require("jquery"),
        BaseLayoutView      = require("views/base"),
        LoadingTemplate     = require("hbs!templates/meeting/loading"),
        LoadingView;

    LoadingView = BaseLayoutView.extend({
        template: LoadingTemplate,
        data: {},
        initialize: function() {
            this.channel            = Radio.channel("App");

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },
        onBeforeRender: function() {
            if( !!App.Trainer ) {
                this.data.message   = i18n.t("meeting.waitingMessageTrainer");
            } else if( !!App.Participant ) {
                this.data.message   = App.Participant.getFullname()+", "+i18n.t("meeting.waitingMessageParticipant");
            }
        },
        onBeforeShow: function() {
            $("body").addClass("loading");
        },
        onDestroy: function() {
            $("body").removeClass("loading");
        }
    });

    return LoadingView;
});
