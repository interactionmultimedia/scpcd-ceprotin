define("views/meeting/trainer-event-info-form", function (require) {
    "use strict";

    var Backbone = require("backbone"),
        Radio = require("backbone.radio"),
        i18n = require("i18n"),
        $ = require("jquery"),
        _ = require("underscore"),
        Trainer = require("models/trainer"),
        Meeting = require("models/meeting"),
        BaseLayoutView = require("views/base"),
        ValidationBehavior = require("views/behaviors/validationBehavior"),
        TrainerEventInfoFormTemplate = require("hbs!templates/meeting/trainer-event-info-form"),
        TrainerEventInfoFormView

    TrainerEventInfoFormView = BaseLayoutView.extend({
        template: TrainerEventInfoFormTemplate,

        ui: {
            'form': 'form',
            'rejoinBlock': '.rejoin-block',
            'rejoinLink': '.rejoin-link'
        },

        events: {
            'submit @ui.form': 'createEvent',
            'click @ui.rejoinLink':'goToParticipants'
        },

        behaviors: {
            ValidationBehavior: {
                behaviorClass: ValidationBehavior
            }
        },

        initialize: function () {

        },

        onShow: function () {
            this.trigger("ValidationBehavior:initFormValidation", {
                formId: "#event-form",
                ignoreTitle: true,
                focusInvalid: true,
                rules: {
                    'name': {
                        required: true
                    },
                    'venue': {
                        required: true
                    },
                    'meeting-type': {
                        required: true
                    },
                    'product': {
                        required: true
                    }
                }
            });
            
            this.checkActiveMeeting(App.Trainer.get('id'));
        },

        createEvent: function (e) {
            e.preventDefault();

            var data = {
                'id_trainer': App.Trainer.get('id'),
                'name': $('#name').val(),
                'venue': $('#venue').val(),
                'type': $('#meeting-type').val(),
                'product': $('#product').val(),
                'code': this.getRandomCode(5),
                'language': i18n.language
            }, that = this;

            App.Meeting = new Meeting(data);
            App.Meeting.unset("id");
            App.Meeting.unset("participants");

            $.when(App.Meeting.save())
                .then(function () {
                    if( !!App.Trainer ) {
                        App.Trainer.set("meeting", App.Meeting);
                    }

                    $('.form-error').fadeOut();
                    that.goToParticipants();
                }, function () {
                    $('.form-error').fadeIn();
                });
        },

        checkActiveMeeting:function(id_trainer) {

            var that = this,
                deferred = $.Deferred();

            $.post(
                App.Config.get("gateway") + "/meeting/checkActive", {
                    'id_trainer': id_trainer
                }
            ).success(function (meeting) {
                    if(meeting.id) {
                        App.Meeting = new Meeting(meeting);
                        $(that.ui.rejoinBlock).fadeIn();
                    }
                    deferred.resolve(meeting);
                }).fail(function (err) {
                    deferred.reject();
                });

            return deferred.promise();

        },
        
        goToParticipants:function(e) {
            if(e) { e.preventDefault(); }
            
            App.router.navigate('event-participants', {
                trigger: true
            });            
        }
    });

    return TrainerEventInfoFormView;
});
