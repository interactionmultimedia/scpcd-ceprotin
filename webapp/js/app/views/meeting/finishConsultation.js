define("views/meeting/finishConsultation", function(require) {
    "use strict";

    var BaseLayoutView              = require("views/base"),
        Radio                       = require("backbone.radio"),
        QuitTemplate                = require("hbs!templates/meeting/finishConsultation"),
        QuitView;

    QuitView = BaseLayoutView.extend({
        template: QuitTemplate,
        ui: {
            'btnBack': '.js-back',
            'btnFinish': '.js-finish'
        },
        events:{
            'click @ui.btnBack': 'backConsultation',
            'click @ui.btnFinish': 'finishConsultation'
        },
        initialize: function() {
            this.channel    = Radio.channel("App");
        },

        /**
         * Action sur le bouton "back"
         */
        backConsultation: function(e) {
            e.preventDefault();
            this.channel.trigger("consultation:finish:back");
        },

        /**
         * Action sur le bouton "finish"
         */
        finishConsultation: function(e) {
            e.preventDefault();
            this.channel.trigger("consultation:finish");
        },

        onShow: function() {
            $("body").addClass("quit-consultation");
        },

        onDestroy: function() {
            $("body").removeClass("quit-consultation");
        }
    });

    return QuitView;
});
