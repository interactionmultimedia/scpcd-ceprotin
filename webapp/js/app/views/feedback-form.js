define("views/feedback-form", function(require) {
    "use strict";

    var Radio                   = require("backbone.radio"),
        $                       = require("jquery"),
        BaseLayoutView          = require("views/base"),
        FormFeedbackTemplate    = require("hbs!templates/feedback-form"),
        FormFeedbackView;

    FormFeedbackView = BaseLayoutView.extend({
        template: FormFeedbackTemplate,
        ui:{
            'fieldEmail': 'input[name="email"]',
            'form': 'form'
        },
        events:{
            'submit @ui.form': 'saveData'
        },

        initialize: function() {
            this.channel    = Radio.channel("App");
        },

        onBeforeShow: function() {
            $("body").addClass("feedback feedback-form");
        },

        onDestroy: function() {
            $("body").removeClass("feedback feedback-form");

            if( App.SimulationState.get("email") !== null ) {
                return;
            }

            App.SimulationState.set("email", "");
            App.SimulationState.save();
        },
        
        saveData: function(e) {
            e.preventDefault();

            App.SimulationState.set("email", "");
            if( !!$(this.ui.fieldEmail).val() ) {
                App.SimulationState.set("email", $(this.ui.fieldEmail).val());
            }
            App.SimulationState.save();

            this.channel.trigger("screen:feedbackForm:save");
        }
    });

    return FormFeedbackView;
});
