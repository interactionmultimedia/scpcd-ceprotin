define("views/consultation/globalConclusion/content", function(require) {
    "use strict";

    var BaseLayoutView              = require("views/base"),
        CustomScrollbarBehavior     = require("views/behaviors/customScrollbarBehavior"),
        ContentTemplate             = require("hbs!templates/consultation/globalConclusion/content"),
        ContentView;

    ContentView = BaseLayoutView.extend({
        template: ContentTemplate,
        behaviors: {
            CustomScrollbarBehavior: {
                behaviorClass: CustomScrollbarBehavior
            }
        },

        initialize: function() {
            this.trigger("CustomScrollbarBehavior:init", {
                $el: this.$el,
                height: 409
            });

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },

        onBeforeRender: function() {
            this.data.content = App.consultation.get("synthesis");
        }
    });

    return ContentView;
});
