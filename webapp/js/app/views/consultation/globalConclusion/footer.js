define("views/consultation/globalConclusion/footer", function(require) {
    "use strict";

    var Radio               = require("backbone.radio"),
        BaseLayoutView      = require("views/base"),
        FooterTemplate      = require("hbs!templates/consultation/globalConclusion/footer"),
        FooterView;

    FooterView = BaseLayoutView.extend({
        template: FooterTemplate,
        ui: {
            'btnShare': '.btn-red'
        },
        events: {
            'click @ui.btnShare': 'share'
        },

        initialize: function() {
            this.channel = Radio.channel("App");
        },

        share: function (e) {
            e.preventDefault();

            this.channel.trigger("modal:globalConclusion:share");
        }
    });

    return FooterView;
});
