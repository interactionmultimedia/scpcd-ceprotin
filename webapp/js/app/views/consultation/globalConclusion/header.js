define("views/consultation/globalConclusion/header", function(require) {
    "use strict";

    var Radio               = require("backbone.radio"),
        BaseLayoutView      = require("views/base"),
        HeaderTemplate      = require("hbs!templates/consultation/globalConclusion/header"),
        HeaderView;

    HeaderView = BaseLayoutView.extend({
        template: HeaderTemplate,
        ui: {
            'btnBack': '.btn-back-circle'
        },
        events: {
            'click @ui.btnBack': 'back'
        },

        initialize: function() {
            this.channel = Radio.channel("App");
        },

        back: function (e) {
            e.preventDefault();

            this.channel.trigger('modal:globalConclusion:end');
        }
    });

    return HeaderView;
});
