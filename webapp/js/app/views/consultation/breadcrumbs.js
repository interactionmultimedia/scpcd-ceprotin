define('views/consultation/breadcrumbs', function(require) {
    "use strict";

    var BaseLayoutView          = require("views/base"),
        BreadcrumbsTemplate     = require("hbs!templates/consultation/breadcrumbs"),
        BreadcrumbsLayoutView;

    BreadcrumbsLayoutView = BaseLayoutView.extend({
        template: BreadcrumbsTemplate,
        data: {},
        initialize: function() {
            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },

        onBeforeRender: function() {
            this.data = App.consultation.toJSON();
        }
    });

    return BreadcrumbsLayoutView;
});
