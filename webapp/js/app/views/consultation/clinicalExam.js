define("views/consultation/clinicalExam", function (require) {
    "use strict";

    var Marionette = require("marionette"),
        BaseLayoutView = require("views/base"),
        Radio = require("backbone.radio"),
        i18n = require("i18n"),
        ClinicalExamTemplate = require("hbs!templates/consultation/clinicalExam"),
        PlotView = require("views/consultation/clinicalExam/plot"),
        PlotsCollection = require("collections/plots"),
        PlotDetailsCollection = require("collections/plotDetails"),
        HeaderWithTitleView = require("views/modal/headerWithTitle"),
        ModalLayoutView = require("views/modal/modal"),
        ContentTextView = require("views/modal/contentText"),
        FeatureImage = require("libs/featureImage"),
        AvatarPosition = require('models/avatarPosition'),
        ClinicalExamView;

    ClinicalExamView = Marionette.CompositeView.extend({
        template: ClinicalExamTemplate,
        childView: PlotView,
        childViewContainer: '#plots',
        ui: {
            'btnExamReport': '.btn-exam-report',
            'btnCloseEnvironment': '.btn-close-environment',
            'gif': 'img.gif-animated'
        },
        events: {
            'click @ui.btnExamReport': 'examReport',
            'click @ui.btnCloseEnvironment': 'resetEnvironment',
        },
        data: {},
        initialize: function (options) {
            this.channel = Radio.channel("App");

            this.channel.on('clinicalExam:renderEnvironment', this.renderEnvironment, this);

            this.model = App.consultation.getStep("ClinicalExaminationModule");
            this.collection = this.model.getPlots();

            if (this.model) {
                this.clinicalExamData = this.model.getInformationsClinicalExam();

                this.data.bloodPressureValue = this.clinicalExamData.bloodPressureValue;
                this.data.temperatureValue = this.clinicalExamData.temperatureValue;
                this.data.heartRateValue = this.clinicalExamData.heartRateValue;
                this.data.hasExamReport = !!this.clinicalExamData.examReport;
            }
        },

        /**
         * Action sur le bouton "back"
         */
        quitExam: function (e) {
            e.preventDefault();
            this.channel.trigger("clinicalExam:quit");
        },

        /**
         * Action sur le bouton "Exam Report"
         */
        examReport: function (e) {
            e.preventDefault();

            if (this.model) {
                this.ui.btnExamReport.removeClass("lookatme");
                App.SimulationState.addGuideElement('btnExamReport');
                this.popupExamReport = new ModalLayoutView({
                    className: "overlay overlay-exam-report overlay-with-padding",
                    headerView: new HeaderWithTitleView({
                        data: {
                            title: i18n.t("consultation.consultationSteps.clinicalExamination.examReport")
                        }
                    }),
                    contentView: new ContentTextView({
                        data: {
                            content: this.clinicalExamData.examReport
                        }
                    }),
                    footerView: null
                });

                this.popupExamReport.setHeaderClassName("overlay-header clearfix");
                this.popupExamReport.showModal();
                this.channel.trigger('clinicalExam:report');
            }
        },

        /**
         * Change le background et cache des éléments
         * @param {string} backgroundUrl The URL of the background to render
         */
        renderEnvironment: function (backgroundUrl) {
            var that = this;
            $('body').css('background-image', 'url(' + backgroundUrl + ')');
            this.ui.btnExamReport.addClass('hidden');
            this.ui.btnCloseEnvironment.removeClass('hidden');
            this.ui.gif.addClass('hidden');
            this.$childViewContainer.addClass('hidden');
            setTimeout(function () {
                that.$el.one('click', function () {
                    that.resetEnvironment();
                });
            }, 10);
        },

        /**
         * Change le background et réaffiche des éléments
         */
        resetEnvironment: function () {
            var environnement = App.consultation.getEnvironnement("Camera-Exam1"),
                caseId = App.consultation.get("caseId"),
                consultationId = App.consultation.get("consultationId"),
                folderName = "c" + caseId + "_c" + consultationId;

            if (this.isDestroyed) {
                return;
            }

            $("body")
                .addClass("clinical-exam no-overlay")
                .css("background-image", "url('/data/assets/" + folderName + "/images/" + environnement.environmentBackground + "')")
                .css("background-position", 'center center')
                .css('background-size', 'cover');

            if (!this._isRendering) {
                this.ui.btnExamReport.removeClass('hidden');
                this.ui.btnCloseEnvironment.addClass('hidden');
                this.ui.gif.removeClass('hidden');
                this.$childViewContainer.removeClass('hidden');
            }
        },

        /**
         * Avant l'affichage :
         * - ajout des classes CSS
         */
        onBeforeRender: function () {
            var caseId = App.consultation.get("caseId"),
                consultationId = App.consultation.get("consultationId"),
                folderName = "c" + caseId + "_c" + consultationId,
                idleExam = App.consultation.get("profile").get("idleExam"),
                that = this;

            this.resetEnvironment();

            var examAvatarPosition = new AvatarPosition(idleExam);
            examAvatarPosition.on('change', function () {
                $(that.ui.gif).attr({
                    width: this.get('width'),
                    height: this.get('height'),
                }).css({
                    top: this.get('top'),
                    left: this.get('left')
                })
            });
            this.data.gif = {
                url: "/data/assets/" + folderName + "/images/" + idleExam.asset,
                height: idleExam.height,
                width: idleExam.width,
                left: idleExam.left,
                top: idleExam.top,
            };

            //elem:     element that has the bg image
            //spots:    backbone collection
            //bgWidth:  intrinsic width of background image
            //bgHeight: intrinsic height of background image
            function FeatureSpot(elem, spots, bgWidth, bgHeight) {
                this.ratio = bgWidth / bgHeight; //aspect ratio of bg image
                this.element = elem;
                this.spots = spots;
                spots.each(function (spot) {
                    spot.set('originalSpotX', spot.get('originalSpotX') / bgWidth); //percent from the left edge of bg image the feature resides
                    spot.set('originalSpotY', (bgHeight - spot.get('originalSpotY')) / bgHeight); //percent from bottom edge of bg image that feature resides
                });
                window.addEventListener("resize", this.setFeaturePositions.bind(this));
                this.setFeaturePositions(); //initialize the <p> positions
            }

            FeatureSpot.prototype.setFeaturePositions = function () {
                var eratio = this.element.clientWidth / this.element.clientHeight; //calc the current container aspect ratio
                if (eratio > this.ratio) { // width of scaled bg image is equal to width of container
                    this.scaledHeight = this.element.clientWidth / this.ratio; // pre calc the scaled height of bg image
                    this.scaledDY = (this.scaledHeight - this.element.clientHeight) / 2; // pre calc the amount of the image that is outside the bottom of the container
                    this.spots.each(this.setWide, this); // set the position of each feature marker
                } else { // height of scaled bg image is equal to height of container
                    this.scaledWidth = this.element.clientHeight * this.ratio; // pre calc the scaled width of bg image
                    this.scaledDX = (this.scaledWidth - this.element.clientWidth) / 2; // pre calc the amount of the image that is outside the left of the container
                    this.spots.each(this.setTall, this); // set the position of each feature marker
                }
            }

            FeatureSpot.prototype.setWide = function (spot) {
                spot.set('spotX', spot.get('originalSpotX') * this.element.clientWidth);
                spot.set('spotY', this.scaledHeight * spot.get('originalSpotY') - this.scaledDY); // calc the pixels above the bottom edge of the image - the amount below the container
            }

            FeatureSpot.prototype.setTall = function (spot) {
                spot.set('spotY', spot.get('originalSpotY') * this.element.clientHeight);
                spot.set('spotX', this.scaledWidth * spot.get('originalSpotX') - this.scaledDX); // calc the pixels to the right of the left edge of image - the amount left of the container
            }

            var computedStyle = getComputedStyle(document.body),
                image = new Image(),
                src = computedStyle.backgroundImage.replace(/url\((['"])?(.*?)\1\)/gi, '$2');
            image.onload = function () {
                var x = new FeatureSpot(document.body, that.collection, this.width, this.height);
                new FeatureImage(document.body, examAvatarPosition, this.width, this.height);
            };
            image.src = src;
        },

        onBeforeShow: function () {
            if (this.data.hasExamReport) {
                if (!App.SimulationState.hasSeenGuideElement('btnExamReport')) {
                    this.ui.btnExamReport.addClass("lookatme");
                }
            } else {
                // no exam report
                if (this.isDestroyed) {
                    return;
                }
                App.SimulationState.addGuideElement('btnExamReport');
                this.channel.trigger('clinicalExam:report');
            }
        },

        /**
         * A la destruction :
         * - suppression des classes CSS
         * - suppression des listeners
         */
        onDestroy: function () {
            $("body").removeClass("clinical-exam no-overlay").css("background-position", "").css("background-size", "");
            this.channel.off('clinicalExam:renderEnvironment', this.renderEnvironment);
        },

        serializeData: function () {
            var viewData = {
                data: this.data
            };

            return _.extend(viewData, Marionette.CompositeView.prototype.serializeData.apply(this, arguments));
        }
    });

    return ClinicalExamView;
});
