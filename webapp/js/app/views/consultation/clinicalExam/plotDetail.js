define("views/consultation/clinicalExam/plotDetail", function (require) {
    "use strict";

    var Radio = require("backbone.radio"),
        i18n = require("i18n"),
        BaseLayoutView = require("views/base"),
        ModalLayoutView = require("views/modal/modal"),
        HeaderWithTitleView = require("views/modal/headerWithTitle"),
        ContentTextView = require("views/modal/contentText"),
        PlotItemTemplate = require("hbs!templates/consultation/clinicalExam/plotDetail"),
        PlotItemView;

    PlotItemView = BaseLayoutView.extend({
        tagName: "li",
        template: PlotItemTemplate,

        ui: {
            "detail": "button"
        },
        events: {
            'click @ui.detail': 'showDetails'
        },

        initialize: function () {
            this.channel = Radio.channel("App");
        },

        showDetails: function (e) {
            e.preventDefault();

            if (this.model.get('itemList').ClinicalExamItemVO.camera) {
                var camera = this.model.get('itemList').ClinicalExamItemVO.camera;
                var environement = App.consultation.getEnvironnement(camera),
                    caseId = App.consultation.get("caseId"),
                    consultationId = App.consultation.get("consultationId"),
                    folderName = "c" + caseId + "_c" + consultationId;
                if (environement && environement.environmentBackground) {
                    this.channel.trigger('clinicalExam:renderEnvironment', "/data/assets/" + folderName + "/images/" + environement.environmentBackground);
                    return;
                }
            }

            var popupPlotDetail = new ModalLayoutView({
                className: "overlay overlay-clinical-exam overlay-with-padding",
                headerView: new HeaderWithTitleView({
                    data: {
                        title: i18n.t("consultation.consultationSteps.clinicalExamination.title"),
                    },
                    hideBtn: true,
                    backBtn: true
                }),
                contentView: new ContentTextView({
                    data: {
                        subtitle: this.model.get("examTitle"),
                        list: this.model.get("itemList").ClinicalExamItemVO,
                        height: window.innerHeight - 75
                    },
                }),
                footerView: null
            });

            popupPlotDetail.setHeaderClassName("overlay-header clearfix");
            popupPlotDetail.showModal();
        }
    });

    return PlotItemView;
});
