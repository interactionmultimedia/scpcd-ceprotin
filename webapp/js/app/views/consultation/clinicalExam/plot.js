define("views/consultation/clinicalExam/plot", function(require) {
    "use strict";

    var Marionette               = require("marionette"),
        PlotItemView             = require("views/consultation/clinicalExam/plotDetail"),
        PlotTemplate             = require("hbs!templates/consultation/clinicalExam/plot"),
        PlotView;

    PlotView = Marionette.CompositeView.extend({
        template: PlotTemplate,
        tagName: "div",
        childView: PlotItemView,
        childViewContainer: '#symptoms',
        ui: {
            "btnPlot": ".hotspot"
        },
        events: {
            'click @ui.btnPlot': 'toggleDetails'
        },
        modelEvents: {
            "change": "render"
        },

        initialize: function() {
            this.collection = this.model.get("details");
        },

        toggleDetails: function(e){
            e.preventDefault();

            $("body")
                .find(this.ui.btnPlot.selector)
                .not(this.$el.find(this.ui.btnPlot.selector))
                .removeClass("is-active");

            this.$el
                .find(this.ui.btnPlot.selector)
                .toggleClass("is-active");
        }
    });

    return PlotView;
});
