define("views/consultation/feedback/content", function(require) {
    "use strict";

    var Marionette          = require("marionette"),
        Radio               = require("backbone.radio"),
        $                   = require("jquery"),
        QuizView            = require("views/consultation/feedback/contentQuiz"),
        ModalLayoutView     = require("views/modal/modal"),
        HeaderView          = require("views/consultation/feedback/details/headerWithNavigation"),
        ContentDetailsView  = require("views/consultation/feedback/details/content"),
        QuizzesCollection   = require("collections/quizzes"),
        CustomScrollbarBehavior     = require("views/behaviors/customScrollbarBehavior"),
        ContentView;

    ContentView = Marionette.CollectionView.extend({
        childView: QuizView,
        className: "synthesis-list",
        tagName: "div",
        behaviors: {
            CustomScrollbarBehavior: {
                behaviorClass: CustomScrollbarBehavior
            }
        },

        initialize: function() {
            var that        = this,
                cpt         = App.consultation.get("quizzes").length,
                moduleOrder = ( App.consultation.appData.StepsOrder ? App.consultation.appData.StepsOrder.Step : [] );

            this.channel = Radio.channel("App");
            this.channel.on("feedback:show:details", this.showDetails.bind(this), this);
            this.channel.on("modal:feedbackDetail:backWaitingRoom", this.backWaitingRoom.bind(this), this);
            this.channel.on("modal:feedbackDetail:back", this.hideDetails.bind(this), this);

            this.collection = new QuizzesCollection(App.consultation.get("quizzes").sortBy(function(object) {
                var index   = _.indexOf(moduleOrder, object.get("moduleName"));
                return ( index !== -1 ? index : cpt++ );
            }));
        },

        closeDetail: function () {
            if(this.popupFeedbackDetail){
                this.popupFeedbackDetail.hideModal();
                this.popupFeedbackDetail.destroy();
            }
        },

        hideDetails: function() {
            this.closeDetail();
            this.channel.trigger('modal:feedback:hideDetails');
        },

        backWaitingRoom: function() {
            this.closeDetail();
            this.channel.trigger('modal:feedback:close');
        },

        showDetails: function(module) {
            if(!module){
                return;
            }

            this.channel.trigger("modal:feedback:showDetails");

            this.popupFeedbackDetail = new ModalLayoutView({
                className: "overlay overlay-consultation-synthesis overlay-with-padding",
                headerView: new HeaderView(),
                contentView: new ContentDetailsView({
                    module: module,
                    model: App.consultation.get("quizzes").getQuizByModuleName(module.get("moduleName"))
                }),
                footerView: null
            });

            this.popupFeedbackDetail.setHeaderClassName("overlay-header clearfix");
            this.popupFeedbackDetail.showModal();
        },

        onBeforeRender: function() {
            this.trigger("CustomScrollbarBehavior:init", {
                $el: this.$el,
                height: window.innerHeight - 175
            });
        },

        onDestroy: function() {
            this.channel.off(null, null, this);

            if(this.popupFeedbackDetail){
                this.popupFeedbackDetail.hideModal();
                this.popupFeedbackDetail.destroy();
                this.popupFeedbackDetail = null;
            }
        }
    });

    return ContentView;
});
