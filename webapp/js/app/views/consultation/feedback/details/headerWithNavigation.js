define("views/consultation/feedback/details/headerWithNavigation", function(require) {
    "use strict";

    var Radio                           = require("backbone.radio"),
        BaseLayoutView                  = require("views/base"),
        HeaderWithNavigationTemplate    = require("hbs!templates/consultation/feedback/details/headerWithNavigation"),
        HeaderWithNavigationView;

    HeaderWithNavigationView = BaseLayoutView.extend({
        template: HeaderWithNavigationTemplate,
        ui: {
            'btnBackConsultation': '.btn-back',
            'btnBack': '.btn-back-circle'
        },
        events: {
            'click @ui.btnBackConsultation': 'backWaitingToom',
            'click @ui.btnBack': 'back'
        },

        initialize: function () {
            this.channel    = Radio.channel("App");

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },

        backWaitingToom: function (e) {
            e.preventDefault();

            this.channel.trigger('modal:feedbackDetail:backWaitingRoom');
        },

        back: function (e) {
            e.preventDefault();

            this.channel.trigger('modal:feedbackDetail:back');
        }
    });

    return HeaderWithNavigationView;
});
