define("views/consultation/feedback/details/contentItem", function(require) {
    "use strict";

    var i18n                        = require("i18n"),
        BaseLayoutView              = require("views/base"),
        CustomScrollbarBehavior     = require("views/behaviors/customScrollbarBehavior"),
        ItemTemplate                = require("hbs!templates/consultation/feedback/details/contentItem"),
        ContentView;

    ContentView = BaseLayoutView.extend({
        tagName: "div",
        className: "item-question",
        template: ItemTemplate,
        ui:{
            'btnComments': '.btn-comments',
            'btnClose': '.btn-close-comments',
            'commentsBubble': '.comments-bubble'
        },
        events:{
            'click @ui.btnComments': 'toggleComments',
            'click @ui.btnClose': 'closeComments'
        },
        behaviors: {
            CustomScrollbarBehavior: {
                behaviorClass: CustomScrollbarBehavior
            }
        },

        initialize: function(options) {
            this.module     = App.consultation.getStep(options.moduleName, false);

            this.model.set("index", (options.childIndex+1));
            this.model.set("moduleTitle", options.moduleTitle);
            this.model.set("className", this.module.get("classNameSynthesis"));

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },

        toggleComments: function(e) {
            e.preventDefault();

            var that        = this,
                isOpened    = this.ui.commentsBubble.hasClass("is-open"),
                h           = this.$el.height() - 86;

            this.ui.commentsBubble
                .animate({
                    "opacity": !Number(isOpened),
                    "min-height":  isOpened ? 0 : h + "px",
                })
                .toggleClass("is-open");

            if(!isOpened){
                that.$el.find('.custom-scroll').height(500);
                that.trigger("CustomScrollbarBehavior:init", {
                    $el: that.$el.find('.custom-scroll'),
                    height: "100%",
                    minHeight: "100%"
                });
            }
        },

        closeComments: function(e) {
            e.preventDefault();

            this.ui.commentsBubble
                .removeClass("is-open")
                .animate({
                    "opacity": 0,
                    "min-height": 0
                });
        }
    });

    return ContentView;
});
