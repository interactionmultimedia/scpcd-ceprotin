define("views/consultation/feedback/details/content", function(require) {
    "use strict";

    var Marionette                  = require("marionette"),
        i18n                        = require("i18n"),
        $                           = require("jquery"),
        BaseLayoutView              = require("views/base"),
        ItemView                    = require("views/consultation/feedback/details/contentItem"),
        ContentDetailsView;

    ContentDetailsView = Marionette.CollectionView.extend({
        childView: ItemView,
        className: "overlay-main",
        tagName: "div",
        module: {},
        initialize: function(options) {

            this.childViewOptions = function(model, index) {
                return {
                    moduleTitle: i18n.t(options.module.get("slugTitle")),
                    moduleName: i18n.t(options.module.get("moduleName")),
                    childIndex: index
                };
            };

            this.collection = this.model.get("questions");
        }
    });

    return ContentDetailsView;
});
