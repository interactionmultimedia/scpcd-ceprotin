define("views/consultation/feedback/headerWithNavigation", function(require) {
    "use strict";

    var Radio                           = require("backbone.radio"),
        BaseLayoutView                  = require("views/base"),
        HeaderWithNavigationTemplate    = require("hbs!templates/consultation/feedback/headerWithNavigation"),
        HeaderWithNavigationView;

    HeaderWithNavigationView = BaseLayoutView.extend({
        template: HeaderWithNavigationTemplate,
        ui: {
            'btnEnd': '.btn-done',
            'btnBack': '.btn-back-circle',
        },
        events: {
            'click @ui.btnEnd': 'done',
            'click @ui.btnBack': 'back',
        },

        initialize: function () {
            this.channel    = Radio.channel("App");

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },

        done: function (e) {
            e.preventDefault();

            this.channel.trigger('modal:feedback:close');
            this.channel.trigger('screen:feedback:exit');
        },

        back: function(e) {
            e.preventDefault();

            this.channel.trigger('modal:feedback:close');
        }
    });

    return HeaderWithNavigationView;
});
