define("views/consultation/feedback/footer", function(require) {
    "use strict";

    var Radio               = require("backbone.radio"),
        BaseLayoutView      = require("views/base"),
        FooterTemplate      = require("hbs!templates/consultation/feedback/footer"),
        FooterView;

    FooterView = BaseLayoutView.extend({
        template: FooterTemplate,
        ui: {
            'btnNext': '.btn-red'
        },
        events: {
            'click @ui.btnNext': 'next'
        },
        data: {},

        initialize: function () {
            this.channel    = Radio.channel("App");

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },

        onBeforeRender: function() {
            //this.data.last = App.consultation.get("last");
            this.data.last = false;
        },

        next: function (e) {
            e.preventDefault();

            if(App.consultation.get("last")){
                this.channel.trigger("modal:feedback:end");
            }
        }
    });

    return FooterView;
});
