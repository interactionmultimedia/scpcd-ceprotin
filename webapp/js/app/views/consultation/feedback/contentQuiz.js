define("views/consultation/feedback/contentQuiz", function(require) {
    "use strict";

    var Radio           = require("backbone.radio"),
        i18n            = require("i18n"),
        BaseLayoutView  = require("views/base"),
        ContentDetailsView  = require("views/consultation/feedback/details/content"),
        QuizTemplate    = require("hbs!templates/consultation/feedback/quizTitle"),
        ContentView;

    ContentView = BaseLayoutView.extend({
        tagName: "section",
        template: QuizTemplate,
        ui:{},
        regions: {
            'content': '.feedback-content'
        },
        data: {},

        initialize: function() {
            this.channel    = Radio.channel("App");
            this.module     = App.consultation.getStep(this.model.get("moduleName"), false);

            if(!this.contentDetailsView || (this.contentDetailsView && this.contentDetailsView.isDestroyed)) {
                this.contentDetailsView = new ContentDetailsView({
                    module: this.module,
                    model: App.consultation.get("quizzes").getQuizByModuleName(this.module.get("moduleName"))
                });
            }

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },

        onBeforeRender: function() {
            if( !!this.module ) {
                this.data.title  = i18n.t(this.module.get("slugTitle"));
                this.$el.addClass(this.module.get("classNameSynthesis"));
            }
        },

        onBeforeShow: function(){
            this.getRegion('content').show(this.contentDetailsView);
        }
    });

    return ContentView;
});
