define("views/consultation/steps/content", function (require) {
    "use strict";

    var BaseLayoutView = require("views/base"),
        Radio = require("backbone.radio"),
        i18n = require("i18n"),
        ContentTemplate = require("hbs!templates/consultation/steps/menu"),
        ContentView;

    ContentView = BaseLayoutView.extend({
        template: ContentTemplate,
        className: "consultation-menu-container",
        currentActiveStep: null,
        ui: {
            'step': 'button',
        },
        events: {
            'click @ui.step': 'selectStep',
        },

        initialize: function () {
            this.channel = Radio.channel("App");
            this.collection = App.consultation.getAvailableSteps();
            this.channel.on('step:select', this.changeStep.bind(this));
            this.channel.on('quitted:step', this.quittedStep.bind(this));
            this.channel.on('clinicalExam:report', this.afterClinicalExam.bind(this));
        },

        /**
         * A la destruction :
         * - suppression des classes CSS
         * - suppression des listeners
         */
        onDestroy: function () {
            this.channel.off('step:select', this.changeStep);
            this.channel.off('quitted:step');
        },

        onBeforeShow: function () {
            this.prepareGuides();
        },

        /**
         * Action sur un bouton d'étape de consultation
         */
        selectStep: function (args) {
            var moduleName = $(args.currentTarget).attr('data-module');
            if (moduleName == "quit" || moduleName == this.currentActiveStep) {
                this.quitStep();
                return;
            } else if (moduleName == "finish") {
                this.channel.trigger("consultation:ended");
                return;
            }
            $(args.currentTarget).addClass('active');
            this.channel.trigger("step:select", {
                moduleName: moduleName
            });
        },

        changeStep: function (args) {
            this.currentActiveStep = args.moduleName;
        },

        quitStep: function () {
            if (this.currentActiveStep != null && !this.isDestroyed) {
                this.channel.trigger('step:quit', this.currentActiveStep);
                this.currentActiveStep = null;
            }
        },

        quittedStep: function () {
            if (this.currentActiveStep != null && !this.isDestroyed) {
                this.ui.step.filter('[data-module="' + this.currentActiveStep + '"]').removeClass('active');
                this.currentActiveStep = null;
            }
        },

        showGuideElement: function (which) {
            switch (which) {
                case "btnMedicalFiles":
                    if (!App.SimulationState.hasSeenGuideElement('btnMedicalFiles')) {
                        this.$el.find('[data-module="MedicalFiles"]').addClass("lookatme").siblings().removeClass('lookatme');
                    }
                    break;
                case "btnClinicalExam":
                    if (!App.SimulationState.hasSeenGuideElement('btnClinicalExam')) {
                        this.$el.find('[data-module="ClinicalExaminationModule"]').addClass("lookatme").siblings().removeClass('lookatme');
                    }
                    break;
                case "btnChatIntro":
                    if (!App.SimulationState.hasSeenGuideElement('btnChatIntro')) {
                        this.$el.find('[data-module="ChatIntro"]').addClass("lookatme").siblings().removeClass('lookatme');
                    }
                    break;
            }
        },

        removeGuideElement: function (which) {
            switch (which) {
                case "btnChatIntro":
                    this.$el.find('[data-module="ChatIntro"]').removeClass("lookatme");
                    App.SimulationState.addGuideElement('btnChatIntro');
                    break;
                case "btnMedicalFiles":
                    this.$el.find('[data-module="ChatIntro"]').removeClass("lookatme");
                    this.$el.find('[data-module="MedicalFiles"]').removeClass("lookatme");
                    App.SimulationState.addGuideElement('btnChatIntro');
                    App.SimulationState.addGuideElement('btnMedicalFiles');
                    break;
                case "btnClinicalExam":
                    this.$el.find('[data-module="ChatIntro"]').removeClass("lookatme");
                    this.$el.find('[data-module="MedicalFiles"]').removeClass("lookatme");
                    this.$el.find('[data-module="ClinicalExaminationModule"]').removeClass("lookatme");
                    App.SimulationState.addGuideElement('btnMedicalFiles');
                    App.SimulationState.addGuideElement('btnChatIntro');
                    App.SimulationState.addGuideElement('btnClinicalExam');
                    break;
            }
        },

        prepareGuides: function () {
            var that = this;
            this.$el.find('[data-module="ClinicalExaminationModule"]').nextAll().click(function () {
                var isModuleAlreadyDone = _.reduce($(this).nextAll(), function (memo, el) {
                    var module = $(el).attr('data-module');
                    return memo || App.SimulationState.hasSeenGuideElement(module);
                }, false);
                if (!isModuleAlreadyDone) {
                    App.SimulationState.addGuideElement('btnMedicalFiles');
                    App.SimulationState.addGuideElement('btnChatIntro');
                    App.SimulationState.addGuideElement('btnClinicalExam');
                    $(this).siblings().removeClass("lookatme");
                    var nextStep = $(this).removeClass("lookatme").next().attr('data-module');
                    if (!App.SimulationState.hasSeenGuideElement(nextStep)) {
                        that.$el.find('[data-module="' + nextStep + '"]').addClass("lookatme");
                        App.SimulationState.addGuideElement(nextStep);
                    }
                }
            });
        },

        afterClinicalExam: function() {
            if (this.isDestroyed) {
                return;
            }
            var $nextStep = this.$el.find('[data-module="ClinicalExaminationModule"]').next();
            var nextStep = $nextStep.attr('data-module');
            if(!App.SimulationState.hasSeenGuideElement(nextStep)) {
                $nextStep.addClass('lookatme');
                App.SimulationState.addGuideElement(nextStep);
            }
        }
    });

    return ContentView;
});
