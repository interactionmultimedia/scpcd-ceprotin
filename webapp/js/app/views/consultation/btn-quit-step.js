define("views/consultation/btn-quit-step", function(require) {
    "use strict";

    var BaseLayoutView              = require("views/base"),
        Radio                       = require("backbone.radio"),
        QuitStepTemplate            = require("hbs!templates/consultation/btn-quit-step"),
        QuitStepView;

    QuitStepView = BaseLayoutView.extend({
        tagName: "button",
        className: "btn-quit btn-consultation-steps",
        template: QuitStepTemplate,
        events: {
            'click': 'quitStep'
        },
        initialize: function(options) {
            this.channel    = Radio.channel("App");

            this.moduleName = options.moduleName || "";
            this.data       = options.data || {};

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },
        onBeforeShow: function() {
            this.$el.attr("href", "#");
        },
        quitStep: function(e) {
            e.preventDefault();

            this.channel.trigger('step:quit', this.moduleName);
        }
    });

    return QuitStepView;
});
