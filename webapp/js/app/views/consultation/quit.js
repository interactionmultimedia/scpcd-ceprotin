define("views/consultation/quit", function(require) {
    "use strict";

    var i18n                        = require("i18n"),
        BaseLayoutView              = require("views/base"),
        Radio                       = require("backbone.radio"),
        QuitTemplate                = require("hbs!templates/consultation/quit"),
        QuitView;

    QuitView = BaseLayoutView.extend({
        template: QuitTemplate,
        ui: {
            'btnBack': '.js-back',
            'btnExit': '.js-exit'
        },
        events:{
            'click @ui.btnBack': 'backConsultation',
            'click @ui.btnExit': 'quitConsultation'
        },
        initialize: function() {
            this.channel    = Radio.channel("App");
            this.channel.trigger("click:quitConsultation");
        },

        /**
         * Action sur le bouton "back"
         */
        backConsultation: function(e) {
            e.preventDefault();
            this.channel.trigger("consultation:back");
        },

        /**
         * Action sur le bouton "quit"
         */
        quitConsultation: function(e) {
            e.preventDefault();
            this.channel.trigger("consultation:quit");
        },

        onBeforeRender: function() {
            var labelConfirm,
                labelBtnQuit;

            if( App.Config.isRP() && !!App.Participant ) {
                labelConfirm  = i18n.t("consultation.finishMessage");
                labelBtnQuit  = i18n.t("action.finish");
            } else {
                labelConfirm  = i18n.t("consultation.exitMessage");
                labelBtnQuit  = i18n.t("action.quit");
            }

            this.data   = {
                labelConfirm: labelConfirm,
                labelBtnQuit: labelBtnQuit
            };
        },

        onShow: function() {
            $("body").addClass("quit-consultation");
        },

        onDestroy: function() {
            $("body").removeClass("quit-consultation");
        }
    });

    return QuitView;
});
