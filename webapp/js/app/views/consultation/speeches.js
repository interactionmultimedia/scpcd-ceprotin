define('views/consultation/speeches', function (require) {
    "use strict";

    var Radio = require("backbone.radio"),
        $ = require("jquery"),
        _ = require("underscore"),
        SpeechesCollection = require("collections/speeches"),
        BaseLayoutView = require("views/base"),
        SpeechTemplate = require("hbs!templates/consultation/speech"),
        SpeechIntroTemplate = require("hbs!templates/consultation/speechIntro"),
        CallTemplate = require("hbs!templates/consultation/call"),
        SpeecheslayoutView;

    SpeecheslayoutView = BaseLayoutView.extend({
        template: "#speeches",
        data: {
            offsetTop: 0,
        },
        initialBackground: null,
        initialize: function (options) {
            var step = App.consultation.getStep("ChatIntro");

            this.moduleName = options.moduleName;

            this.baseUrl = "/data/assets/c" + App.consultation.get("caseId") + "_c" + App.consultation.get("consultationId") + "/";

            var ChatIntro = App.consultation.getModuleByName("ChatIntro");
            if (ChatIntro && ChatIntro.ModuleVO && ChatIntro.ModuleVO.content && ChatIntro.ModuleVO.content.ContentVO) {
                this.data.offsetTop = ChatIntro.ModuleVO.content.ContentVO.offsetTop;
            }

            this.initialBackground = $("body").css("background-image");

            this.collection = new SpeechesCollection(step.getSpeeches(), step.getSpeechesAvatars());

            this.channel = Radio.channel('App');
            this.channel.on('consultation:speech:play', this.showSpeech.bind(this), this);
            this.channel.on('consultation:speech:endPlay', this.launchReply.bind(this), this);
            this.channel.on('consultation:speech:reply', this.showSpeechReply.bind(this), this);
            this.channel.on('consultation:speech:ended', this.endSpeech.bind(this), this);
        },
        onBeforeShow: function () {
            $("body").removeClass("consultation").addClass("consultation-dialogue");

            this.showIntro();
        },
        onDestroy: function () {
            $("body").removeClass("consultation-dialogue").addClass("consultation");
            $('body').removeClass('speech-intro');

            if (this.textInterval) {
                clearInterval(this.textInterval);
            }

            $('body').css('background-image', this.initialBackground);

            this.channel.off(null, null, this);
        },
        showIntro: function() {
            var step = App.consultation.getStep("ChatIntro"),
                that = this,
                avatars = _.filter(step.getSpeechesAvatars(), function(avatar) { return avatar.active; }),
                sortedAvatars = _.sortBy(avatars, function(avatar){ return (avatar.position == 'right') ? 1 : 0; }),
                template = SpeechIntroTemplate({
                    avatars: sortedAvatars,
                    baseUrl: this.baseUrl
                });
            $('body').addClass('speech-intro');
            this.$el
                .empty()
                .append(
                    $(template)
                )
                .fadeIn("fast");
            this.$el.find('.btn-start').click(function () {
                that.$el.empty();
                that.startSpeech.call(that);
                App.SimulationState.addGuideElement('btnStartSpeech');
            });
            if (!App.SimulationState.hasSeenGuideElement('btnStartSpeech')) {
                this.$el.find('.btn-start').addClass("lookatme");
            }
        },
        startSpeech: function() {
            $('body').removeClass('speech-intro');
            this.channel.trigger('consultation:speech:play', this.collection.first());
        },
        showSpeech: function (speech) {
            var that = this;

            if (!!speech.get('text')) {
                setTimeout(function () {
                    that.showText(speech);
                }, 800);
            } else {
                this.channel.trigger('consultation:speech:endPlay', speech);
            }
        },
        showSpeechReply: function (speech) {
            var that = this,
                text = speech.get('text');

            if (!!text) {
                setTimeout(function () {
                    that.showText(speech);
                }, 800);
            } else {
                this.channel.trigger('consultation:speech:ended', speech);
            }
        },
        launchReply: function (speech) {
            $('body').removeClass('speech-intro');
            this.channel.trigger('consultation:speech:ended', speech);
        },
        endSpeech: function (speech) {
            var index = this.collection.indexOf(speech);

            this.hideSpeech();

            if (index + 1 >= this.collection.length) {
                this.channel.trigger('consultation:speech:stepEnded', this.moduleName);
                return;
            }

            var nextSpeech = this.collection.at(index + 1);
            if (nextSpeech.get('camera') != null) {
                var environnement = App.consultation.getEnvironnement(nextSpeech.get('camera')),
                    caseId = App.consultation.get("caseId"),
                    consultationId = App.consultation.get("consultationId"),
                    folderName = "c" + caseId + "_c" + consultationId;
                    $("body").css("background-image", "url('/data/assets/" + folderName + "/images/" + environnement.environmentBackground + "')");
            }

            if (_.isArray(nextSpeech.get('deactivate'))) {
                this.channel.trigger('consultation:speech:deactivate', nextSpeech.get('deactivate'));
            }

            if (_.isArray(nextSpeech.get('activate'))) {
                this.channel.trigger('consultation:speech:activate', nextSpeech.get('activate'));
            }

            if (nextSpeech.get('type') == 'sound') {
                this.channel.trigger('consultation:speech:play', nextSpeech);
            } else if (nextSpeech.get('type') == 'call') {
                this.showCall(nextSpeech);
            }
        },
        hideSpeech: function () {
            this.channel.trigger('speech:idle');
            this.$el.fadeOut("fast");
        },
        showCall: function (speech) {
            var that = this,
                template = CallTemplate({
                    text: speech.get('text'),
                    avatar: this.baseUrl + speech.get('avatar').get('avatar')
                });
            this.$el
                .empty()
                .append(
                    $(template)
                )
                .fadeIn("fast");
            this.$el.find('.btn-call').click(function () {
                that.$el.empty();
                that.launchReply(speech);
            });
            $('body').addClass('speech-intro');
            this.$el.find('.btn-call').addClass("lookatme");
        },
        showText: function (speech) {
            var that = this,
                template = SpeechTemplate({
                    className: "speech-" + speech.get('avatar').get('position'),
                    speechText: speech.get('text'),
                    avatar: this.baseUrl + speech.get('avatar').get('avatar'),
                    data: this.data
                });

            this.$el
                .empty()
                .append(
                    $(template)
                )
                .fadeIn("fast", function () {
                    that.channel.trigger('speech:talk', speech.get('avatar'));
                    var $el = $(this).find('.bubble-inner p'),
                        height = ~~$el.height(),
                        textOffset = 25,
                        currentOffset = 0,
                        maxOffset = 0;

                    if (height > 60) {
                        maxOffset = height - 60;

                        setTimeout(function () {
                            that.textInterval = setInterval(function () {
                                if (currentOffset <= maxOffset) {
                                    currentOffset += textOffset;
                                    $el.animate({
                                        "marginTop": "-" + currentOffset + "px"
                                    });
                                }
                            }, 4000);
                        }, 4500);
                    }
                });
        }
    });

    return SpeecheslayoutView;
});
