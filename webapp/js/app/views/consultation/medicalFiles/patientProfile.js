define("views/consultation/medicalFiles/patientProfile", function(require) {
    "use strict";

    var Radio                   = require("backbone.radio"),
        BaseLayoutView          = require("views/base"),
        PatientProfileTemplate  = require("hbs!templates/consultation/medicalFiles/patientProfile"),
        PatientProfileView;

    PatientProfileView = BaseLayoutView.extend({
        template: PatientProfileTemplate,

        initialize: function() {
            this.model      = App.consultation.get("profile");
            this.baseUrl    = "/data/assets/c" + App.consultation.get("caseId") + "_c" + App.consultation.get("consultationId") + "/";
        },

        onBeforeRender: function() {
            var patientProfilePicture = App.consultation.getPatientPictureProfile();

            this.data   = {
                patientFullName: App.consultation.getPatientFullName(),
                patientProfilePicture: ( !!patientProfilePicture ? this.baseUrl + patientProfilePicture + ".png" : "" )
            };
        }
    });

    return PatientProfileView;
});
