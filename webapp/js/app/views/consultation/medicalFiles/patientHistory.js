define("views/consultation/medicalFiles/patientHistory", function(require) {
    "use strict";

    var BaseLayoutView          = require("views/base"),
        PatientHistoryTemplate  = require("hbs!templates/consultation/medicalFiles/patientHistory"),
        PatientHistoryView;

    PatientHistoryView = BaseLayoutView.extend({
        template: PatientHistoryTemplate,

        initialize: function() {
            this.collection = App.consultation.get("historyOfDiseases");
        }
    });

    return PatientHistoryView;
});
