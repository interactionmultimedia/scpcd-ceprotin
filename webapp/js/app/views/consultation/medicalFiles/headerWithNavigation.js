define("views/consultation/medicalFiles/headerWithNavigation", function(require) {
    "use strict";

    var Radio                           = require("backbone.radio"),
        BaseLayoutView                  = require("views/base"),
        HeaderWithNavigationTemplate    = require("hbs!templates/consultation/medicalFiles/headerWithNavigation"),
        HeaderWithNavigationView;

    HeaderWithNavigationView = BaseLayoutView.extend({
        template: HeaderWithNavigationTemplate,
        ui: {
            'btnBack': '.btn-back-circle',
            'btnClose': '.btn-close'
        },
        events: {
            'click @ui.btnBack': 'back',
            'click @ui.btnClose': 'close'
        },

        initialize: function () {
            this.channel    = Radio.channel("App");

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },

        back: function (e) {
            e.preventDefault();

            this.channel.trigger('modal:medicalFiles:back');
        },

        close: function (e) {
            e.preventDefault();

            this.channel.trigger('modal:close');
        }
    });

    return HeaderWithNavigationView;
});
