define("views/consultation/medicalFiles/wayOfLiving", function(require) {
    "use strict";

    var BaseLayoutView              = require("views/base"),
        WayOfLivingTemplate    = require("hbs!templates/consultation/medicalFiles/wayOfLiving"),
        WayOfLivingView;

    WayOfLivingView = BaseLayoutView.extend({
        template: WayOfLivingTemplate,

        initialize: function() {
            this.collection = App.consultation.get("wayOfLiving");
        }
    });

    return WayOfLivingView;
});
