define("views/consultation/medicalFiles/specialistsReport", function(require) {
    "use strict";

    var BaseLayoutView              = require("views/base"),
        SpecialistsReportTemplate   = require("hbs!templates/consultation/medicalFiles/specialistsReports"),
        SpecialistsReportView;

    SpecialistsReportView = BaseLayoutView.extend({
        template: SpecialistsReportTemplate,

        initialize: function() {
            this.collection = App.consultation.get("specialistsReports");
        }
    });

    return SpecialistsReportView;
});
