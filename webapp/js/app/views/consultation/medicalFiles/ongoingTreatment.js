define("views/consultation/medicalFiles/ongoingTreatment", function(require) {
    "use strict";

    var BaseLayoutView              = require("views/base"),
        OngoingTreatmentTemplate    = require("hbs!templates/consultation/medicalFiles/ongoingTreatment"),
        OngoingTreatmentView;

    OngoingTreatmentView = BaseLayoutView.extend({
        template: OngoingTreatmentTemplate,
        data: {
            noItems: false
        },
        initialize: function() {
            this.collection      = App.consultation.get("ongoingTreatments");
            if(this.collection.length == 0) {
                this.data.noItems = true;
            } else {
                this.data.noItems = false;
            }
        },
    });

    return OngoingTreatmentView;
});
