define("views/consultation/medicalFiles/neonatSymptoms", function(require) {
    "use strict";

    var BaseLayoutView              = require("views/base"),
        NeonatSymptomsTemplate    = require("hbs!templates/consultation/medicalFiles/neonatSymptoms"),
        NeonatSymptomsView;

    NeonatSymptomsView = BaseLayoutView.extend({
        template: NeonatSymptomsTemplate,

        initialize: function() {
            this.collection = App.consultation.get("neonatSymptoms");
        }
    });

    return NeonatSymptomsView;
});
