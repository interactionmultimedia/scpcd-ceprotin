define("views/consultation/medicalFiles/examsResultsComplementary", function(require) {
    "use strict";

    var BaseLayoutView                      = require("views/base"),
        ExamsResultsComplementaryTemplate   = require("hbs!templates/consultation/medicalFiles/examsResultsComplementary"),
        ExamsResultsComplementaryView;

    ExamsResultsComplementaryView = BaseLayoutView.extend({
        template: ExamsResultsComplementaryTemplate,

        initialize: function() {
            this.collection = App.consultation.get("examsResultsComplementary");
        }
    });

    return ExamsResultsComplementaryView;
});
