define("views/consultation/medicalFiles/contentItem", function(require) {
    "use strict";

    var BaseLayoutView      = require("views/base"),
        ContentItemTemplate = require("hbs!templates/consultation/medicalFiles/contentItem"),
        ContentItemView;

    ContentItemView = BaseLayoutView.extend({
        tagName: "li",
        template: ContentItemTemplate,
        ui: {
            'btnMedicalFile': 'a'
        },
        triggers: {
            "click @ui.btnMedicalFile": {
                event: "medicalFiles:select",
                preventDefault: true
            }
        },
        initialize: function(options) {
            this.showGuideElement   = ( typeof options.showGuideElement !== "undefined" ? options.showGuideElement : false );
        },
        onBeforeShow: function() {
            this.$el.addClass(this.model.get("className"));

            if( this.showGuideElement ) {
                this.$el.find(this.ui.btnMedicalFile).addClass("lookatme");
            }
        }
    });

    return ContentItemView;
});
