define("views/consultation/medicalFiles/familyBackground", function(require) {
    "use strict";

    var BaseLayoutView              = require("views/base"),
        FamilyBackgroundTemplate    = require("hbs!templates/consultation/medicalFiles/familyBackground"),
        FamilyBackgroundView;

    FamilyBackgroundView = BaseLayoutView.extend({
        template: FamilyBackgroundTemplate,

        initialize: function() {
            this.collection = App.consultation.get("familyBackground");
        }
    });

    return FamilyBackgroundView;
});
