define("views/consultation/medicalFiles/laboratoryResults", function(require) {
    "use strict";

    var BaseLayoutView          = require("views/base"),
        ExamsResultsTemplate    = require("hbs!templates/consultation/medicalFiles/examsResults"),
        LaboratoryResultsView;

        LaboratoryResultsView = BaseLayoutView.extend({
        template: ExamsResultsTemplate,

        initialize: function() {
            this.collection = App.consultation.get("laboratoryResults");
        }
    });

    return LaboratoryResultsView;
});
