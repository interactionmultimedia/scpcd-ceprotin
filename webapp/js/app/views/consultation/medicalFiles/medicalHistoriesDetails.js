define("views/consultation/medicalFiles/medicalHistoriesDetails", function(require) {
    "use strict";

    var BaseLayoutView              = require("views/base"),
        MedicalHistoriesDetailsView;

    MedicalHistoriesDetailsView = BaseLayoutView.extend({
        
    });

    return MedicalHistoriesDetailsView;
});
