define("views/consultation/medicalFiles/contentWithNavigation", function (require) {
    "use strict";

    var Radio = require("backbone.radio"),
        $ = require("jquery"),
        BaseLayoutView = require("views/base"),
        CustomScrollbarBehavior = require("views/behaviors/customScrollbarBehavior"),
        ContentWithNavigationTemplate = require("hbs!templates/consultation/medicalFiles/contentWithNavigation"),
        ContentWithNavigationView;

    ContentWithNavigationView = BaseLayoutView.extend({
        template: ContentWithNavigationTemplate,

        behaviors: {
            CustomScrollbarBehavior: {
                behaviorClass: CustomScrollbarBehavior
            }
        },

        ui: {
            'btnPrevious': '.btn-back',
            'btnNext': '.btn-next'
        },

        events: {
            'click @ui.btnPrevious': 'showPrevious',
            'click @ui.btnNext': 'showNext'
        },

        regions: {
            'inner': '.overlay-main-inner'
        },

        initialize: function (options) {
            var that = this;

            this.moduleIndex = ~~options.moduleIndex;
            this.moduleIndexMax = ~~options.moduleIndexMax;
            this.contentView = options.contentView;

            this.channel = Radio.channel("App");

            this.getRegion("inner").on("show", function () {
                that.trigger("CustomScrollbarBehavior:destroy", {
                    $el: that.getRegion("inner").$el
                });
                that.trigger("CustomScrollbarBehavior:init", {
                    $el: that.getRegion("inner").$el,
                    height: window.innerHeight - 160 
                });
            });
        },

        onBeforeShow: function () {
            this.showChildView("inner", this.contentView);

            if (this.moduleIndex === 0) {
                $(this.ui.btnPrevious).hide();
            }

            if (this.moduleIndex === this.moduleIndexMax) {
                $(this.ui.btnNext).hide();
                this.channel.trigger("medicalFiles:guide:ended");
            }
        },

        onShow: function () {
            App.SimulationState.addGuideElement('btnMedicalFile' + this.moduleIndex);
        },

        showPrevious: function (e) {
            e.preventDefault();

            this.channel.trigger('modal:medicalFiles:previous', this.moduleIndex);
        },

        showNext: function (e) {
            e.preventDefault();

            this.channel.trigger('modal:medicalFiles:next', this.moduleIndex);
        }
    });

    return ContentWithNavigationView;
});
