define("views/consultation/medicalFiles/examsResults", function(require) {
    "use strict";

    var BaseLayoutView          = require("views/base"),
        ExamsResultsTemplate    = require("hbs!templates/consultation/medicalFiles/examsResults"),
        ExamsResultsView;

    ExamsResultsView = BaseLayoutView.extend({
        template: ExamsResultsTemplate,

        initialize: function() {
            this.collection = App.consultation.get("examsResults");
        }
    });

    return ExamsResultsView;
});
