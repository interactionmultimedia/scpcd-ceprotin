define("views/consultation/medicalFiles/medicalHistories", function(require) {
    "use strict";

    var BaseLayoutView                  = require("views/base"),
        MedicalHistoriesDetailsView     = require("views/consultation/medicalFiles/medicalHistoriesDetails"),
        AntecedentsAllergyTemplate      = require("hbs!templates/consultation/medicalFiles/antecedentsAllergy"),
        AntecedentsDiseaseTemplate      = require("hbs!templates/consultation/medicalFiles/antecedentsDisease"),
//        AntecedentsFamilyTemplate       = require("hbs!templates/consultation/medicalFiles/antecedentsFamily"),
//        AntecedentsObstetricTemplate    = require("hbs!templates/consultation/medicalFiles/antecedentsObstetric"),
        AntecedentsOthersTemplate       = require("hbs!templates/consultation/medicalFiles/antecedentsOthers"),
        AntecedentsSurgeryTemplate      = require("hbs!templates/consultation/medicalFiles/antecedentsSurgery"),
        MedicalHistoriesTemplate        = require("hbs!templates/consultation/medicalFiles/medicalHistories"),
        MedicalHistoriesView;

    MedicalHistoriesView = BaseLayoutView.extend({
        template: MedicalHistoriesTemplate,
        regions: {
            'allergy': '#anetcedentsAllergy',
            'disease': '#antecedentsDisease',
            'family': '#antecedentsFamily',
            'obstetric': '#antecedentsObstetric',
            'others': '#antecedentsOthers',
            'surgery': '#antecedentsSurgery'
        },

        initialize: function() {
            this.model  = App.consultation.get("medicalHistories");
        },

        onBeforeShow: function() {
            this.showChildView('allergy', new MedicalHistoriesDetailsView({
                collection: this.model.get("antecedentsAllergy"),
                template: AntecedentsAllergyTemplate
            }));

            this.showChildView('disease', new MedicalHistoriesDetailsView({
                collection: this.model.get("antecedentsDisease"),
                template: AntecedentsDiseaseTemplate
            }));

//            this.showChildView('family', new MedicalHistoriesDetailsView({
//                collection: this.model.get("antecedentsFamily"),
//                template: AntecedentsFamilyTemplate
//            }));

//            this.showChildView('obstetric', new MedicalHistoriesDetailsView({
//                collection: this.model.get("antecedentsObstetric"),
//                template: AntecedentsObstetricTemplate
//            }));
//
            this.showChildView('others', new MedicalHistoriesDetailsView({
                collection: this.model.get("antecedentsOthers"),
                template: AntecedentsOthersTemplate
            }));

            this.showChildView('surgery', new MedicalHistoriesDetailsView({
                collection: this.model.get("antecedentsSurgery"),
                template: AntecedentsSurgeryTemplate
            }));
        }
    });

    return MedicalHistoriesView;
});
