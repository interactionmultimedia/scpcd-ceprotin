define("views/consultation/medicalFiles/content", function(require) {
    "use strict";

    var Marionette      = require("marionette"),
        Radio           = require("backbone.radio"),
        _               = require("underscore"),
        ContentItemView = require("views/consultation/medicalFiles/contentItem"),
        ContentTemplate = require("hbs!templates/consultation/medicalFiles/content"),
        ContentView;

    ContentView = Marionette.CompositeView.extend({
        template: ContentTemplate,
        childView: ContentItemView,
        childViewContainer: '#medical-files',
        data: {},

        childEvents: {
            'medicalFiles:select': 'selectMedicalFile'
        },

        initialize: function() {
            this.channel    = Radio.channel("App");

            this.collection = App.consultation.getAvailableMedicalFiles();
            this.baseUrl    = "/data/assets/c" + App.consultation.get("caseId") + "_c" + App.consultation.get("consultationId") + "/";

            this.indexGuideElement  = null;
            this.childViewOptions   = function(model, index) {
                var showGuideElement = false;
                if( this.indexGuideElement === null && !App.SimulationState.hasSeenGuideElement('btnMedicalFile'+index) ) {
                    this.indexGuideElement  = index;
                    showGuideElement = true;
                }

                return {
                    childIndex: index,
                    showGuideElement: showGuideElement
                };
            };
        },

        onBeforeRender: function() {
            var patientProfilePicture = App.consultation.getPatientPictureProfile();

            this.data   = {
                patientFullName: App.consultation.getPatientFullName(),
                patientProfilePicture: ( !!patientProfilePicture ? this.baseUrl + patientProfilePicture + ".png" : "" )
            };
        },

        selectMedicalFile: function(args) {
            var moduleName = args.model.get("moduleName");

            this.removeGuideElement(args);

            this.channel.trigger("medicalFiles:select", {
                moduleName: moduleName
            });
        },

        removeGuideElement: function(args) {
            var indexMax = this.collection.length-1;
            if( args._index === this.indexGuideElement ) {
                App.SimulationState.addGuideElement('btnMedicalFile'+args._index);

                if( args._index === indexMax ) {
                    this.channel.trigger("medicalFiles:guide:ended");
                }
            } else {
                for(var i=0;i<=indexMax;i++) {
                    App.SimulationState.addGuideElement('btnMedicalFile'+i);
                }

                this.channel.trigger("medicalFiles:guide:ended");
            }
        },

        serializeData: function() {
            var viewData = {
                data: this.data
            };

            return _.extend(viewData, Marionette.CompositeView.prototype.serializeData.apply(this, arguments));
        }
    });

    return ContentView;
});
