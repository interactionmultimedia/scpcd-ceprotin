define('views/consultation/audio-player', function(require) {
    "use strict";

    var $                       = require("jquery"),
        Radio                   = require("backbone.radio"),
        Howler                  = require("howler"),
        i18n                    = require("i18n"),
        BaseLayoutView          = require("views/base"),
        AudioPlayerTemplate     = require("hbs!templates/consultation/audio-player"),
        AudioPlayerLayoutView;

    AudioPlayerLayoutView = BaseLayoutView.extend({
        template: AudioPlayerTemplate,
        isInterlocutor: false,
        ui:{
            'muteButton': '.btn-mute'
        },
        events: {
            'click @ui.muteButton': 'toggleSound'
        },
        initialize: function() {
            var that = this;

            this.channel = Radio.channel('App');
            this.channel.on('consultation:speech:play', this.playSpeechSound.bind(this), this);
            this.channel.on('consultation:speech:reply', this.playSpeechInterlocutorSound.bind(this), this);

            this.currentVolume  = 1;
            this.baseUrl        = "/data/assets/c" + App.consultation.get("caseId") + "_c" + App.consultation.get("consultationId") + "/sounds";

            // FOR DEBUG
            if( !!App.Config.isDev() ) {
                var keys = [];
                $(window).on('keydown', function(e) {
                    if(typeof that.sound === "undefined") {
                        return;
                    }

                    keys[e.keyCode] = e.keyCode;
                    var keysArray = that.getNumberArray(keys);
                    if(keysArray.toString() === "71,84") {
                        that.stopSound();
                    } else if(keysArray.toString() === "80") {
                        that.pauseSound();
                    }
                });
            }

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },
        onDestroy: function() {
            this.stopSound();

            this.channel.off(null, null, this);

            if( !!App.Config.isDev() ) {
                $(window).off('keydown');
            }
        },

        playSpeechSound: function(speech) {
            var that    = this,
                sound   = speech.get('file');

            this.isInterlocutor    = false;

            setTimeout(function() {
                that.playSound(sound, function() {
                    that.channel.trigger('consultation:speech:endPlay', speech);
                });
            }, 1000);
        },
        playSpeechInterlocutorSound: function(speech) {
            var that    = this,
                sound   = speech.get('file');

            this.isInterlocutor    = true;

            setTimeout(function() {
                that.playSound(sound, function() {
                    that.channel.trigger('consultation:speech:ended', speech);
                });
            }, 1000);
        },
        toggleSound: function() {
            var $muteBtn = $(this.ui.muteButton);

            this.currentVolume = $muteBtn.hasClass('is-off') ? 1 : 0;

            if( this.sound ) {
                this.sound.volume(this.currentVolume);
            }

            $muteBtn.toggleClass('is-off');

            if( $muteBtn.hasClass('is-off') ) {
                $muteBtn.text(i18n.t("action.unmute"));
            } else {
                $muteBtn.text(i18n.t("action.mute"));
            }
        },
        playSound: function(sound, endCb) {
            
            if(sound == "" || this.isDestroyed) return;
            
            var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);

            this.stopSound();
            this.sound  = new Howl({
                src: [this.baseUrl + "/" + sound],
                volume: this.currentVolume,
                preload: true,
                onend: endCb
            });

            this.sound.play();

            if(iOS && !App.iosTricked) {
                App.iosTricked = true;
                this.toggleSound();
            }
        },
        stopSound: function() {
            if( this.sound ) {
                this.sound.unload();
            }
        },
        pauseSound: function() {
            if( this.sound ) {
                this.sound.pause();
            }
        },
        getNumberArray: function(arr) {
            var newArr = new Array();
            for(var i = 0; i < arr.length; i++){
                if(typeof arr[i] === "number"){
                    newArr[newArr.length] = arr[i];
                }
            }
            return newArr;
        }
    });

    return AudioPlayerLayoutView;
});
