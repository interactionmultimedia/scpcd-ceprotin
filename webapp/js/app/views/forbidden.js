define("views/forbidden", function(require) {
    "use strict";

    var $                       = require("jquery"),
        BaseLayoutView          = require("views/base"),
        ForbiddenTemplate       = require("hbs!templates/forbidden"),
        ForbiddenView;

    ForbiddenView = BaseLayoutView.extend({
        template: ForbiddenTemplate,
        onBeforeShow: function() {
            $("body").addClass("forbidden");
        },
        onDestroy: function() {
            $("body").removeClass("forbidden");
        }
    });

    return ForbiddenView;
});
