define("views/footer", function(require) {
    "use strict";
    
    var BaseLayoutView  = require("views/base"),
        FooterTemplate  = require("hbs!templates/footer"),
        FooterView;

    FooterView = BaseLayoutView.extend({
        template: FooterTemplate,
        elementName: 'footer',
        className: 'site-footer',
        onBeforeRender: function() {
            this.$el.attr("role", "contentinfo");
        }
    });

    return FooterView;
});
