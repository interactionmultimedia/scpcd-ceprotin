define("views/behaviors/customScrollbarBehavior", function(require) {
    "use strcit";

    var Marionette          = require("marionette"),
        Mousewheel          = require("jquery-mousewheel"),
        mCustomScrollbar    = require("mCustomScrollbar"),
        CustomScrollbarBehavior;

    CustomScrollbarBehavior = Marionette.Behavior.extend({
        initialize: function () {
            this.view.on("CustomScrollbarBehavior:init", this.init.bind(this));
            this.view.on("CustomScrollbarBehavior:destroy", this.destroy.bind(this));
        },
        init: function(params) {
            var $el = params.$el,
                customHeight = params.height || null,
                minHeight = params.minHeight || null;

            if( !!minHeight ) {
                $el.css("minHeight", minHeight);
            }

            require(["css!/node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css"], function() {
                if( $el.height() > 469 || customHeight != null) {
                    if(customHeight){
                        $el.height(customHeight);
                    } else {
                        $el.height(469);
                    }
                }

                $el.parents(".overlay").addClass("overlay-with-scroll");

                if($($el).find('img').not('.comments-bubble img, img.mCS_img_loaded').length) {
                    $($el).ready(function() {
                        $el.mCustomScrollbar();
                    });
                } else {
                    $el.mCustomScrollbar();
                }
                
            });
        },
        destroy: function(params) {
            if(params.$el && params.$el.hasClass('mCustomScrollbar')) {
                params.$el.mCustomScrollbar('destroy');
            }
        }
    });

    return CustomScrollbarBehavior;
});
