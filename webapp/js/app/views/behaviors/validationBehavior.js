define("views/behaviors/validationBehavior", function(require) {
    "use strict";

    var Marionette                  = require("marionette"),
        Radio                       = require("backbone.radio"),
        $                           = require("jquery"),
        Validate                    = require("jquery.validate"),
        ValidationBehavior;

    ValidationBehavior = Marionette.Behavior.extend({
        initialize: function () {
            this.view.on("ValidationBehavior:initFormValidation", this.initFormValidation, this);
        },
        initFormValidation: function (params) {
            this.setDefaults();
            this.validateForm(params.formId, params);
        },
        validateForm: function (formId, options) {
            $(formId).validate(options);
        },
        setDefaults : function () {
            $.validator.setDefaults({
                highlight: function (element) {
                    return $(element)
                                .closest('.row')
                                .addClass('has-error');
                },
                unhighlight: function (element) {
                    return $(element)
                                .closest('.row')
                                .removeClass('has-error')
                                .find('.help-block-hidden')
                                .removeClass('help-block-hidden')
                                .addClass('help-block');
                },
                errorPlacement: function (error, element) {}
            });
        }

    });

    return ValidationBehavior;
});
