define("views/empty", function(require) {
    "use strict";

    var Marionette      = require("marionette"),
        _               = require("underscore"),
        EmptyView;

    EmptyView = Marionette.LayoutView.extend({
        tagName: "div",
        render: function(){
            return true;
        }
    });

    return EmptyView;
});