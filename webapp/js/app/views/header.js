define("views/header", function(require) {
    "use strict";

    var BaseLayoutView          = require("views/base"),
        Radio                   = require("backbone.radio"),
        $                       = require("jquery"),
        _                       = require("underscore"),
        LanguageSelectView      = require("views/language-select"),
        HeaderTemplate          = require("hbs!templates/header"),
        HeaderView;

    HeaderView = BaseLayoutView.extend({
        className: "site-header clearfix",
        elementName: "header",
        template: HeaderTemplate,
        regions: {
            'languageSelect': '#language-select'
        },
        ui:{
            'btnHome': 'h1 a',
            'infosArea': '#infos-area'
        },
        events:{
            'click @ui.btnHome': 'goToHome'
        },
        initialize: function() {
            this.channel    = Radio.channel("App");

            BaseLayoutView.prototype.initialize.apply(this, arguments);
        },
        onBeforeRender: function() {
            this.$el.attr("role", "banner");
        },
        onDestroy: function() {
            this.channel.off(null, null, this);
        },
        onShow: function() {
            if( App.Languages.getActiveLanguages().length <= 1 ) {
                return;
            }
            
            this.showChildView('languageSelect', new LanguageSelectView());
        },
        goToHome: function(e) {
            e.preventDefault();

            App.router.navigate('#', {trigger: true});
        }
    });

    return HeaderView;
});
