define("templates/helpers/ifChecked", function (require) {
    "use strict";

    var Handlebars = require("hbs/handlebars");

    var ifChecked = function(answer, moduleName, options) {
        var quizz = App.SimulationState.getQuizByModuleName(moduleName);
        if(!!quizz) {
            var question = quizz.get('questions').findWhere({quizQuestionId: options.data.root.quizQuestionId});
            if(!!question) {
                var simulationAnswer = question.get('answers').findWhere({id: answer.id});
                if(simulationAnswer.get('checked')) {
                    return options.fn();
                }
            }
        }
        
    }
        

    Handlebars.registerHelper('ifChecked', ifChecked);
    return ifChecked;
});
