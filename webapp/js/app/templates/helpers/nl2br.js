define("templates/helpers/nl2br", function (require) {
    "use strict";

    var Handlebars = require("hbs/handlebars");

    var nl2br = function(text) {
        var nl2br = (text + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br>' + '$2');
        return new Handlebars.SafeString(nl2br);
    }
        

    Handlebars.registerHelper('nl2br', nl2br);
    return nl2br;
});
