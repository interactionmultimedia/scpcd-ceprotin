define("templates/helpers/len", function (require) {
    "use strict";

    var Handlebars = require("hbs/handlebars");

    var len = function(json) {
        return Object.keys(json).length;
    }

    Handlebars.registerHelper('len', len);
    return len;
});
