define("templates/helpers/inc", function (require) {
    "use strict";

    var Handlebars = require("hbs/handlebars");

    var inc = function (v1, v2) {
        return v1 + v2;
    };

    Handlebars.registerHelper('inc', inc);
    return inc;
});
