define("templates/helpers/ifGoodAnswer", function (require) {
    "use strict";

    var Handlebars = require("hbs/handlebars");

    var ifGoodAnswer = function(item, options) {
        if(item.good == item.checked) {
            return options.fn();
        } else {
            return options.inverse();
        }
    }
        

    Handlebars.registerHelper('ifGoodAnswer', ifGoodAnswer);
    return ifGoodAnswer;
});
