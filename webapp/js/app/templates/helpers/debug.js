define("templates/helpers/debug", function (require) {
    "use strict";

    var Handlebars = require("hbs/handlebars");

    var debug = function (obj) {
        console.log(obj);
    };

    Handlebars.registerHelper('debug', debug);
    return debug;
});
