define("templates/helpers/ifEqual", function (require) {
    "use strict";

    var Handlebars = require("hbs/handlebars");

    var ifEqual = function (v1, v2, options) {
        if (v1 === v2) {
            return options.fn(this);
        }
        return options.inverse(this);
    };

    Handlebars.registerHelper('ifEqual', ifEqual);
    return ifEqual;
});
