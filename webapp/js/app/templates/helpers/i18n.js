define("templates/helpers/i18n", function(require) {
    "use strict";

    var Handlebars  = require("hbs/handlebars"),
        i18n        = require("i18n");

    function i18nHelper() {
        var keys = [],
            isEmpty = true,
            str;

        for(var i in arguments) {
            if (typeof arguments[i] == "string") {
                if( arguments[i] !== "" ) {
                    isEmpty    = false;
                    keys.push(arguments[i]);
                }
            }
        }

        if( isEmpty ) {
            return "";
        }

        str = keys.join('.');

        return (i18n != undefined ? i18n.t(str) : str);
    }

    Handlebars.registerHelper("i18n", i18nHelper);

    return i18n;
});
