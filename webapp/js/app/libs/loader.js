define("libs/loader", function (require) {
    "use strict";

    var Marionette  = require("marionette"),
        $           = require("jquery"),
        Loader;

    var Loader = Marionette.Object.extend({
        asset: function (url) {
            var defer = $.Deferred();
            var image = new Image();

            image.onload = loaded;
            image.onerror = errored;
            image.onabort = errored;

            image.src = url;

            function loaded() {
                unbindEvents();
                defer.notify();
                defer.resolve(image);
            }

            function errored() {
                unbindEvents();
                defer.reject(image);
            }

            function unbindEvents() {
                image.onload = null;
                image.onerror = null;
                image.onabort = null;
            }
            return defer.promise();
        },

        audio: function (url) {
            var defer = $.Deferred();

            var audio = new Howl({
                src: [url],
                preload: true,
                onload: loaded,
                onloaderror: errored
            });

            function loaded() {
                defer.notify();
                defer.resolve(audio);
            }

            function errored() {
                defer.reject(audio);
            }
            return defer.promise();
        },
    });

    return Loader;
});
