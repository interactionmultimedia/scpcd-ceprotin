define("libs/featureImage", function (require) {
    "use strict";

    var _ = require('underscore');

    //elem:     element that has the bg image
    //model:    backbone model
    //bgWidth:  intrinsic width of background image
    //bgHeight: intrinsic height of background image
    function FeatureImage(elem, model, bgWidth, bgHeight) {
        this.ratio = bgWidth / bgHeight; //aspect ratio of bg image
        this.element = elem;
        this.model = model;
        this.bgWidth = bgWidth;
        this.bgHeight = bgHeight;
        if (_.isEmpty(model.get('originalLeft'))) {
            model.set('originalLeft', model.get('left') / bgWidth); //percent from the left edge of bg image the feature resides
        }
        if (_.isEmpty(model.get('originalTop'))) {
            model.set('originalTop', model.get('top') / bgHeight); //percent from bottom edge of bg image that feature resides
        }
        if (_.isEmpty(model.get('originalWidth'))) {
            model.set('originalWidth', model.get('width')); //percent from bottom edge of bg image that feature resides
        }
        if (_.isEmpty(model.get('originalHeight'))) {
            model.set('originalHeight', model.get('height')); //percent from bottom edge of bg image that feature resides
        }
        window.addEventListener("resize", this.setFeaturePositions.bind(this));
        this.setFeaturePositions(); //initialize the <p> positions
    }

    FeatureImage.prototype.setFeaturePositions = function () {
        var eratio = this.element.clientWidth / this.element.clientHeight; //calc the current container aspect ratio
        this.scaledWidth = this.element.clientHeight * this.ratio; // pre calc the scaled width of bg image
        this.scaledDX = (this.scaledWidth - this.element.clientWidth) / 2; // pre calc the amount of the image that is outside the
        this.scaledHeight = this.element.clientWidth / this.ratio; // pre calc the scaled height of bg image
        this.scaledDY = (this.scaledHeight - this.element.clientHeight) / 2; // pre calc the amount of the image that is outside the bottom of the container
        if (eratio > this.ratio) { // width of scaled bg image is equal to width of container
            this.setWide(); // set the position of each feature marker
        } else { // height of scaled bg image is equal to height of container left of the container
            this.setTall(); // set the position of each feature marker
        }
        if (this.element.clientWidth > this.bgWidth) { // width is greater than original background
            if (this.scaledDX > 0) { // height is cropped
                var scale = (this.element.clientWidth + this.scaledDX * 2) / this.bgWidth;
                this.model.set('width', this.model.get('originalWidth') * scale);
                this.model.set('height', this.model.get('originalHeight') * scale);
            } else {
                var scale = this.element.clientWidth / this.bgWidth;
                this.model.set('width', this.model.get('originalWidth') * scale);
                this.model.set('height', this.model.get('originalHeight') * scale);
            }
        } else if (this.element.clientHeight > this.bgHeight) { // height is greater than original background
            var scale = this.element.clientHeight / this.bgHeight;
            this.model.set('width', this.model.get('originalWidth') * scale);
            this.model.set('height', this.model.get('originalHeight') * scale);
        } else if (this.element.clientHeight < this.bgHeight) { // height is lower than original background
            if (this.scaledDY > 0) { // height is cropped
                var scale = (this.element.clientHeight + this.scaledDY * 2) / this.bgHeight;
                this.model.set('width', this.model.get('originalWidth') * scale);
                this.model.set('height', this.model.get('originalHeight') * scale);
            } else {
                var scale = this.element.clientHeight / this.bgHeight;
                this.model.set('width', this.model.get('originalWidth') * scale);
                this.model.set('height', this.model.get('originalHeight') * scale);
            }
        } else {
            this.model.set('width', this.model.get('originalWidth'));
            this.model.set('height', this.model.get('originalHeight'));
        }
    }

    FeatureImage.prototype.setWide = function () {
        this.model.set('left', this.model.get('originalLeft') * this.element.clientWidth);
        this.model.set('top', this.scaledHeight * this.model.get('originalTop') - this.scaledDY); // calc the pixels above the bottom edge of the image - the amount below the container
    }

    FeatureImage.prototype.setTall = function () {
        this.model.set('top', this.model.get('originalTop') * this.element.clientHeight);
        this.model.set('left', this.scaledWidth * this.model.get('originalLeft') - this.scaledDX); // calc the pixels to the right of the left edge of image - the amount left of the container
    }

    return FeatureImage;
});
