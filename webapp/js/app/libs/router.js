define("libs/router", function(require) {
    "use strict";

    var $                       = require("jquery"),
        BaseRouter              = require("libs/baseRouter"),
        MainController          = require("controllers/main"),
        ConsultationController  = require("controllers/consultation"),
        WaitingRoomController  = require("controllers/waitingRoom"),
        controllers = {
            Main: MainController,
            Consultation: ConsultationController,
            WaitingRoom: WaitingRoomController
        };

    $.each(controllers, function(key, Controller){
        var tmpController = new Controller();
        BaseRouter.prototype.processAppRoutes(tmpController, tmpController.routes);
    });

    return BaseRouter;
});
