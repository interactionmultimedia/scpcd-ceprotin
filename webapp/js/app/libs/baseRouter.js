define("libs/baseRouter", function(require) {
    "use strict";

    var Backbone    = require("backbone"),
        Marionette  = require("marionette"),
        _           = require("underscore"),
        Cookies     = require("cookies"),
        BaseRouter;

    BaseRouter = Marionette.AppRouter.extend({
        currentRoute: {},
        check: function check() {
            this.next();
        },
        goto: function goto(route) {
            Backbone.history.navigate(route, {
                trigger: true
            });
        },
        before: function before() {
            if (Backbone.history.fragment !== "" && !App.Config.isDev() && !App.router.flagged && !this.isPageForbidden()) {
                this.goto("");
                this.next();
                return;
            }
            this.check();
        },
        after: function after() {
            this.currentRoute = {};
        },
        navigate: function(fragment, options) {
            App.router.flagged = true;
            Backbone.history.navigate(fragment, options);
            return this;
        },
        next: function next() {
            this.currentRoute.callback && this.currentRoute.callback.apply(this, this.currentRoute.args);
            this.trigger.apply(this, ['route:' + this.currentRoute.name].concat(this.currentRoute.args));
            this.trigger('route', this.currentRoute.name, this.currentRoute.args);
            Backbone.history.trigger('route', this, this.currentRoute.name, this.currentRoute.args);
            this.after.apply(this, this.currentRoute.args);
        },
        route: function route(current_route, name, callback) {
            if (!_.isRegExp(current_route)) current_route = this._routeToRegExp(current_route);
            if (_.isFunction(name)) {
                callback = name;
                name = '';
            }
            if (!callback) callback = this[name];

            var that = this;

            Backbone.history.route(current_route, function (fragment) {
                that.currentRoute.name = name;
                that.currentRoute.callback = callback;
                that.currentRoute.args = that._extractParameters(current_route, fragment);

                that.before.apply(that);
            });

            return this;
        },
        testAuthentication: function testAuthentication() {
            return true;
        },
        testAuthenticationCookie: function testAuthenticationCookie(cookieName) {
            var regFindParam = /[\?&]{1}from=(shire|email)([&]|$)/;
            if( !Cookies.get(cookieName) ) {
                if( document.location.search.match(regFindParam) ) {
                    Cookies.set(cookieName, true, {expires: 15});
                    window.location.href = "/";
                    return true;
                } else {
                    App.router.goto("forbidden");
                    return false;
                }
            }

            return true;
        },
        isPageForbidden: function isError() {
            return ( Backbone.history.fragment.indexOf("forbidden") !== -1 );
        }
    });

    return BaseRouter;
});
