define("models/asset", function(require) {
    "use strict";

    var Backbone                = require("backbone"),
        AssetModel;

    AssetModel   = Backbone.Model.extend({
        defaults: {
            url: null,
            type: null
        },

        initialize: function(options) {
            if(options && options.url){
                if(this.hasContentImage(options.url)){
                    this.set("type", "image");
                } else {
                    this.set("type", "other");
                }
            }
        },

        hasContentImage: function (value) {
            var re = new RegExp(/\.(png|gif|jpeg|jpg)$/i);
            return re.test(value);
        },
    });

    return AssetModel;
});
