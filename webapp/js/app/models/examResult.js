define("models/examResult", function(require) {
    "use strict";

    var Backbone            = require("backbone"),
        AssetsCollection    = require("collections/assets"),
        ExamResultModel;

    ExamResultModel    = Backbone.Model.extend({
        defaults: {
            examType: null,
            examDate: null,
            examTitle: null,
            examResults: null,
            examCost: null,
            assetVOList: null,
            onlyAsset: false
        },

        initialize: function(data) {
            if( data.assetVOList ) {
                this.set("assetVOList", new AssetsCollection(data.assetVOList.AssetVO));
                if(this.get("assetVOList").length == 1 && this.get('examResults') == null) {
                    this.set('onlyAsset', 1);
                }
            }
        },

        toJSON : function() {
            return JSON.parse(JSON.stringify(this.attributes));
        }
    });

    return ExamResultModel;
});
