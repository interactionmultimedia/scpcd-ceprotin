define("models/avatarPosition", function (require) {
    "use strict";

    var Backbone = require("backbone"),
        AvatarPosition;

    AvatarPosition = Backbone.Model.extend({
        defaults: {
            url: null,
            height: 0,
            width: 0,
            left: 0,
            top: 0,
            originalLeft: 0,
            originalTop: 0
        }
    });

    return AvatarPosition;
});
