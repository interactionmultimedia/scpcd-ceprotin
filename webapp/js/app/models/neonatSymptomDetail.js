define("models/neonatSymptomDetail", function(require) {
    "use strict";

    var Backbone                = require("backbone"),
        NeonatSymptomDetailModel;

        NeonatSymptomDetailModel    = Backbone.Model.extend({
        defaults: {
            string: "",
        },

        toJSON : function() {
            return JSON.parse(JSON.stringify(this.attributes));
        }
    });

    return NeonatSymptomDetailModel;
});
