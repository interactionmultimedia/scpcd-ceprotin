define("models/familyBackground", function(require) {
    "use strict";

    var Backbone                        = require("backbone"),
        FamilyBackgroundModel;

    FamilyBackgroundModel    = Backbone.Model.extend({
        defaults: {
            observationsValue: null,
        },

        toJSON : function() {
            return JSON.parse(JSON.stringify(this.attributes));
        }
    });

    return FamilyBackgroundModel;
});
