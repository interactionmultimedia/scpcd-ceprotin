define("models/avatar", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        AvatarPosition  = require("models/avatarPosition"),
        AvatarModel;

    AvatarModel   = Backbone.Model.extend({
        defaults: {
            avatar: null,
            name: null,
            job: null,
            speaking: new AvatarPosition(),
            idle: new AvatarPosition()
        },
        initialize: function(values) {
            this.set('speaking', new AvatarPosition(values.speaking));
            this.set('idle', new AvatarPosition(values.idle));
        }
    });

    return AvatarModel;
});
