define("models/clinicalCase", function(require) {
    "use strict";

    var Backbone            = require("backbone"),
        $                   = require("jquery"),
        ClinicalCaseModel;

    ClinicalCaseModel   = Backbone.Model.extend({
        defaults: {
            id: null,
            caseId: null,
            caseName: null,
            caseNameWaitingRoom: null,
            patientFirstName: null,
            patientLastName: null,
            patientAge: null,
            language: null,
            published: false,
            nbExaminations: null,
            waitingRoom: null
        },

        initialize: function(options) {
            if(options.caseId){
                this.set("id", ~~options.caseId);
            }
            if(options.published){
                this.set("published", ( ~~options.published === 1 ));
            }
        },

        getPatientFullName: function() {
            return $.trim(this.get("patientFirstName")+" "+this.get("patientLastName"));
        },

        isLast: function(consultationId) {
            return ~~consultationId === this.getLastConsultationIndex();
        },

        getLastConsultationIndex: function() {
            var nbExaminations = 0,
                consultation,
                end = ~~this.get("nbExaminations"),
                i;
                
            for (i = 0; i < end; i++) {
                consultation    = this.getConsultation(i);
                if( ~~consultation.published === 1 || !App.Config.isProduction() ) {
                    nbExaminations++;
                }
            }
            
            return ( nbExaminations - 1 );
        },

        getConsultation: function(index) {
            return this.get("consultations")[index];
        }
    });

    return ClinicalCaseModel;
});
