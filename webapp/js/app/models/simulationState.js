define("models/simulationState", function (require) {
    "use strict";

    var Backbone                = require('backbone'),
        BackboneLocalStorage    = require("backbone.localStorage"),
        Radio                   = require("backbone.radio"),
        _                       = require("underscore"),
        Cookies                 = require("cookies"),
        QuizzesCollection       = require("collections/quizzes"),
        SimultationStateModel;

    SimultationStateModel = Backbone.Model.extend({
        defaults: {
            id: 1,
            email: null,
            quizzes: new QuizzesCollection(),
            guideElements: [],
            seenConsultations: [],
            seenAllMedicalFiles: false
        },

        initialize: function() {
            this.channel    = Radio.channel("App");
            this.channel.on("consultation:ended", this.setCookieGuideShown.bind(this), this);
            this.resetGuides();
        },

        localStorage: new BackboneLocalStorage("simulationState", {
            serialize: function (item) {
                var toSave = _.isObject(item) ? JSON.stringify(item) : item;
                return toSave;
            },

            deserialize: function (data) {
                var attributes = JSON.parse(data),
                    record = {};

                _.each(attributes, function (value, field) {
                    if (value === null) {
                        return;
                    }

                    switch (field) {
                        case "quizzes":
                            record.quizzes  = new QuizzesCollection(value);
                            break;

                        default:
                            record[field] = value;
                            break;
                    }
                });

                return record;
            }
        }),

        saveQuiz: function(moduleName, quiz) {
            var model = this.get("quizzes").find({
                moduleName: moduleName
            });

            if( !!model ) {
                this.get("quizzes").remove(model);
            }

            this.get("quizzes").add(quiz);

            this.save();
        },

        getQuizByModuleName: function(moduleName) {
            return this.get("quizzes").findWhere({
                moduleName: moduleName
            });
        },

        addGuideElement: function(name) {
            var guideElements;

            if( this.hasGuideElement(name) ) {
                return;
            }

            guideElements   = this.get("guideElements");
            guideElements.push(name);

            this.set("guideElements", guideElements);
            this.save();
        },

        removeGuideElement: function(name) {
            var guideElements;

            if( !this.hasGuideElement(name) ) {
                return;
            }

            guideElements   = this.get("guideElements");
            var index = guideElements.indexOf(name);
            guideElements.splice(index, 1);
            this.set("guideElements", guideElements);
            this.save();
        },

        hasGuideElement: function(name) {
            return ( _.indexOf(this.get("guideElements"), name) !== -1 );
        },

        hasSeenGuideElement: function(name) {
            return ( Cookies.get('guideCompleted') || this.hasGuideElement(name) );
        },

        resetGuides: function() {
            this.set("guideElements", []);
            Cookies.remove('guideCompleted');
            this.set('seenAllMedicalFiles', false);
            this.save();
        },

        setCookieGuideShown: function() {
            Cookies.set('guideCompleted', true, {expires: 365});
        },

        hasSeenConsultation: function(index) {
            return ( _.indexOf(this.get("seenConsultations"), index) !== -1 );
        },

        seenConsultation: function(index) {
            if( this.hasSeenConsultation(index) ) {
                return;
            }

            var seenConsultations   = this.get("seenConsultations");
            seenConsultations.push(index);
            this.set("seenConsultations", seenConsultations);
        },

        seenAllMedicalFiles: function() {
            this.set('seenAllMedicalFiles', true);
            this.save();
        },

        hasSeenAllMedicalFiles: function() {
            return this.get('seenAllMedicalFiles');
        }
    });

    return SimultationStateModel;
});
