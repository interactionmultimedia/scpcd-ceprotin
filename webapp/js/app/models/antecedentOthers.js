define("models/antecedentOthers", function(require) {
    "use strict";

    var Backbone    = require("backbone"),
        AntecedentModel;

    AntecedentModel   = Backbone.Model.extend({
        defaults: {
            discoveredID: null,
            date: null,
            observationsValue: null
        }
    });

    return AntecedentModel;
});
