define("models/profile", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        PatientProfileModel;

    PatientProfileModel   = Backbone.Model.extend({
        defaults: {
            patientFirstName: "",
            patientLastName: "",
            patientAge: "",
            patientSex: "",
            maritalStatus: "",
            profession: "",
            children: "",
            weight: "",
            height: "",
            abdominalPerimeter: "",
            imc: "",
            patientProfilePicture: ""
        }
    });

    return PatientProfileModel;
});