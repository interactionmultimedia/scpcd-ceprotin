define("models/trainer", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        $               = require("jquery"),
        MeetingModel    = require("models/meeting"),
        TrainerModel;

    TrainerModel    = Backbone.Model.extend({
        defaults: {
            id: null,
            login: null,
            meeting: null
        },
        initialize: function() {
            this.urlRoot    = App.Config.get("gateway") + "/trainer";
        },
        login: function(data) {
            var that        = this,
                deferred    = $.Deferred();

            $.ajax({
                url: this.urlRoot + '/login',
                data: data,
                method: "POST"
            }).done(function (result) {
                if( !!result.id ) {
                    that.set({
                        id: result.id,
                        login: data.login
                    });
                    deferred.resolve();
                } else {
                    deferred.reject();
                }
            }).fail(function () {
                deferred.reject();
            });

            return deferred.promise();
        },
        parse: function (response) {
            if( response.meeting ) {
                response.meeting = new MeetingModel(response.meeting);
            }

            return response;
        },
        getRP: function() {
            return this.get("meeting").getRP();
        }
    });

    return TrainerModel;
});
