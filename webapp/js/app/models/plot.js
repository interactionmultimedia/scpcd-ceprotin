define("models/plot", function(require) {
    "use strict";

    var Backbone                    = require("backbone"),
        PlotDetailsCollection       = require("collections/plotDetails"),
        PlotModel;

    PlotModel   = Backbone.Model.extend({
        defaults: {
            spotId: null,
            spotX: null,
            spotY: null,
            originalSpotX: null,
            originalSpotY: null,
            details:  new PlotDetailsCollection()
        },
        initialize: function(){
            this.set('originalSpotX', this.get('spotX'));
            this.set('originalSpotY', this.get('spotY'));
        }
    });

    return PlotModel;
});
