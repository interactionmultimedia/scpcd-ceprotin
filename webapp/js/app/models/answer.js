define("models/answer", function (require) {
    "use strict";

    var Backbone = require("backbone"),
        AnswerModel;

    AnswerModel = Backbone.Model.extend({
        defaults: {
            id: null,
            quizAnswerId: null,
            answerLabel: null,
            answerValue: null,
            checked: false
        },

        initialize: function (options) {
            this.set("good", this.isGoodAnswer());
            if (!options.id) {
                this.set("id", ~~options.quizAnswerId);
            }
            this.unset("quizAnswerId");
        },

        isCorrect: function () {
            var answerIsCorrect = this.isGoodAnswer();
            return this.get("checked") == answerIsCorrect;
        },

        isGoodAnswer: function () {
            return (~~this.get("answerValue") === 1 || ~~this.get("answerValue") === 2);
        },

        toJSON: function () {
            return JSON.parse(JSON.stringify(this.attributes));
        }
    });

    return AnswerModel;
});
