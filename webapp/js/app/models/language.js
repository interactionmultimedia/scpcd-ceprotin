define("models/language", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        LanguageModel;

    LanguageModel   = Backbone.Model.extend({
        defaults: {
            code: null,
            key: null,
            label: null,
            className: null,
            classNameCurrent: null
        }
    });

    return LanguageModel;
});
