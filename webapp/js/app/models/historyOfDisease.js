define("models/historyOfDisease", function(require) {
    "use strict";

    var Backbone                = require("backbone"),
        HistoryOfDiseaseModel;

    HistoryOfDiseaseModel    = Backbone.Model.extend({
        defaults: {
            evenement: null,
            date: null,
            traitement: null,
            observationsValue: null
        },
        toJSON : function() {
            return JSON.parse(JSON.stringify(this.attributes));
        }
    });

    return HistoryOfDiseaseModel;
});
