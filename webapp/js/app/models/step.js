define("models/step", function (require) {
    "use strict";

    var Backbone = require("backbone"),
        PlotsCollection = require("collections/plots"),
        PlotDetailsCollection = require("collections/plotDetails"),
        i18n = require("i18n"),
        StepModel;

    StepModel = Backbone.Model.extend({
        defaults: {
            slugTitle: null,
            slugTitleQuit: null,
            moduleName: null,
            className: null,
            dataModule: null,
            available: false
        },

        initialize: function () {
            var slugQuit = this.get('slugTitle').replace('.title', '.quit'),
                quitLabel = i18n.exists(slugQuit);
            if (quitLabel) {
                this.set('slugTitleQuit', i18n.t(slugQuit));
            }
        },

        /**
         * Récupération des informations de l'examen clinique
         * @return {Object} Examen clinique
         */
        getInformationsClinicalExam: function () {
            return this.get("dataModule").ModuleVO.content.ContentVO.ClinicalExamBasicExamsVO;
        },

        /**
         * Retour les 'plots' de l'examen clinique
         * @return {Collection} Collection de plots
         */
        getPlots: function () {
            var dataPlots = this.get("dataModule").ModuleVO.content.ContentVO.spotList.ClinicalExamSpotVO,
                dataPlotDetails = this.get("dataModule").ModuleVO.content.ContentVO.examsList.ClinicalExamVO,
                plots = new PlotsCollection(dataPlots);

            if (!Array.isArray(dataPlotDetails)) {
                dataPlotDetails = [dataPlotDetails];
            }

            plots.each(function (element) {
                var details = _.where(dataPlotDetails, {
                    spotId: element.get("spotId")
                });
                element.set("details", new PlotDetailsCollection(details));
            });

            return plots;
        },

        getSpeeches: function () {
            return this.get("dataModule").ModuleVO.content.ContentVO.contentList.ChatItemVO;
        },

        getSpeechesAvatars: function () {
            return this.get("dataModule").ModuleVO.content.ContentVO.avatars;
        }
    });

    return StepModel;
});
