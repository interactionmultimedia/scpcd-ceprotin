define("models/neonatSymptom", function(require) {
    "use strict";

    var Backbone                        = require("backbone"),
        NeonatSymptomsDetailsCollection    = require("collections/neonatSymptomDetails"),
        NeonatSymptomModel;

    NeonatSymptomModel    = Backbone.Model.extend({
        defaults: {
            eventTitle: null,
            symptomsList: new NeonatSymptomsDetailsCollection()
        },

        initialize: function(data) {
            if( data.symptomsList ) {
                this.set("symptomsList", new NeonatSymptomsDetailsCollection(data.symptomsList));
            }
        },

        toJSON : function() {
            return JSON.parse(JSON.stringify(this.attributes));
        }
    });

    return NeonatSymptomModel;
});
