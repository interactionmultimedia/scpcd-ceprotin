define("models/participant", function (require) {
    "use strict";

    var Backbone = require("backbone"),
        $ = require("jquery"),
        MeetingModel = require("models/meeting"),
        ParticipantModel;

    ParticipantModel = Backbone.Model.extend({
        idAttribute: "id",
        defaults: {
            id: null,
            id_meeting: null,
            veeva_id: null,
            code: null,
            lastname: null,
            firstname: null,
            country: null,
            meeting: null,
            workplace: {
                id:null,
                name:null,
                address1:null,
                address2:null,
                zipcode:null,
                city:null,
                country:null,
                state:null
            }
        },

        initialize: function () {
            this.urlRoot = App.Config.get("gateway") + "/participant";
        },

        save: function (attrs, opts) {
            var options = {
                url: this.urlRoot,
                type: 'POST'
            };

            _.extend(options, opts);

            Backbone.Model.prototype.save.call(this, attrs, options);
        },

        delete: function (attrs, opts) {
            var options = {
                url: this.urlRoot,
                type: 'DELETE'
            };

            _.extend(options, opts);

            Backbone.Model.prototype.save.call(this, attrs, options);
        },

        login: function (data) {
            var that = this,
                deferred = $.Deferred();

            $.ajax({
                url: this.urlRoot + '/login',
                data: data,
                method: "POST"
            }).done(function (result) {
                if (!!result.id) {
                    that.set("id", result.id);
                    deferred.resolve();
                } else {
                    deferred.reject();
                }
            }).fail(function () {
                deferred.reject();
            });

            return deferred.promise();
        },

        parse: function (response) {
            if (response.meeting) {
                response.meeting = new MeetingModel(response.meeting);
            }

            return response;
        },

        getRP: function () {
            return this.get("meeting").getRP();
        },

        getFullname: function () {
            return [
                this.get("firstname"),
                this.get("lastname")
            ].join(" ");
        },

        toJSON: function () {
            return JSON.parse(JSON.stringify(this.attributes));
        }
    });

    return ParticipantModel;
});
