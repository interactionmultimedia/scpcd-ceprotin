define("models/meeting", function (require) {
    "use strict";

    var Backbone = require("backbone"),
        MeetingModel;

    MeetingModel = Backbone.Model.extend({
        defaults: {
            id: null,
            name: null,
            venue: null,
            code: null,
            type: null,
            product: null,
            language: null
        },

        initialize: function () {
            this.urlRoot = App.Config.get("gateway") + "/meeting";
        },

        getRP: function () {
            return this.get("code");
        },
        
        finishMeeting: function() {
            
            var that = this,
                deferred = $.Deferred();

            $.post(
                this.urlRoot+'/end/'+App.Meeting.get('id'),
                {
                    cases:App.launchedCases
                }
            )
            .done(function (result) {
                deferred.resolve(result);
            }).fail(function () {
                deferred.reject();
            });

            return deferred.promise();
            
        },

        save: function (attrs, opts) {
            var options = {
                url: this.urlRoot,
                type: 'POST'
            };

            _.extend(options, opts);

            return Backbone.Model.prototype.save.call(this, attrs, options);
        },

        getLastCodeParticipant: function(id) {
            var deferred = $.Deferred();

            $.get(
                App.Config.get("gateway") + "/meeting/getLastCodeParticipant", {
                    'id': id
                }
            ).success(function (result) {
                deferred.resolve(result.code);
            }).fail(function (err) {
                deferred.reject();
            });

            return deferred.promise();
        },

        getParticipantsStats: function() {

            var that = this,
                deferred = $.Deferred();

            $.get(
                App.Config.get("gateway") + "/meeting/"+App.Meeting.get('id')+"/getStats"
            ).success(function (meeting) {
                deferred.resolve(meeting);
            }).fail(function (err) {
                deferred.reject();
            });

            return deferred.promise();
            
        }
    });

    return MeetingModel;
});
