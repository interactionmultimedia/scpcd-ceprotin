define("models/config", function(require) {
    "use strict";

    var Backbone    = require("backbone"),
        $           = require("jquery"),
        ConfigModel;

    ConfigModel   = Backbone.Model.extend({
        defaults: {
            config: null,
            mode: null,
            gateway: null,
            local: null,
            rp: null,
            webconf: null
        },

        fetch: function (options) {
            var that    = this,
                promise = $.Deferred();

            options     = options || {};
            options.url = "/js/app/config/config.json";

            $.ajax(options)
                .complete(function(response){
                    if( !!response.responseJSON ) {
                        that.set(response.responseJSON);
                        that.set("mode", "local");
                        promise.resolve();
                    } else {
                        promise.reject();
                    }
                });

            return promise;
        },

        getGAID: function() {
            var mode = this.get("mode");
            if( !!this.get(mode) && !!this.get(mode).GA_ID ) {
                return this.get(mode).GA_ID;
            }
            return null;
        },

        isDev: function() {
            return ( this.get("config") === "dev" );
        },

        isProduction: function() {
            return ( this.get("config") === "production" );
        },

        isLocal: function() {
            return ( this.get("mode") === "local" );
        },

        isRP: function() {
            return ( this.get("mode") === "rp" );
        },

        isWebconf: function() {
            return ( this.get("mode") === "webconf" );
        }
    });

    return ConfigModel;
});
