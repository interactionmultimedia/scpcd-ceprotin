define("models/veeva", function (require) {
    "use strict";

    var Backbone = require("backbone"),
        $ = require("jquery"),
        VeevaModel;

    VeevaModel = Backbone.Model.extend({
        findParticipant: function (data) {

            var that = this,
                deferred = $.Deferred();

            $.ajax({
                url: App.Config.get("gateway") + "/veeva/findParticipants",
                method: "POST",
                data: data
            }).success(function (result) {
                deferred.resolve(result);
            }).fail(function () {
                deferred.reject();
            });
            return deferred.promise();

        }
    });

    return VeevaModel;
});
