define("models/antecedentDisease", function(require) {
    "use strict";

    var Backbone    = require("backbone"),
        i18n        = require("i18n"),
        AntecedentModel;

    AntecedentModel   = Backbone.Model.extend({
        defaults: {
            discoveredID: null,
            disease: null,
            timeData: null,
            treatment: null,
            observationsValue: null
        },
        set: function(value) {
            if( value.observationsValue === "SECTION_MEDICAL_HISTORY_MEDICAL_HISTORY_NODISEASE" ) {
                value.observationsValue = '<ul><li>'+i18n.t("consultation.medicalFiles.medicalHistory.defaults."+value.observationsValue)+'</li></ul>';
            }

            Backbone.Model.prototype.set.apply(this, arguments);
        }
    });

    return AntecedentModel;
});
