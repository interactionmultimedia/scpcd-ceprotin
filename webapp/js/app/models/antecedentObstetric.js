define("models/antecedentObstetric", function(require) {
    "use strict";

    var Backbone    = require("backbone"),
        AntecedentModel;

    AntecedentModel   = Backbone.Model.extend({
        defaults: {
            discoveredID: null,
            date: null,
            obstetric: null,
            observationsValue: null
        }
    });

    return AntecedentModel;
});
