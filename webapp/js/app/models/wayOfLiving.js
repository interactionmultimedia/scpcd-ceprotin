define("models/wayOfLiving", function(require) {
    "use strict";

    var Backbone                        = require("backbone"),
        WayOfLivingDetailsCollection    = require("collections/wayOfLivingDetails"),
        WayOfLivingModel;

    WayOfLivingModel    = Backbone.Model.extend({
        defaults: {
            discoveredID: null,
            title: null,
            subSectionType: null,
            contentList: new WayOfLivingDetailsCollection()
        },

        initialize: function(data) {
            if( data.contentList ) {
                this.set("contentList", new WayOfLivingDetailsCollection(data.contentList));
            }
        },

        toJSON : function() {
            return JSON.parse(JSON.stringify(this.attributes));
        }
    });

    return WayOfLivingModel;
});
