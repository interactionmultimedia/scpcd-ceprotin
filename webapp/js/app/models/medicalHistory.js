define("models/medicalHistory", function(require) {
    "use strict";

    var Backbone                = require("backbone"),
        AntecedentsAllergy      = require("collections/antecedentsAllergy"),
        AntecedentsDisease      = require("collections/antecedentsDisease"),
        AntecedentsFamily       = require("collections/antecedentsFamily"),
        AntecedentsObstetric    = require("collections/antecedentsObstetric"),
        AntecedentsOthers       = require("collections/antecedentsOthers"),
        AntecedentsSurgery      = require("collections/antecedentsSurgery"),
        MedicalHistoryModel;

    MedicalHistoryModel   = Backbone.Model.extend({
        defaults: {
            antecedentsAllergy: new AntecedentsAllergy(),
            antecedentsDisease: new AntecedentsDisease(),
            antecedentsFamily: new AntecedentsFamily(),
            antecedentsObstetric: new AntecedentsObstetric(),
            antecedentsOthers: new AntecedentsOthers(),
            antecedentsSurgery: new AntecedentsSurgery()
        },
        parse: function(data) {
            if( data.ModuleVO.content.ContentVO.MedicalHistoriesAllergyListVO ) {
                this.set("antecedentsAllergy", new AntecedentsAllergy(data.ModuleVO.content.ContentVO.MedicalHistoriesAllergyListVO.MedicalHistoriesAllergyItems.MedicalHistoriesAllergyVO));
            }
            
            if( data.ModuleVO.content.ContentVO.MedicalHistoriesDiseaseListVO ) {
                this.set("antecedentsDisease", new AntecedentsDisease(data.ModuleVO.content.ContentVO.MedicalHistoriesDiseaseListVO.MedicalHistoriesDiseaseItems.MedicalHistoriesDiseaseVO));
            }

            if( data.ModuleVO.content.ContentVO.MedicalHistoriesFamilyListVO ) {
                this.set("antecedentsFamily", new AntecedentsFamily(data.ModuleVO.content.ContentVO.MedicalHistoriesFamilyListVO.MedicalHistoriesFamilyItems.MedicalHistoriesFamilyVO));
            }

            if( data.ModuleVO.content.ContentVO.MedicalHistoriesObstetricListVO ) {
                this.set("antecedentsObstetric", new AntecedentsObstetric(data.ModuleVO.content.ContentVO.MedicalHistoriesObstetricListVO.MedicalHistoriesObstetricItems.MedicalHistoriesObstetricVO));
            }

            if( data.ModuleVO.content.ContentVO.MedicalHistoriesOthersListVO ) {
                this.set("antecedentsOthers", new AntecedentsOthers(data.ModuleVO.content.ContentVO.MedicalHistoriesOthersListVO.MedicalHistoriesOthersItems.MedicalHistoriesOthersVO));
            }

            if( data.ModuleVO.content.ContentVO.MedicalHistoriesSurgeryListVO ) {
                this.set("antecedentsSurgery", new AntecedentsSurgery(data.ModuleVO.content.ContentVO.MedicalHistoriesSurgeryListVO.MedicalHistoriesSurgeryItems.MedicalHistoriesSurgeryVO));
            }
        }
    });

    return MedicalHistoryModel;
});
