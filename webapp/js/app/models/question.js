define("models/question", function(require) {
    "use strict";

    var Backbone                = require("backbone"),
        AnswersCollection       = require("collections/answers"),
        QuestionModel;

    QuestionModel   = Backbone.Model.extend({
        defaults: {
            quizQuestionId: null,
            questionLabel: null,
            questionType: null,
            questionComment: null,
            questionIntro: null,
            questionImage: null,
            synthesis: null,
            answers: null
        },

        initialize: function(options){
            if( options.answers ) {
                this.set("answers", new AnswersCollection(options.answers));
            } else {
                this.set("answers", new AnswersCollection(options.answerList.QuizAnswerVO));
            }

            this.unset("answerList");
            this.unset("questionCoefficient");
        },

        isCorrect: function(){
            var answersIncorrect = this.get("answers").filter(function(a){
                return !a.isCorrect();
            });

            return answersIncorrect.length === 0;
        },

        hasAnswer: function(){
            var answers = this.getUserAnswers();

            return answers.length > 0;
        },

        getUserAnswers: function(){
            var answers = this.get("answers").filter(function(a){
                return a.get("checked");
            });

            return new AnswersCollection(answers);
        },

        resetSelection: function() {
            this.get("answers").each(function(a){
                a.set("checked", false);
            });
        },

        isMultiple: function() {
            return this.get("questionType") === "checkbox";
        },

        toJSON: function () {
            return JSON.parse(JSON.stringify(this.attributes));
        }
    });

    return QuestionModel;
});
