define("models/plotDetail", function(require) {
    "use strict";

    var Backbone    = require("backbone"),
        PlotDetailModel;

    PlotDetailModel   = Backbone.Model.extend({
        defaults: {
            examTitle: null
        }
    });

    return PlotDetailModel;
});
