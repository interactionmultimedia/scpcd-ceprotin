define("models/medicalFile", function(require) {
    "use strict";

    var Backbone    = require("backbone"),
        MedicalFileModel;

    MedicalFileModel   = Backbone.Model.extend({
        defaults: {
            slugTitle: null,
            moduleName: null,
            className: null,
            dataModule: null,
            available: false
        }
    });

    return MedicalFileModel;
});
