define("models/wayOfLivingDetail", function(require) {
    "use strict";

    var Backbone                = require("backbone"),
        WayOfLivingDetailModel;

    WayOfLivingDetailModel    = Backbone.Model.extend({
        defaults: {
            string: "",
        },

        toJSON : function() {
            return JSON.parse(JSON.stringify(this.attributes));
        }
    });

    return WayOfLivingDetailModel;
});
