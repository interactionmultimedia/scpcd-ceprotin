define("models/specialistReport", function(require) {
    "use strict";

    var Backbone                = require("backbone"),
        SpecialistReportModel;

    SpecialistReportModel    = Backbone.Model.extend({
        defaults: {
            date: null,
            title: null,
            specialistDiscipline: null,
            observationsValue: null
        },
        toJSON : function() {
            return JSON.parse(JSON.stringify(this.attributes));
        }
    });

    return SpecialistReportModel;
});
