define("models/ongoingTreatment", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        OngoingTreatmentModel;

    OngoingTreatmentModel   = Backbone.Model.extend({
        defaults: {
            discoveredID: "",
            therapeuticClassValue: "",
            frequencyValue: "",
            durationValue: "",
            observationsValue: ""
        }
    });

    return OngoingTreatmentModel;
});
