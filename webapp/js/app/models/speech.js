define("models/speech", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        SpeechModel;

    SpeechModel   = Backbone.Model.extend({
        defaults: {
            environmentId: "1",
            avatarId: "1",
            text: "",
            file: "",
            type: "sound",
            camera: null,
            avatar: new Backbone.Model()
        },
        initialize: function() {
            var avatar = this.collection.avatars.get(this.get('avatarId'));
            if(avatar instanceof Backbone.Model) {
                this.set('avatar', avatar);
            }
        }
    });

    return SpeechModel;
});