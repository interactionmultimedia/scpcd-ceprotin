define("models/quiz", function(require) {
    "use strict";

    var Backbone                = require("backbone"),
        QuestionsCollection     = require("collections/questions"),
        QuizModel;

    QuizModel   = Backbone.Model.extend({
        defaults: {
            quizId: null,
            quizTitle: null,
            moduleName: null,
            questions: null
        },

        initialize: function(options){
            if(options.title){
                this.set("quizTitle", options.title);
            }

            if( options.questions ) {
                this.set("questions", new QuestionsCollection(options.questions));
            } else {
                this.set("questions", new QuestionsCollection(options.quizQuestionVOList.QuizQuestionVO));
            }

            this.unset("quizQuestionVOList");
        },

        resetSelection: function() {
            this.get("questions").each(function(q){
                q.resetSelection();
            });
        },

        toJSON: function () {
            return JSON.parse(JSON.stringify(this.attributes));
        }
    });

    return QuizModel;
});
