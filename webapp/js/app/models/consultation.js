define("models/consultation", function (require) {
    "use strict";

    var Backbone                            = require("backbone"),
        $                                   = require("jquery"),
        _                                   = require("underscore"),
        i18n                                = require("i18n"),
        merge                               = require("deepmerge"),
        PatientProfileModel                 = require("models/profile"),
        MedicalHistory                      = require("models/medicalHistory"),
        StepsCollection                     = require("collections/steps"),
        MedicalFilesCollection              = require("collections/medicalFiles"),
        AnnexesCollection                   = require("collections/annexes"),
        QuizzesCollection                   = require("collections/quizzes"),
        OngoingTreatmentsCollection         = require("collections/ongoingTreatments"),
        WayOfLivingsCollection              = require("collections/wayOfLivings"),
        NeonatSymptomsCollection            = require("collections/neonatSymptoms"),
        FamilyBackgroundsCollection         = require("collections/familyBackgrounds"),
        HistoryOfDiseasesCollection         = require("collections/historyOfDiseases"),
        SpecialistsReportsCollection        = require("collections/specialistsReports"),
        ExamsResultsCollection              = require("collections/examsResults"),
        QuizModel                           = require("models/quiz"),
        ConsultationModel;

    ConsultationModel = Backbone.Model.extend({
        defaults: {
            annexes: new AnnexesCollection(),
            caseId: null,
            caseName: null,
            consultationId: null,
            consultationIndex: null,
            environnement: null,
            examsResults: new ExamsResultsCollection(),
            laboratoryResults: new ExamsResultsCollection(),
            examsResultsComplementary: new ExamsResultsCollection(),
            historyOfDiseases: new HistoryOfDiseasesCollection(),
            medicalFiles: new MedicalFilesCollection(),
            medicalHistories: new MedicalHistory(),
            name: null,
            ongoingTreatments: new OngoingTreatmentsCollection(),
            profile: new PatientProfileModel(),
            quizzes: new QuizzesCollection(),
            specialistsReports: new SpecialistsReportsCollection(),
            synthesis: null,
            steps: new StepsCollection(),
            wayOfLiving: new WayOfLivingsCollection(),
            familyBackground: new FamilyBackgroundsCollection(),
            neonatSymptoms: new NeonatSymptomsCollection()
        },
        xmlData: null,
        appData: null,

        initialize: function(options){
            this.set("caseId", options.caseId);
            this.set("consultationId", options.consultationId);
            this.set("consultationIndex", ~~options.consultationId);

            var clinicalCase    = App.ClinicalCases.get(~~options.caseId),
                caseName        = clinicalCase.getConsultation(~~options.consultationId).title;

            this.set("caseName", clinicalCase.get("caseName"));
            this.set("name", caseName);
            this.setFetchUrl();

        },

        initData: function() {
            this.initEnvironnement();
            this.initPatientProfile();
            this.initAvailableSteps();
            this.initAvailableMedicalFiles();
            this.initAnnexes();
            this.initMedicalHistories();
            this.initQuizzes();
            this.initOngoingTreatment();
            this.initWayOfLiving();
            this.initFamilyBackground();
            this.initHistoryOfDiseases();
            this.initSpecialistsReport();
            this.initExamsResults();
            this.initLaboratoryResults();
            this.initExamsResultsComplementary();
            this.initGlobalConclusion();
            this.initNeonatSymptoms()
        },

        setFetchUrl: function() {
            this.url    = "/data/cases/case_" + this.get("caseId") + "_" + this.get("consultationId") + ".json";
        },

        fetch: function (options) {
            options = options || {};
            options.dataType = "json";
            return Backbone.Model.prototype.fetch.call(this, options);
        },

        parse: function(data) {
            this.xmlData = data;
            this.appData = data.ArrayOfApplicationVO.ApplicationVO;

            return this.appData;
        },

        /**
         * Récupération d'un module par son nom
         * @param  {String} name Nom du module (balise <VOName>)
         */
        getModuleByName: function(name) {
            var data = _(this.appData.ModuleVO).filter(function(node){
                return node.VOName == name;
            });

            if( !!data && !!data[0] ) {
                return {
                    ModuleVO: data[0]
                };
            }

            return null;
        },

        initGlobalConclusion: function() {
            var data        = "",
                node        = $(this.xmlData).find('ApplicationVO > synthesis')[0];

            if(!!node){
                data = JSON.parse(node);
            }

            this.set("synthesis", data);
        },

        /**
         * Initialisation des environnements de la plateforme
         * => fusion des données PG et custom
         */
        initEnvironnement: function () {
            var data      = this.getModuleByName('EnvironmentModule');

            if( !!data ) {
                this.set("environnement", data);
            }
        },

        /**
         * Initialisation du profil patient
         */
        initPatientProfile: function() {
            var data      = this.getModuleByName('PatientProfileModule'),
                reg         = /^avatars\/avatar/,
                asset;

            if( !!data ) {
                this.set("profile", new PatientProfileModel(data.ModuleVO.content.ContentVO));

                if( !!this.appData.bundlesList ) {
                    asset   = _.find(this.appData.bundlesList.AssetVO, function(value) {
                        return reg.test(value.url);
                    });

                    this.get("profile").set("patientProfilePicture", ( !!asset ? asset.url : "" ));
                }
            }
        },

        /**
         * Initialisation des environnements de la plateforme
         * => fusion des données PG et custom
         */
        initAnnexes: function () {
            var data      = this.getModuleByName('AppendicesModule');

            if( !!data ) {
                this.set("annexes", new AnnexesCollection(data.ModuleVO.content.ContentVO.AppendiceItemVOList.AppendiceItemVO));
            }
        },

        /**
         *
         */
        initMedicalHistories: function () {
            var data              = this.getModuleByName('MedicalHistoriesModule'),
                medicalHistories    = new MedicalHistory();

            if( !!data ) {
                medicalHistories.parse(data);

                this.set("medicalHistories", medicalHistories);
            }
        },

        /**
         * Initialisation des environnements de la plateforme
         * => fusion des données PG et custom
         */
        initOngoingTreatment: function () {
            var data              = this.getModuleByName('CurrentTreatmentModule');

            if( !!data ) {
                this.set("ongoingTreatments", new OngoingTreatmentsCollection(data.ModuleVO.content.ContentVO));
            }
        },

        initWayOfLiving: function () {
            var data              = this.getModuleByName('LifestyleModule');

            if( !!data ) {
                this.set("wayOfLiving", new WayOfLivingsCollection(data.ModuleVO.content.ContentVO));
            }
        },


        initNeonatSymptoms: function () {
            var data              = this.getModuleByName('NeonatSymptomsModule');

            if( !!data ) {
                this.set("neonatSymptoms", new NeonatSymptomsCollection(data.ModuleVO.content.ContentVO));
            }
        },

        initFamilyBackground: function () {
            var data              = this.getModuleByName('FamilyBackgroundModule');

            if( !!data ) {
                this.set("familyBackground", new FamilyBackgroundsCollection(data.ModuleVO.content.ContentVO));
            }
        },

        initHistoryOfDiseases: function () {
            var data              = this.getModuleByName('HistoryOfDiseaseModule');

            if( !!data ) {
                this.set("historyOfDiseases", new HistoryOfDiseasesCollection(data.ModuleVO.content.ContentVO.HistoryOfDiseaseItems.HistoryOfDiseaseStepVO));
            }
        },

        initSpecialistsReport: function() {
            var data              = this.getModuleByName('SpecialistsConsultationReportCustomModule');

            if( !!data ) {
                this.set("specialistsReports", new SpecialistsReportsCollection(data.ModuleVO.content.ContentVO.ReportItems.ReportItemVO));
            }
        },

        initExamsResults: function() {
            var data              = this.getModuleByName('ExamsResultsModule');
            if( !!data ) {
                this.set("examsResults", new ExamsResultsCollection(data.ModuleVO.content.ContentVO));
            }
        },

        initLaboratoryResults: function() {
            var data              = this.getModuleByName('LaboratoryResultsModule');
            if( !!data ) {
                this.set("laboratoryResults", new ExamsResultsCollection(data.ModuleVO.content.ContentVO));
            }
        },

        initExamsResultsComplementary: function() {
            var data              = this.getModuleByName('ResultsComplementaryExamsCustomModule');

            if( !!data ) {
                this.set("examsResultsComplementary", new ExamsResultsCollection(data.ModuleVO.content.ContentVO));
            }
        },

        /**
         * Ajout de tout les quiz d'une consultation
         */
        initQuizzes: function () {
            var that    = this,
                modules = ['Diagnosis', 'PrescriptionExams', 'Other', ['TreatmentOthersModule'], 'MonitoringProtocol', 'MedicalTreatmentModule'],
                quizzes = new QuizzesCollection();

            _.each(modules, function(mod){
                var dataSimple, quiz, data;
                if(_.isArray(mod)){
                    var questions   = [];

                    _.each(mod, function(subModule){

                        dataSimple      = that.getModuleByName(subModule);

                        if( !!dataSimple ) {

                            if(!data){
                                data = dataSimple;
                            }

                            _.each(dataSimple.ModuleVO.content.ContentVO.contentList.SuperItemVO.QuizVO.quizQuestionVOList, function(_questions){
                                if(Array.isArray(_questions)) {
                                    _.each(_questions, function(_q){
                                        questions.push(_q);
                                    });
                                } else {
                                    questions.push(_questions);
                                }                                
                            });
                        }

                    });

                    if( !!data ) {
                        data.ModuleVO.content.ContentVO.contentList.SuperItemVO.QuizVO.quizQuestionVOList.QuizQuestionVO = questions;
                        quiz        = new QuizModel(Object.assign(data.ModuleVO.content.ContentVO.contentList.SuperItemVO.QuizVO, {moduleName: mod[0], title: i18n.t(that.getStep(mod[0], false).get("slugTitle"))}));
                    }
                } else {
                    data      = that.getModuleByName(mod);

                    if( !!data ) {
                        quiz    = new QuizModel(Object.assign(data.ModuleVO.content.ContentVO.contentList.SuperItemVO.QuizVO, {moduleName: mod, title: i18n.t(that.getStep(mod, false).get('slugTitle'))}));
                    }
                }

                if( quiz ) {
                    quizzes.add(quiz);
                    that.set("quizzes", quizzes);
                }
            });
        },

        /**
         * Initialisation des étapes de consultation
         */
        initAvailableSteps: function () {
            this.get('steps').each(this.initModelDataModule.bind(this));
        },

        /**
         * Initialisation du dossier médical
         */
        initAvailableMedicalFiles: function () {
            this.get('medicalFiles').each(this.initModelDataModule.bind(this));
        },

        initModelDataModule: function(model) {
            var data      = this.getModuleByName(model.get('moduleName'));

            if( model.get('moduleName') === 'MedicalFiles' ) {
                var data = this.getModuleByName('PatientProfileModule');
            }

            if( !!data ){
                model.set('dataModule', data);
                model.set('available', true);
            }
        },

        getMergeData: function (module, moduleMore) {
            var data = null;

            if(module !== null && moduleMore !== null){
                data = Object.assign(module, moduleMore);
            } else {
                if(module){
                    data = module;
                }

                if(moduleMore){
                    data = moduleMore;
                }
            }

            return data;
        },

        getPatientFullName: function() {
            var profile = this.get("profile");

            return $.trim(profile.get("patientFirstName")+" "+profile.get("patientLastName"));
        },

        getPatientPictureProfile: function() {
            return this.get("profile").get("patientProfilePicture");
        },

        /**
         * Des étapes de consultation sont-elles disponibles ?
         * => affichage du bouton "Consultation Steps"
         */
        hasSteps: function () {
            return this.getAvailableSteps().length > 0;
        },

        /**
         * Retourne les étapes de consultation disponibles
         */
        getAvailableSteps: function () {
            var stepsOrder = ( this.appData.StepsOrder ? this.appData.StepsOrder.Step : [] );
            return new StepsCollection(this.get("steps").getAvailableSteps(stepsOrder));
        },

        /**
         * Des éléments du dossier médical sont-ils disponibles ?
         * => affichage du bouton "Medical Files"
         */
        hasMedicalFiles: function () {
            return this.getAvailableMedicalFiles().length > 0;
        },

        /**
         * Retourne les étapes de consultation disponibles
         */
        getAvailableMedicalFiles: function () {
            return new MedicalFilesCollection(this.get("medicalFiles").getAvailableMedicalFiles());
        },

        /**
         * Retourne les informations d'un environnement selon son
         * @param  {String} name Nom de l'environnement
         */
        getEnvironnement: function(name) {
            return _.find(this.get("environnement").ModuleVO.content.ContentVO, {
                cameraName: name
            });
        },

        getStep: function (moduleName, filtered) {
            var options = {
                moduleName: moduleName
            };

            if( typeof filtered === "undefined" ) {
                options.available   = true;
            }

            return this.get("steps").findWhere(options);
        },

        getIdWithPrefix: function(prefix) {
            return prefix+this.get("caseId");
        },

        isPatientFemale: function () {
            var reg = /^avatars\/f/,
                url = this.get("profile").get("patientProfilePicture");

            return ( !!url ? reg.test(url) : false );
        },

        isDoctorFemale: function () {
            
            var isFemale = (this.get("profile").get("isFemaleDoctor"));
            return (typeof isFemale != 'undefined');
        
        },

        hasAnnexes: function () {
            return this.get("annexes").length > 0;
        },

        getTextByModuleName: function(moduleName) {
            return this.getStep(moduleName).get("dataModule");
        },

        toJSON : function() {
            return JSON.parse(JSON.stringify(this.attributes));
        },

        cleanConsultationForFeedback: function() {
            var attrs   = ['caseName', 'name', 'quizzes', 'synthesis'],
                that    = this;

            _.each(this.attributes, function(value, attr){
                if(_.indexOf(attrs, attr) === -1){
                    that.unset(attr);
                }
            });
        }
    });

    return ConsultationModel;
});
