define("models/annexe", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        AnnexeModel;

    AnnexeModel   = Backbone.Model.extend({
        defaults: {
            text: null
        }
    });

    return AnnexeModel;
});
