define("models/antecedentFamily", function(require) {
    "use strict";

    var Backbone    = require("backbone"),
        i18n        = require("i18n"),
        AntecedentModel;

    AntecedentModel   = Backbone.Model.extend({
        defaults: {
            discoveredID: null,
            disease: null,
            familyMember: null,
            observationsValue: null
        },
        set: function(value) {
            if( value.observationsValue === "SECTION_MEDICAL_HISTORY_FAMILY_NO" ) {
                value.observationsValue = '<ul><li>'+i18n.t("consultation.medicalFiles.medicalHistory.defaults."+value.observationsValue)+'</li></ul>';
            }

            Backbone.Model.prototype.set.apply(this, arguments);
        }
    });

    return AntecedentModel;
});
