define("app", function(require) {
    "use strict";

    var Marionette              = require("marionette"),
        $                       = require("jquery"),
        _                       = require("underscore"),
        Radio                   = require("backbone.radio"),
        Modernizr               = require("modernizr"),
        i18n                    = require("i18n"),
        i18nXHR                 = require("i18nXHR"),
        i18nLngDetector         = require("i18nLngDetector"),
        Router                  = require("libs/router"),
        ConfigModel             = require("models/config"),
        SimulationState         = require("models/simulationState"),
        LanguagesCollection     = require("collections/languages"),
        ClinicalCasesCollection = require("collections/clinicalCases"),
        app;

    app = Marionette.Application.extend({
        initialize: function intialize() {
            var that = this;

            Radio.channel("App").reply("region:show", this.showRegion.bind(this));
            Radio.channel("App").reply("legals:show", this.showLegals.bind(this));
            Radio.channel("App").reply("track:event", this.trackEvent.bind(this));
            Radio.channel("App").reply("reset:simulationState", this.destroySimulationState.bind(this));

            window.App = (window.App) || new Marionette.Application();

            App.addRegions({
                mainRegion: "#wrapper",
                legalsRegion: "#legals"
            });

            App.Config          = new ConfigModel();
            App.Languages       = new LanguagesCollection();
            App.ClinicalCases   = new ClinicalCasesCollection();

            $.when(App.Config.fetch(), App.Languages.fetch()).then(function() {
                $.when(App.ClinicalCases.fetch()).then(that.initI18n);
            });

            this.ajaxSetup();

            App.on("start", this.start.bind(this));
            App.on("i18n:file:loaded", function() {
                $('html').addClass("lg-" + App.i18n.language);
                $.when(that.initAnalytics()).always(function(){
                    App.start();
                });
            });
        },

        /**
         * Chargement google analytics
         */
        initAnalytics: function() {
            var promise = $.Deferred(),
                GA_ID   = App.Config.getGAID();

            if(!!GA_ID){
                requirejs.s.contexts._.config.config = {
                    'GA': {
                        'id' : GA_ID
                    }
                };
                require(['GA'], function (GA) {
                    App.GA = GA;
                    promise.resolve();
                }, function (err) {
                    promise.reject();
                });
            } else {
                promise.reject();
            }

            return promise;
        },

        /*
         *
         */
        initI18n: function() {
            var lngDetector = new i18nLngDetector(),
                options     = {
                    defaultNS: "shire",
                    fallbackLng : "en",
                    ns: [ "shire" ],
                    backend: {
                        loadPath: "/js/locales/{{lng}}/{{ns}}.json"
                    }
                },
                clinicalCases           = App.ClinicalCases.getPublishedClinicalCasesByLanguage(),
                availableLanguages      = App.Languages.getAvailableLanguages(),
                whitelist               = _.intersection(_.keys(clinicalCases), availableLanguages);

            App.Languages.setActiveLanguages(whitelist);

            if( whitelist.length <= 1 ) {
                i18n
                    .use(i18nXHR)
                    .init(_.extend(options, {
                        lng: ( !!whitelist[0] ? whitelist[0] : "en" )
                    }), function() {
                        App.i18n = i18n;
                        App.trigger("i18n:file:loaded");
                    });
            } else {
                lngDetector.addDetector({
                    name: 'navigatorCustom',

                    lookup: function lookup(options) {
                        var found = [];

                        if (typeof navigator !== 'undefined') {
                            if (navigator.languages) {
                                // chrome only; not an array, so can't use .push.apply instead of iterating
                                for (var i = 0; i < navigator.languages.length; i++) {
                                    found.push(navigator.languages[i]);
                                }
                            }
                            if (navigator.userLanguage) {
                                found.push(navigator.userLanguage);
                            }
                            if (navigator.language) {
                                found.push(navigator.language);
                            }

                            found   = found.map(function(currentValue) {
                                return currentValue.replace(/-[a-z0-9]+/i, '');
                            });
                        }

                        return found.length > 0 ? found : undefined;
                    }
                });

                i18n
                    .use(i18nXHR)
                    .use(lngDetector)
                    .init(_.extend(options, {
                        detection: {
                            order: ['querystring', 'cookie', 'localStorage', 'navigatorCustom', 'htmlTag']
                        },
                        whitelist: whitelist
                    }), function() {
                        App.i18n = i18n;
                        App.trigger("i18n:file:loaded");
                    });
            }
        },

        start: function start() {
            $(document).find('title').text(i18n.t('app.title'));

            App.router = new Router();

            App.SimulationState = new SimulationState();
            App.SimulationState.get(1);

            if (Backbone.history) {
                Backbone.history.start();
                this.trigger("backbone:history:start");
            }
        },

        showRegion: function(params) {
            App.mainRegion.show(params.view, {forceShow: true});
        },

        showLegals: function(params) {
            App.legalsRegion.show(params.view);
        },

        trackEvent: function(params) {
            if(App.GA){
                App.GA.event(params.category, params.action, params.label, params.value);
            }
        },

        destroySimulationState: function(){
            var emailFeedback = App.SimulationState.get("email");

            App.SimulationState.destroy();
            App.SimulationState = new SimulationState();
            if(!!emailFeedback){
                App.SimulationState.set("email", emailFeedback);
                App.SimulationState.save();
            }

        },
        ajaxSetup: function () {
            $.ajaxSetup({
                beforeSend: function (xhr, settings) {
                    if(i18n){
                        settings.url += ( /\?/.test(settings.url) !== true ? "?" : "&" )+"language=" + i18n.language;
                    }
                }
            });
        }
    });

    return app;
});
