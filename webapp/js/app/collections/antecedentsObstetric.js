define("collections/antecedentsObstetric", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        AntecedentModel = require("models/antecedentObstetric"),
        Antecedents;

    Antecedents   = Backbone.Collection.extend({
        model: AntecedentModel
    });

    return Antecedents;
});
