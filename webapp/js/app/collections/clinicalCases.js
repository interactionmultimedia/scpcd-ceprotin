define("collections/clinicalCases", function(require) {
    "use strict";

    var Backbone                    = require("backbone"),
        ClinicalCaseModel           = require("models/clinicalCase"),
        ClinicalCasesCollection;

    ClinicalCasesCollection   = Backbone.Collection.extend({
        model: ClinicalCaseModel,
        mainJobBag: "",
        url: "/data/cases/cases.json",

        parse: function(data) {
            if(data) {
                this.xmlData        = data;
                this.appData        = data.ArrayOfCaseDescriptionVO.CaseDescriptionVO;
                this.mainJobBags    = data.ArrayOfCaseDescriptionVO.mainJobBags;
            }

            return this.appData;
        },

        getPublishedClinicalCases: function(roomId, language) {
            return new ClinicalCasesCollection(this.filter(function(m){
                return ( m.get("language") === language
                        && (m.get("published") || !App.Config.isProduction()) );
            }));
        },

        getPublishedClinicalCasesByLanguage: function() {
            var publishedClinicalCases = new ClinicalCasesCollection(this.filter(function(m){
                return (m.get("published"));
            }));

            return publishedClinicalCases.groupBy('language');
        },

        getMainJobBag: function(language) {
            var data;

            if( !_.isArray(this.mainJobBags) ) {
                this.mainJobBags = [this.mainJobBags];
            }

            data  = _.findWhere(this.mainJobBags, {language: language});
            
            return ( !!data ? data.jobBag : "" );
        }
    });

    return ClinicalCasesCollection;
});
