define("collections/familyBackgrounds", function(require) {
    "use strict";

    var Backbone                        = require("backbone"),
        FamilyBackgroundModel          = require("models/familyBackground"),
        FamilyBackgroundsCollection;

    FamilyBackgroundsCollection    = Backbone.Collection.extend({
        model: FamilyBackgroundModel
    });

    return FamilyBackgroundsCollection;
});
