define("collections/annexes", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        AnnexeModel     = require("models/annexe"),
        Annexes;

    Annexes   = Backbone.Collection.extend({
        model: AnnexeModel
    });

    return Annexes;
});
