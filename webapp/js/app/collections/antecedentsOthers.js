define("collections/antecedentsOthers", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        AntecedentModel = require("models/antecedentOthers"),
        Antecedents;

    Antecedents   = Backbone.Collection.extend({
        model: AntecedentModel
    });

    return Antecedents;
});
