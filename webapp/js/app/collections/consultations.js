define("collections/consultations", function(require) {
    "use strict";

    var Backbone            = require("backbone"),
        ConsultationModel   = require("models/consultation"),
        Consultations;

    Consultations   = Backbone.Collection.extend({
        model: ConsultationModel,

        caseExists: function(caseId) {
            return this.find(function(c){
                return ~~c.get("caseId") === ~~caseId;
            });
        },

        consultationExists: function(consultationId) {
            return this.find(function(c){
                return ~~c.get("consultationId") === ~~consultationId;
            });
        }
    });

    return Consultations;
});
