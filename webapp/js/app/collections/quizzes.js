define("collections/quizzes", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        QuizModel     = require("models/quiz"),
        Quizzes;

    Quizzes   = Backbone.Collection.extend({
        model: QuizModel,

        getQuizByModuleName: function(moduleName) {
            return this.findWhere({
                moduleName: moduleName
            });
        }
    });

    return Quizzes;
});
