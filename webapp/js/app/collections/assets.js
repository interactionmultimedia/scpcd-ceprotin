define("collections/assets", function(require) {
    "use strict";

    var Backbone            = require("backbone"),
        AssetModel          = require("models/asset"),
        AssetsCollection;

    AssetsCollection   = Backbone.Collection.extend({
        model: AssetModel
    });

    return AssetsCollection;
});
