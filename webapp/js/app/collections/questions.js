define("collections/questions", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        QuestionModel   = require("models/question"),
        Questions;

    Questions   = Backbone.Collection.extend({
        model: QuestionModel
    });

    return Questions;
});
