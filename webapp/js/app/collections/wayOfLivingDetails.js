define("collections/wayOfLivingDetails", function(require) {
    "use strict";

    var Backbone                        = require("backbone"),
        WayOfLivingDetailModel          = require("models/wayOfLivingDetail"),
        WayOfLivingDetailsCollection;

    WayOfLivingDetailsCollection    = Backbone.Collection.extend({
        model: WayOfLivingDetailModel
    });

    return WayOfLivingDetailsCollection;
});
