define("collections/ongoingTreatments", function(require) {
    "use strict";

    var Backbone                    = require("backbone"),
        OngoingTreatmentModel       = require("models/ongoingTreatment"),
        OngoingTreatmentsCollection;

    OngoingTreatmentsCollection   = Backbone.Collection.extend({
        model: OngoingTreatmentModel,
    });

    return OngoingTreatmentsCollection;
});
