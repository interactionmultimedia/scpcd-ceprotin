define("collections/neonatSymptoms", function(require) {
    "use strict";

    var Backbone                        = require("backbone"),
        neonatSymptomModel          = require("models/neonatSymptom"),
        NeonatSymptomsCollection;

    NeonatSymptomsCollection    = Backbone.Collection.extend({
        model: neonatSymptomModel
    });

    return NeonatSymptomsCollection;
});
