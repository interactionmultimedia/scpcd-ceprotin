define("collections/answers", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        AnswerModel     = require("models/answer"),
        Answers;

    Answers   = Backbone.Collection.extend({
        model: AnswerModel
    });

    return Answers;
});
