define("collections/specialistsReports", function(require) {
    "use strict";

    var Backbone                        = require("backbone"),
        SpecialistReportModel           = require("models/specialistReport"),
        SpecialistsReportsCollection;

    SpecialistsReportsCollection    = Backbone.Collection.extend({
        model: SpecialistReportModel
    });

    return SpecialistsReportsCollection;
});
