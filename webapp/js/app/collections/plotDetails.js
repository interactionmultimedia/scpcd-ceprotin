define("collections/plotDetails", function(require) {
    "use strict";

    var Backbone            = require("backbone"),
        PlotDetailModel     = require("models/plotDetail"),
        PlotDetailsCollection;

    PlotDetailsCollection   = Backbone.Collection.extend({
        model: PlotDetailModel,
    });

    return PlotDetailsCollection;
});
