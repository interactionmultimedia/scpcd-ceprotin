define("collections/antecedentsAllergy", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        AntecedentModel = require("models/antecedentAllergy"),
        Antecedents;

    Antecedents   = Backbone.Collection.extend({
        model: AntecedentModel
    });

    return Antecedents;
});
