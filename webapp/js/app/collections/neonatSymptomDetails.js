define("collections/neonatSymptomDetails", function (require) {
    "use strict";

    var Backbone = require("backbone"),
        NeonatSymptomDetailModel = require("models/neonatSymptomDetail"),
        NeonatSymptomDetailsCollection;

    NeonatSymptomDetailsCollection = Backbone.Collection.extend({
        model: NeonatSymptomDetailModel
    });

    return NeonatSymptomDetailsCollection;
});
