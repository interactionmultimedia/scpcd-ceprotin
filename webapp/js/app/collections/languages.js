define("collections/languages", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        LanguageModel   = require("models/language"),
        Languages;

    Languages   = Backbone.Collection.extend({
        model: LanguageModel,
        url: "/js/locales/languages.json",
        getAvailableLanguages: function() {
            return this.pluck("code");
        },
        getLanguageByCode: function(code) {
            return this.findWhere({"code": code});
        },
        getActiveLanguages: function() {
            var that = this;

            return new Languages(this.filter(function(m){
                return ( _.indexOf(that.activeLanguages, m.get("code")) !== -1 );
            }));
        },
        setActiveLanguages: function(activeLanguages) {
            this.activeLanguages    = activeLanguages;
        }
    });

    return Languages;
});
