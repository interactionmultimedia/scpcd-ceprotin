define("collections/participants", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        Participant     = require("models/participant"),
        Participants;

    Participants = Backbone.Collection.extend({
        model: Participant
    });

    return Participants;
});
