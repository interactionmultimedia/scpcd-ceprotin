define("collections/wayOfLivings", function(require) {
    "use strict";

    var Backbone                        = require("backbone"),
        WayOfLivingModel          = require("models/wayOfLiving"),
        WayOfLivingsCollection;

    WayOfLivingsCollection    = Backbone.Collection.extend({
        model: WayOfLivingModel
    });

    return WayOfLivingsCollection;
});
