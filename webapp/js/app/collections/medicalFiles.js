define("collections/medicalFiles", function(require) {
    "use strict";

    var Backbone            = require("backbone"),
        MedicalFileModel    = require("models/medicalFile"),
        MedicalFilesCollection;

    MedicalFilesCollection   = Backbone.Collection.extend({
        model: MedicalFileModel,
        url: "/data/medicalFiles.json",

        getAvailableMedicalFiles: function () {
            return this.where({
                "available": true
            });
        },

        getModuleIndex: function(moduleName) {
            var model = this.findWhere({
                "moduleName": moduleName
            });

            return this.indexOf(model);
        }
    });

    return MedicalFilesCollection;
});
