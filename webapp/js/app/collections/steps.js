define("collections/steps", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        _               = require("underscore"),
        StepModel       = require("models/step"),
        StepsCollection;

    StepsCollection   = Backbone.Collection.extend({
        model: StepModel,
        url: "/data/steps.json",

        getAvailableSteps: function (moduleOrder) {
            var steps = this.where({
                "available": true
            });

            if( !!moduleOrder ) {
                var cpt = steps.length;
                steps   = _.sortBy(steps, function(object) {
                    var index   = _.indexOf(moduleOrder, object.get("moduleName"));
                    return ( index !== -1 ? index : cpt++ );
                });
            }

            return steps;
        }
    });

    return StepsCollection;
});
