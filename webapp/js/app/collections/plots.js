define("collections/plots", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        PlotModel       = require("models/plot"),
        PlotsCollection;

    PlotsCollection   = Backbone.Collection.extend({
        model: PlotModel
    });

    return PlotsCollection;
});
