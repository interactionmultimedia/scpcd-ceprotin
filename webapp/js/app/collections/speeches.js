define("collections/speeches", function(require) {
    "use strict";

    var Backbone            = require("backbone"),
        SpeechModel         = require("models/speech"),
        AvatarsCollection   = require("collections/avatars"),
        SpeechesCollection;

    SpeechesCollection   = Backbone.Collection.extend({
        model: SpeechModel,
        initialize: function(speeches, avatars) {
            this.avatars = new AvatarsCollection(avatars);
        }
    });

    return SpeechesCollection;
});
