define("collections/examsResults", function(require) {
    "use strict";

    var Backbone                 = require("backbone"),
        ExamResultModel          = require("models/examResult"),
        ExamResultsCollection;

    ExamResultsCollection    = Backbone.Collection.extend({
        model: ExamResultModel
    });

    return ExamResultsCollection;
});
