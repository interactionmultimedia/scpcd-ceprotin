define("collections/antecedentsFamily", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        AntecedentModel = require("models/antecedentFamily"),
        Antecedents;

    Antecedents   = Backbone.Collection.extend({
        model: AntecedentModel
    });

    return Antecedents;
});
