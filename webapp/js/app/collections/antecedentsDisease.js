define("collections/antecedentsDisease", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        AntecedentModel = require("models/antecedentDisease"),
        Antecedents;

    Antecedents   = Backbone.Collection.extend({
        model: AntecedentModel
    });

    return Antecedents;
});
