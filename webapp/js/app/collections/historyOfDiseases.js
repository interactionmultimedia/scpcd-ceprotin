define("collections/historyOfDiseases", function(require) {
    "use strict";

    var Backbone                        = require("backbone"),
        HistoryOfDiseaseModel           = require("models/historyOfDisease"),
        HistoryOfDiseasesCollection;

    HistoryOfDiseasesCollection    = Backbone.Collection.extend({
        model: HistoryOfDiseaseModel
    });

    return HistoryOfDiseasesCollection;
});
