define("collections/avatars", function(require) {
    "use strict";

    var Backbone        = require("backbone"),
        AvatarModel     = require("models/avatar"),
        Avatars;

    Avatars   = Backbone.Collection.extend({
        model: AvatarModel
    });

    return Avatars;
});
