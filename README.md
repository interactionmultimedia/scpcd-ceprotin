# Shire - Ceprotin

### Technos utilisées

> NodeJS

> BacvagrakboneJS

> MarionetteJS

> RequireJS

### Environnement technique

> Ubuntu Trusty 12.04 x64

> PHP 5.6

> Mysql 5.6

> NodeJS 5

> Ip : 192.168.56.59

### Pré-requis

> Pour le minification : ```sudo npm install -g requirejs``` (r.js: 2.3.2, RequireJS: 2.3.2, UglifyJS: 2.7.3)

> Pour utiliser les outils d'automatisation : ```npm install -g grunt-cli```

### Mots de passe de la box

> root user debian : root/vagrant

> vagrant user debian : vagrant:vagrant

### Urls utiles

Configurer votre fichier hosts pour pointer vers l'IP attribuée à votre VM :

* [http://cases.shire.local/](http://cases.shire.local/)

### Initialisation du projet

Se positionner dans le répertoire du projet puis

> `$ git init`

> `$ git remote add origin git@bitbucket.org:interactionmultimedia/scpcd-ceprotin.git`

> `$ git pull origin integ`


### Lancement de la box

Avant tout, toujours dans le répertoire où l'on a récupéré le dépôt, assurez-vous que le plugin ```vagrant-hostmanager``` est bien installé :

> `$ vagrant plugin install vagrant-hostmanager`

Puis taper :

> `$ vagrant up`

puis pour se connecter à la box en ssh :

> `$ vagrant ssh`

#### Installation des dépendances

Depuis la machine hôte :

> `$ npm install`

Directement depuis la box :

> `~~$ cd /var/www/ && npm install --legacy-bundling~~

> `$ cd /var/www/webapp && bower install`

### Déploiement

Pour déployer la _Web app_ sur la préprod, à la racine du projet, exécuter :

> `$ grunt build`

Puis envoyer les fichiers sur le serveur FTP.

### Utilitaires (dev)

Pour génèrer un mot de passe, depuis la box :

> `$ /var/www/utils/generate-pwd.js MonMotDePasse`

Pour vérifier les traductions :

> `$ /var/www/utils/check-locale.js jp`
> `$ /var/www/utils/check-locale.js es`