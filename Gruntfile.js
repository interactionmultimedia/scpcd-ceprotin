module.exports = function (grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        availabletasks: {
            tasks: {
                options: {
                    showTasks: ["user"],
                    filter: "exclude",
                    tasks: ["availabletasks", "default"],
                    groups: {
                        "Optimisation": ["minify-webapp", "imagemin"],
                        "Assets": ["list-webapp-assets"]
                    },
                    descriptions: {
                        "minify-webapp": "Optimise le code de la Web app",
                        "list-webapp-assets": "Liste les assets de la Web app",
                        "imagemin": "Compresse sans perte les images des assets"
                    }
                }
            }
        },
        folder_list: {
            options: {
                files: true,
                folders: false
            },
            generic: {
                src: ['img/**/*', 'data/assets/*_c0/avatars/*', '!img/**/*.xml', '!img/**/*.json'],
                dest: 'webapp/img/assets.json',
                cwd: 'webapp/'
            },
            cas1consultation1EN: {
                src: ['data/assets/c1_c0/**/*', '!**/*.json'],
                dest: 'webapp/data/assets/c1_c0/assets.json',
                cwd: 'webapp/'
            },
            cas1consultation2EN: {
                src: ['data/assets/c1_c1/**/*', '!**/*.json'],
                dest: 'webapp/data/assets/c1_c1/assets.json',
                cwd: 'webapp/'
            },
            cas1consultation3EN: {
                src: ['data/assets/c1_c2/**/*', '!**/*.json'],
                dest: 'webapp/data/assets/c1_c2/assets.json',
                cwd: 'webapp/'
            },
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'webapp/',
                    src: ['data/assets/**/*.{png,jpg,gif,jpeg,svg}'],
                    dest: 'webapp/dist/'
                },{
                    expand: true,
                    cwd: 'webapp/',
                    src: ['img/*.{png,jpg,gif,jpeg,svg}', 'img/**/*.{png,jpg,gif,jpeg,svg}'],
                    dest: 'webapp/dist/'
                }]
            }
        },
        shell: {
            minify_webapp: {
                options: {
                    stdout: false
                },
                command: "r.js -o webapp/build.js"
            },
            update_sources: {
                command: "git checkout integ && git pull"
            }
        },
    });

    // Load plugins
    grunt.loadNpmTasks("grunt-available-tasks");
    grunt.loadNpmTasks("grunt-folder-list");
    grunt.loadNpmTasks("grunt-shell");
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    // Defining default task
    grunt.registerTask("default", ["build"]);

    grunt.registerTask("build", ["list-webapp-assets","minify-webapp"]);

    grunt.registerTask("list-webapp-assets", ["folder_list"]);
    grunt.registerTask("minify-webapp", ["shell:minify_webapp"]);
};
